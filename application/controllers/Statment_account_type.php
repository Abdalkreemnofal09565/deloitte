<?php

require_once 'Welcome.php';

class Statment_account_type extends Welcome
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('payment_type_model');
        $this->model = $this->payment_type_model;
        $this->view = 65;
        $this->add = 66;
        $this->edit = 67;
        $this->active = 68;
        $this->inactive = 69;
        $this->delete = 70;

    }

    function index()
    {
        if (check_permission($this->view) == true) {
            $this->render_page('payment_type/index', 'Statement Account Type ');
        }
    }

    function datatable()
    {
        if (check_permission($this->view, false)) {
            $edit = check_permission($this->edit, false);
            $active = check_permission($this->active, false);
            $inactive = check_permission($this->inactive, false);
            $deleted = check_permission($this->delete, false);
            $col_ord = array(
                'pt_id',
                'pt_title',
                'pt_type',
                'pt_date_added',
                's_title',
                's_id'
            );
            $col_search = array(
                'pt_id',
                'pt_title',
                'pt_type',
                'pt_date_added',
                's_title',
                's_id'
            );
            $name_table = 'view_payment_type';
            $order = array('pt_id' => 'DESC');
            $where[0] = "3";
            $col_where[0] = 's_id !=';
            $joinTable = null;
            $joinCol = null;
            $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
            $data = array();
            $id = 1;
            foreach ($list as $item) {
                $row = array();
                $row['id'] = $item->pt_id;
                $row['title'] = $item->pt_title;;
                if ($item->pt_type == 0) {
                    $row['type'] = "increase";
                } else {
                    $row['type'] = "decrease";
                }

                $row['date_register'] = $item->pt_date_added;

                $row['num'] = $id;
                $row['state'] = $item->s_title;
                if ($item->s_id == '1') {
                    $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill"  id="state_' . $item->pt_id . '">' . $item->s_title . '</span>';
                } else if ($item->s_id == '2') {
                    $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" id="state_' . $item->pt_id . '">' . $item->s_title . '</span>';
                }

                $row['option'] = '<span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">';
                //edit
                if ($edit) {
                    $row['option'] .= '                <a class="dropdown-item" href="' . base_url("Statment_account_type/edit/" . $item->pt_id) . '"><i class="la la-edit"></i> Edit Details</a>';
                }
                //inactive
                if ($item->s_id == '1' && $inactive) {
                    $row['option'] .= ' <a class="dropdown-item" onclick="item_inactive(' . $item->pt_id . ')"   ><i class="la la-times-circle"></i> Inactive</a>';
                } //active
                else if ($active) {
                    $row['option'] .= '  <a class="dropdown-item" onclick="item_active(' . $item->pt_id . ')"  ><i class="la la-check"></i> Active</a>';
                }
                //delete
                if ($deleted) {
                    $row['option'] .= '  <a class="dropdown-item" onclick="delete_item(' . $item->pt_id . ')"  ><i class="la la-remove"></i> Delete</a>';
                }
                $row['option'] .= '</div></span>';

                $id++;
                $data[] = $row;
            }

            $output = array(
                "draw" => @$_POST['draw'],
                "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
                "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
                "data" => $data,
            );
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    function add()
    {
        if (check_permission(29)) {
            $this->render_page('payment_type/add', 'New Level');
        }
    }

    function create()
    {
        $this->form_validation->set_rules('title', 'title', 'required|min_length[3]|max_length[99]');
        $this->form_validation->set_rules('type', 'type', 'required');
        if ($this->form_validation->run() == true) {
            $data ["pt_title"] = $this->input->post("title");
            $data ["pt_type"] = $this->input->post("type");
            $this->model->set($data);
            $output = array('result' => "1");
        } else {
            $output = array('result' => "0",
                "title" => form_error('title'),
                "price" => form_error('type'),
            );
        }
        echo json_encode($output);
    }

    function edit($id)
    {
        if (check_permission($this->edit) == true) {
            $payment_type = $this->db->where('pt_id', $id)->get('view_payment_type');
            if ($payment_type->num_rows() == 0) redirect(base_url('level'));
            $data['payment_type'] = $payment_type->row();
            $this->render_page('payment_type/edit', "Edit Payment type", $data);
        }
    }

    function do_edit()
    {
        if (check_permission($this->edit, false)) {
            $this->form_validation->set_rules('title', 'title', 'required|min_length[3]|max_length[99]');
            $this->form_validation->set_rules('type', 'type', 'required');
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == true) {
                $data['pt_title'] = $this->input->post('title');
                $data['pt_type'] = $this->input->post('type');
                $id = $this->input->post("id");
                $this->model->update($id, $data);
                echo json_encode(array('result' => '1'));
            } else {
                echo json_encode(array('result' => "0", "title" => form_error('title'), "type" => form_error('type')));
            }
        } else {
            echo json_encode(array('result' => '0'));
        }

    }

    function list_deleted()
    {
        if (check_permission($this->view) == true) {
            $this->render_page('payment_type/list_delete', 'Levels');
        }
    }

    function export($delete = false)
    {
//        $this->load->model("excel_export_model");
        $this->load->library("excel");
        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);
        $table_columns = array("System Id", "Payment type", "type", "State", "date added");
        $column = 0;

        foreach ($table_columns as $field) {

            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }
        $excel_row = 2;
        $col_ord = array(
            'pt_id',
            'pt_title',
            'pt_type',
            'pt_date_added',
            's_title',
            's_id'
        );
        $col_search = array(
            'pt_id',
            'pt_title',
            'pt_type',
            'pt_date_added',
            's_title',
            's_id'
        );
        $name_table = 'view_payment_type';
        $order = array('pt_id' => 'DESC');
        $where[0] = "3";
        $col_where[0] = 's_id !=';
        $joinTable = null;
        $joinCol = null;
        $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
        $data = array();
        $id = 1;
        foreach ($list as $item) {
            $type = 0;
            if ($item->pt_type == 0) {
                $type = "increase";
            } else {
                $type = "decrease";
            }
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $item->pt_id);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $item->pt_title);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $type);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $item->s_title);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $item->pt_date_added);


            $excel_row++;
        }
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        $name_file = "Payment type Data";
        if ($delete)
            $name_file = "Payment type Deleted Data";

        header('Content-Disposition: attachment;filename="' . $name_file . '.xls"');
        $object_writer->save('php://output');

    }


}