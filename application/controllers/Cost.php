<?php
require_once 'Welcome.php';

class Cost extends Welcome
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Cost_model');
        $this->model = $this->Cost_model;
    }

    function index()
    {
        if (check_permission(58))
            $this->render_page("cost/index", 'Time Work');
    }

    function manger($pending = null)
    {
        if (check_permission(58) || ($this->db->get_where('project', array('project_manger' => $this->session->userdata('id')))->num_rows() == 1)) {
            $data['pending'] = $pending;
            $this->render_page('project_manger/expanse', 'Expanse Manger',$data);
        }
    }

    function datatable($manger = null, $pending = null)
    {

        if (check_permission(58) || $manger == "manger") {
            $col_ord = array(
                'cp_id',
                'ct_id',
                'e_id',
                's_id',
                'cp_note',
                'cp_price',
                'p_id',
                'cp_date_added',
                'client',
                'clint_id',
                'payment_type',
                'project',
                'state',
                'employee', 'project_manger'

            );
            $col_search = array(
                'cp_id',
                'ct_id',
                'e_id',
                 's_id',
                'cp_note',
                'cp_price',
                'p_id',
                'cp_date_added',
                'client',
                'clint_id',
                'payment_type',
                'project',
                'state',
                'employee', 'project_manger'
            );
            $name_table = 'view_costs';
            $order = array('s_id' => 'AEC');
            $where = array();
            $col_where = array();
            if (isset($manger)) {
                $where[0] = $this->session->userdata('id');
                $col_where[0] = "project_manger";
            }
            if (isset($manger)&&isset($pending)) {
                $where[1] = '4';
                $col_where[1] = 's_id';
            }

            $joinTable = null;
            $joinCol = null;
            $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
            $data = array();
            $id = 1;
            foreach ($list as $item) {
                $row = array();
                $row['date_added'] = $item->cp_date_added;
                $row['hoursType'] = $item->payment_type;
                $row['client'] = $item->client;
                $row['project'] = $item->project;
                $row['employee'] = $item->employee;
                $row['title'] = $item->cp_date;
                $row['price'] = $item->cp_price;
                $row['note'] = $item->cp_note;
                $row['num'] = $id;
                $row['state'] = $item->state;
                if ($item->s_id == '4') {
                    $row['state'] = '<span class="kt-badge  kt-badge--warning kt-badge--inline kt-badge--pill" id="state_' . $item->cp_id . '">' . $item->state . '</span>';
                } else if ($item->s_id == '5') {
                    $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" id="state_' . $item->cp_id . '">' . $item->state . '</span>';
                } else if ($item->s_id == '6') {
                    $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" id="state_' . $item->cp_id . '">' . $item->state . '</span>';
                }


                $row['option'] = '
                <button class="btn btn-sm btn-outline-danger " onclick="reject(' . $item->cp_id . ')">
                              <i class="la la-remove"></i>reject
                            </button>
                            <button class="btn btn-sm btn-outline-success "  onclick="accept(' . $item->cp_id . ')">
                              <i class="la la-check"></i>accept
                            </button>
                            
                            ';
                $id++;
                $data[] = $row;
            }

            $output = array(
                "draw" => @$_POST['draw'],
                "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
                "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
                "data" => $data,
            );
            echo json_encode($output);

        }
    }

    function reject()
    {
        if (check_permission(58, false)) {
            $id = $this->input->post("id");
            $this->model->state($id, 6);
            $_SESSION['project_manger_time_sheet'] = $this->db->get_where('view_hours_project', array('project_manger' => $this->session->userdata('id'), 's_id' => 4))->num_rows();
            $_SESSION['project_manger_expanse'] = $this->db->get_where('view_costs', array('project_manger' => $this->session->userdata('id'), 's_id' => 4))->num_rows();

        }
    }

    function accept()
    {
        if (check_permission(58, false)) {
            $id = $this->input->post("id");
            $this->model->state($id, 5);
            $_SESSION['project_manger_time_sheet'] = $this->db->get_where('view_hours_project', array('project_manger' => $this->session->userdata('id'), 's_id' => 4))->num_rows();
            $_SESSION['project_manger_expanse'] = $this->db->get_where('view_costs', array('project_manger' => $this->session->userdata('id'), 's_id' => 4))->num_rows();


        }
    }


}
