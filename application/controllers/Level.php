<?php

require_once 'Welcome.php';

class Level extends Welcome
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Level_model');
        $this->model = $this->Level_model;
        $this->add = 23;
        $this->view = 24;
        $this->edit = 28;
        $this->active = 26;
        $this->inactive = 27;
        $this->delete = 25;

    }

    function index()
    {
        if (check_permission($this->view) == true) {
            $this->render_page('level/index', 'Levels');
        }
    }

    function datatable($delete = false)
    {
        if (check_permission($this->view, false)) {
            $edit = check_permission($this->edit, false);
            $active = check_permission($this->active, false);
            $inactive = check_permission($this->inactive, false);
            $deleted = check_permission($this->delete, false);
            $col_ord = array(
                'l_id',
                'l_title',
                'l_price',
                'l_date_added',
                's_title',
                's_id'
            );
            $col_search = array(
                'l_id',
                'l_title',
                'l_price',
                'l_date_added',
                's_title',
                's_id'
            );
            $name_table = 'view_level';
            $order = array('l_id' => 'DESC');
            $col_where[0] = 's_id !=';
            if (isset($delete) && $delete == "true")
                $col_where[0] = 's_id =';

            $where[0] = "3";
            $joinTable = null;
            $joinCol = null;
            $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
            $data = array();
            $id = 1;
            foreach ($list as $item) {
                $row = array();
                $row['id'] = $item->l_id;
                $row['title'] = $item->l_title;;
                $row['price'] = $item->l_price;;
                $row['date_register'] = $item->l_date_added;

                $row['num'] = $id;
                $row['state'] = $item->s_title;
                if ($item->s_id == '1') {
                    $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill"  id="state_' . $item->l_id . '">' . $item->s_title . '</span>';
                } else if ($item->s_id == '2') {
                    $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill"  id="state_' . $item->l_id . '">' . $item->s_title . '</span>';
                }
                $row['option'] = '<span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">';
                //edit
                if ($edit) {
                    $row['option'] .= '                <a class="dropdown-item" href="' . base_url("level/edit/" . $item->l_id) . '"><i class="la la-edit"></i> Edit Details</a>';
                }
                //inactive
                if ($item->s_id == '1' && $inactive) {
                    $row['option'] .= ' <a class="dropdown-item" onclick="item_inactive(' . $item->l_id . ')"   ><i class="la la-times-circle"></i> Inactive</a>';
                } //active
                else if ($active) {
                    $row['option'] .= '  <a class="dropdown-item" onclick="item_active(' . $item->l_id . ')"  ><i class="la la-check"></i> Active</a>';
                }
                //delete
                if ($deleted) {
                    $row['option'] .= '  <a class="dropdown-item" onclick="delete_item(' . $item->l_id . ')"  ><i class="la la-remove"></i> Delete</a>';
                }
                $row['option'] .= '</div></span>';


                $id++;
                $data[] = $row;
            }

            $output = array(
                "draw" => @$_POST['draw'],
                "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
                "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
                "data" => $data,
            );
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    function add()
    {
        if (check_permission(29)) {
            $this->render_page('level/add', 'New Level');
        }
    }

    function create()
    {
        $this->form_validation->set_rules('level', 'level', 'required|min_length[3]|max_length[99]');
        $this->form_validation->set_rules('price', 'price', 'required');
        if ($this->form_validation->run() == true) {
            $data ["l_title"] = $this->input->post("level");
            $data ["l_price"] = $this->input->post("price");
            $this->model->set($data);
            $output = array('result' => "1");
        } else {
            $output = array('result' => "0",
                "level" => form_error('level'),
                "price" => form_error('price'),
            );
        }
        echo json_encode($output);
    }

    function edit($id)
    {
        if (check_permission($this->edit) == true) {
            $level = $this->db->where('l_id', $id)->get('level');
            if ($level->num_rows() == 0) redirect(base_url('level'));
            $data['level'] = $level->row();
            $this->render_page('level/edit', "Edit level", $data);
        }
    }

    function do_edit()
    {
        if (check_permission($this->edit, false)) {
            $this->form_validation->set_rules('level', 'level', 'required');
            $this->form_validation->set_rules('price', 'price', 'required');
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == true) {
                $data['l_title'] = $this->input->post('level');
                $data['l_price'] = $this->input->post('price');
                $id = $this->input->post("id");
                $new_id = $this->model->set($data);
                $e = $this->db->get('employee')->result();
                $this->load->model('Employee_model');
                foreach ($e as $employee) {
                    $level = $this->Employee_model->get_level($employee->e_id);

                    if (  isset($level[0]) &&  $level[0]->l_id == $id   ) {
                        $data1['e_id'] = $employee->e_id;
                        $data1['l_id'] = $new_id;
                        $data1['le_note'] = $level[0]->le_note;
                        $this->Employee_model->set_level($data1);
                    }
                }


                echo json_encode(array('result' => '1'));
            } else {
                echo json_encode(array('result' => "0", "level" => form_error('level'), "price" => form_error('price')));
            }
        } else {
            echo json_encode(array('result' => '0'));
        }

    }

    function list_deleted()
    {
        if (check_permission($this->view) == true) {
            $this->render_page('level/list_delete', 'Levels');
        }
    }

    function export($deleted = null)
    {
//        $this->load->model("excel_export_model");
        $this->load->library("excel");
        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);
        $table_columns = array("System Id", "Level", "Price per hour", "State", "date added");
        $column = 0;

        foreach ($table_columns as $field) {

            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }
        $excel_row = 2;
        $col_ord = array(
            'l_id',
            'l_title',
            'l_price',
            'l_date_added',
            's_title',
            's_id'
        );
        $col_search = array(
            'l_id',
            'l_title',
            'l_price',
            'l_date_added',
            's_title',
            's_id'
        );
        $name_table = 'view_level';
        $order = array('l_id' => 'DESC');
        $col_where[0] = 's_id !=';
        if (isset($deleted) && $deleted == "true")
            $col_where[0] = 's_id =';
        $where[0] = "3";
        $joinTable = null;
        $joinCol = null;
        $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
        $data = array();
        $id = 1;
        foreach ($list as $item) {
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $item->l_id);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $item->l_title);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $item->l_price);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $item->l_price);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $item->l_date_added);

            $excel_row++;
        }
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        $name_file = "Level Data";
        if (isset($delete) && $delete == "deleted")
            $name_file = "Level Deleted Data";

        header('Content-Disposition: attachment;filename="' . $name_file . '.xls"');
        $object_writer->save('php://output');

    }

}