<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{
    public $dataTable;
    public $model;
    public $view;
    public $edit;
    public $add;
    public $active;
    public $inactive;
    public $delete;

    function __construct()
    {
        
        parent::__construct();
        
        if (!isset($_SESSION['id']) || !isset($_SESSION['login'])) {
          
            redirect(base_url('login'));
           
        }

        $this->load->model('DataTable');
        $this->dataTable = $this->DataTable;

    }

    public function RandomString()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 10; $i++) {
            $randstring = $randstring . $characters[rand(0, strlen($characters))];

        }

        return $randstring;
    }

    function render_page($page, $title, $data = null)
    {
        $header['title'] = $title;
        $header['my_project'] = $this->db->select('count(*) as cnt')->group_by('p_id')->get_where('project_emplyee', array('e_id' => $this->session->userdata('id'), 's_id' => 1, 'pe_date_start<=' => date("Y/m/d"), 'pe_date_active>=' => date("Y/m/d")));
        $permissions = $this->db->get_where('permission', array('e_id' => $_SESSION['id']))->result();
        foreach ($permissions as $permission) {
            $header['pre_' . $permission->sr_id] = "ok";
        }
        if (!isset($_SESSION['project_manger'])) {
            $project_manger = $this->db->get_where('project', array('project_manger' => $this->session->userdata('id')));
            if ($project_manger->num_rows() != 0) {
                $_SESSION['project_manger_num'] = $project_manger->num_rows();
                $_SESSION['project_manger'] = true;
                $_SESSION['project_manger_time_sheet'] = $this->db->get_where('view_hours_project', array('project_manger' => $this->session->userdata('id'), 's_id' => 4))->num_rows();
                $_SESSION['project_manger_expanse'] = $this->db->get_where('view_costs', array('project_manger' => $this->session->userdata('id'), 's_id' => 4))->num_rows();

            } else {
                $_SESSION['project_manger'] = false;
            }
        }
        if ($header['my_project']->num_rows() != 0)
            $header['my_project'] = $header['my_project']->num_rows();
        else {
            $header['my_project'] = 0;
        }

        $this->load->view('share/header', $header);
        $this->load->view($page, $data);
        $this->load->view('share/footer');

    }

    public function index()
    {

        $this->render_page('home/index', 'home page');
    }

    public function login()
    {
        
        $this->load->view('login');
    }

    function page_403()
    {
        page_403();
    }

    function active()
    {
        if (check_permission($this->active, false)) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == true) {
                $id = $this->input->post('id');

                $this->model->state($id, 1);
                echo $this->db->last_query();
            }
            echo json_encode(array('result' => 1));
        }
    }

    function inactive()
    {
        if (check_permission($this->inactive, false)) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == true) {
                $id = $this->input->post('id');
                $this->model->state($id, 2);

            }
            echo json_encode(array('result' => 1));
        }
    }

    function remove()
    {
        if (check_permission($this->delete, false)) {
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == true) {
                $id = $this->input->post('id');
                $this->model->state($id, 3);
            }
            echo json_encode(array('result' => 1));
        }
    }

    function not_found()
    {
        $this->load->view('share/page_404');
    }

    function profile()
    {
        $data['profile'] = $this->db->get_where('employee', array('e_id' => $this->session->userdata('id')))->row();
        $data['level'] = $this->db->order_by('le_id', 'DESC')->limit(1)->get_where('view_employee_level', array('e_id' => $this->session->userdata('id')))->row();
        $this->render_page('profile/index', 'My profile', $data);
    }

    function change_password()
    {
        $this->form_validation->set_rules('password', '<b>Password</b>', 'trim|required|min_length[8]|max_length[99]');
        $this->form_validation->set_rules('new_password', '<b>New Password</b>', 'trim|required|min_length[8]|max_length[99]');
        $this->form_validation->set_rules('conf_new_password', '<b>Confirmation New Password</b>', 'trim|required|min_length[8]|max_length[99]|matches[new_password]');
        if ($this->form_validation->run() == TRUE) {
            $password = $this->input->post('password', true);
            $password = md5($password . "_deloitte");
            $check_password = $this->db->get_where('employee', array(
                'e_id' => $this->session->userdata('id'),
                'e_password' => $password
            ));
            if ($check_password->num_rows() > 0) {
                $password = $this->input->post('new_password', true);
                $password = md5($password . "_deloitte");
                $this->db->where(array('e_id' => $this->session->userdata('id')))->update('employee', array('e_password' => $password));
                echo json_encode(array('result' => 1));
            } else {
                echo json_encode(array('result' => 0, 'password' => 'The password is wrong'));
            }

        } else {
            echo json_encode(array(
                'result' => 0,
                'password' => form_error('password'),
                'new_password' => form_error('new_password'),
                'conf_new_password' => form_error('conf_new_password'),
            ));
        }
    }

    function logout()
    {
        session_destroy();
        $_SESSION = array();
        session_unset();
        redirect(base_url());
    }

    function export_datebase()
    {
        $da['e_id'] = $this->session->userdata('id');
        $this->db->insert('backup', $da);

        $this->load->dbutil();

        $prefs = array(
            'format' => 'zip',
            'filename' => 'my_db_backup.sql'
        );


        $backup =& $this->dbutil->backup($prefs);

        $db_name = 'backup-on-' . date("Y-m-d-H-i-s") . '.zip';
        $save = 'pathtobkfolder/' . $db_name;

        $this->load->helper('file');
        write_file($save, $backup);


        $this->load->helper('download');
        force_download($db_name, $backup);
    }

    function backup()
    {
        if (check_permission(96, true)) {
            $data['list'] = $this->db->select('backup.*,employee.e_first_name,employee.e_last_name')->join('employee', 'employee.e_id =backup.e_id')->get('backup')->result();
            $this->render_page('backup/index', 'backup', $data);
        }
    }
}
