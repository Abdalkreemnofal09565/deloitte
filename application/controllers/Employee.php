<?php

include_once 'Welcome.php';

class Employee extends Welcome
{
    // role in employee
    private $set_level = false;
    private $view_level = false;
    private $report = false;
    private $reset_password = false;
    private $view_project = false;
    private $send_email = false;
    private $set_permission = false;
    private $update_permission = false;

    function __construct()
    {

        parent::__construct();
        $this->load->model('Employee_model');
        $this->model = $this->Employee_model;
        $this->add = 1;
        $this->edit = 2;
        $this->delete = 3;
        $this->active = 4;
        $this->inactive = 5;
        $this->reset_password = 6;
        $this->view_project = 7;
        $this->report = 8;
        $this->send_email = 9;
        $this->view = 10;
        $this->set_level = 29;
        $this->set_permission = 49;


    }

    function index()
    {
        if (check_permission($this->view) == true) {
            $category = $this->db->get_where('category', array('s_id' => 1, 'c_employee' => 1))->result();
            $data['category'] = $category;
            $this->render_page('employee/index', 'Employees', $data);
        }
    }

    function datatable()
    {
        if (check_permission($this->view, false)) {
            $add = check_permission($this->add, false);
            $edit = check_permission($this->edit, false);
            $delete = check_permission($this->delete, false);
            $active = check_permission($this->active, false);
            $inactive = check_permission($this->inactive, false);
            $reset_password = check_permission($this->reset_password, false);
            $view_project = check_permission($this->view_project, false);
            $report = check_permission($this->report, false);
            $send_email = check_permission($this->send_email, false);
            $view = true;
            $set_level = check_permission($this->set_level, false);
            $set_permission = check_permission($this->set_permission, false);

            $col_ord = array(
                'e_id',
                'e_email',
                'e_first_name',
                'e_last_name',
                'e_date_added',
                'e_note',
                'e_identify_number',
                's_title',
                's_id'
            );
            $col_search = array(
                'e_id',
                'e_email',
                'e_first_name',
                'e_last_name',
                'e_date_added',
                'e_note',
                'e_identify_number',
                's_title',
                's_id'
            );
            $name_table = 'view_employee';
            $order = array('e_first_name' => 'ASC', 'e_last_name' => 'ASC');
            $where[0] = "3";
            $col_where[0] = 's_id !=';
            $joinTable = null;
            $joinCol = null;
            $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
            $data = array();
            $id = 1;
            $category = $this->db->get_where('category', array('s_id' => 1, 'c_employee' => 1))->result();

            foreach ($list as $item) {
                $row = array();
                $row['email'] = $item->e_email;
                $row['id'] = $item->e_id;
                $row['first_name'] = $item->e_first_name;
                $row['last_name'] = $item->e_last_name;
                $row['name'] = $item->e_first_name . ' ' . $item->e_last_name;
                $row['date_register'] = $item->e_date_added;
                $row['id_employee'] = $item->e_identify_number;
                $row['num'] = $id;
                $row['note'] = $item->e_note;
                $row['state'] = $item->s_title;
                foreach ($category as $value) {
                    $row[(string)$value->c_title . ""] = $this->model->get_category($item->e_id, $value->c_id);
                }

                if ($item->s_id == '1') {
                    $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" id="state_' . $item->e_id . '">' . $item->s_title . '</span>';
                } else if ($item->s_id == '2') {
                    $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" id="state_' . $item->e_id . '">' . $item->s_title . '</span>';
                }

                $row['option'] = '<span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">';
                if ($edit)
                    $row['option'] .= '<a class="dropdown-item" href="' . base_url('employee/edit/' . $item->e_id) . '"><i class="la la-edit"></i> Edit Details</a>';
                if ($delete)
                    $row['option'] .= '<a class="dropdown-item" onclick="delete_item(' . $item->e_id . ')" > <i class="la la-remove"></i> Delete</a>';
                if ($active && $item->s_id == 2)
                    $row['option'] .= '<a class="dropdown-item active-item "  onclick="item_active(' . $item->e_id . ')" id="active_' . $item->e_id . '"  ><i class="la la-check"></i> Active</a>';
                else if ($inactive && $item->s_id == 1)
                    $row['option'] .= '<a class="dropdown-item   unactive-item"  onclick="item_inactive(' . $item->e_id . ')" id="inactive_' . $item->e_id . '" ><i class="la la-times-circle"></i> Inactive</a>';
//                if ($view_project)
//                    $row['option'] .= '<a class="dropdown-item" href="#"><i class="la la-edit"></i> Projects</a>';
                if ($set_permission)
                    $row['option'] .= '<a class="dropdown-item" href="' . base_url('employee/permission/' . $item->e_id) . '"><i class="la la-shield"></i>Permission</a>';
                if ($set_level)
                    $row['option'] .= '<a class="dropdown-item" href="' . base_url('employee/level/' . $item->e_id) . '"><i class="la la-sort"></i>Set Level</a>';
                if ($report)
                    $row['option'] .= '<a class="dropdown-item" href="' . base_url('employee/generated_report/' . $item->e_id) . '"><i class="la la-print"></i> Generate Report</a>';
                $row['option'] .= ' </div></span>';

                $id++;
                $data[] = $row;
            }

            $output = array(
                "draw" => @$_POST['draw'],
                "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
                "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
                "data" => $data,
            );
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    function add()
    {
        if (check_permission($this->add)) {
            $category = $this->db->get_where('category', array('s_id' => 1, 'c_employee' => 1))->result();
            $d = array();
            foreach ($category as $item) {
                $sub_Category = $this->db->get_where('sub_category', array('c_id' => $item->c_id, 's_id' => 1))->result();
                $array = array();
                $array['category'] = $item->c_title;
                $array['sub_category'] = $sub_Category;
                $d[] = $array;
            }
            $data['additional_data'] = $d;
            $this->render_page('employee/add', 'New Employee', $data);
        }
    }


    function create()
    {
        if (check_permission($this->add, false)) {
            $this->form_validation->set_rules('first_name', 'First Name', 'required|min_length[2]|max_length[99]');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[2]|max_length[99]');
            $this->form_validation->set_rules('identify_number', 'Last Name', 'max_length[99]');
            $this->form_validation->set_rules('email', 'Email', 'required|min_length[2]|max_length[99]|is_unique[employee.e_email]');
            $output = array();
            if ($this->form_validation->run() == true) {
                $data['e_first_name'] = $this->input->post("first_name");
                $data['e_last_name'] = $this->input->post("last_name");
                $data['e_email'] = $this->input->post("email");
                $data['e_identify_number'] = $this->input->post("identify_number");
                $data['e_note'] = $this->input->post("note");
                $password = $this->RandomString();
                $data['e_password'] = md5($password . "_deloitte");
                $id = $this->model->set($data);
                $categories = $this->input->post('sub_category');
                $this->model->add_categories($id, $categories);
                $this->load->model("EmailModel");
                $this->EmailModel->create_new_user($this->input->post("email"), $password);
                $output = array('result' => "1");

            } else {
                $output = array(
                    'result' => "0",
                    'email' => form_error('email'),
                    'first_name' => form_error('first_name'),
                    'last_name' => form_error('last_name'),
                    'identify_number' => form_error('identify_number')
                );
            }
            echo json_encode($output);
        }

    }

    function level($id)
    {
        if (check_permission($this->set_level)) {
            $data['history'] = $this->model->get_level($id);
            $data['id'] = $id;
            $data['level'] = $this->model->get_levels();
            $this->render_page('employee/level', 'History Level', $data);
        }
    }

    function set_level()
    {

        if (check_permission($this->set_level, false)) {
            $this->form_validation->set_rules('id', 'ID', 'required');
            $this->form_validation->set_rules('level', 'level', 'required');
            if ($this->form_validation->run() == true) {
                $data['e_id'] = $this->input->post('id');
                $data['l_id'] = $this->input->post('level');
                $data['le_note'] = $this->input->post('note');
                $this->model->set_level($data);
            }
            redirect(base_url('employee/level/' . $this->input->post('id')));
            return;
        }
        redirect(base_url('employee'));
    }


    function edit($id)
    {
        if (check_permission($this->edit)) {
            $this->load->model('Employee_model');
            $category = $this->db->get_where('category', array('s_id' => 1, 'c_employee' => 1))->result();
            $d = array();
            foreach ($category as $item) {
                $sub_Category = $this->db->get_where('sub_category', array('c_id' => $item->c_id))->result();
                $array = array();
                $array['category'] = $item->c_title;
                $array['sub_category'] = $sub_Category;
                $array['active'] = $this->model->get_category_active($id, $item->c_id);
                $d[] = $array;
            }
            $data['id'] = $id;
            $data['additional_data'] = $d;
            $data['employee'] = $this->Employee_model->get($id);
            $this->render_page('employee/edit', 'Update .Info', $data);
        }
    }

    function update()
    {
        if (check_permission($this->edit, false)) {
            $this->form_validation->set_rules('first_name', 'First Name', 'required|min_length[2]|max_length[99]');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[2]|max_length[99]');
            $this->form_validation->set_rules('identify_number', 'Last Name', 'max_length[99]');
            $this->form_validation->set_rules('email', 'Email', 'required|min_length[2]|max_length[99]');
            $this->form_validation->set_rules('id', 'ID', 'required');
            if ($this->form_validation->run() == TRUE) {
                $set['e_first_name'] = $this->input->post('first_name');
                $set['e_last_name'] = $this->input->post('last_name');
                $set['e_identify_number'] = $this->input->post('identify_number');
                $set['e_email'] = $this->input->post('email');
                $set['e_note'] = $this->input->post('note');
                $id = $this->input->post('id');
                $this->model->update($id, $set);
                $this->model->remove_all_categry($id);
                $categories = $this->input->post('sub_category');
                $this->model->add_categories($id, $categories);
                echo json_encode(array('result' => '1'));
            } else {
                echo json_encode(array('result' => '0',
                    'email' => form_error('email'),
                    'first_name' => form_error('first_name'),
                    'last_name' => form_error('last_name'),
                    'identify_number' => form_error('identify_number')));
            }

        } else {
            echo json_encode(array('result' => '0'));
        }
    }

    function permission($employee)
    {
        if (check_permission($this->set_permission)) {
            if ($this->model->found($employee)) {

                $data['permission'] = $this->model->get_all_permission($employee);
                $data['info_employee'] = $this->model->get_info($employee);
                $this->render_page('employee/set_permission', 'set permission', $data);
            } else {
                redirect(base_url('page-404'));
            }
        }
    }

    function set_permission()
    {
        if (check_permission($this->set_permission, false)) {
            $employee = $this->input->post('employee');
            $persmission = $this->input->post('persmission');
            $this->model->update_permission($employee, $persmission);
            echo json_encode(array('result' => 1));

        } else {
            echo json_encode(array('result' => 0));

        }
    }

    function generated_report($employee)
    {
        if (check_permission($this->report)) {
            $information_employee = $this->db->get_where('employee', array('e_id' => $employee));
            if ($information_employee->num_rows() == 0) redirect(base_url('employee'));
            $data['employee'] = $information_employee->row();
            $data['last_level'] = null;
            $data['levels'] = array();
            $levels = $this->db->order_by('le_date_added', 'DESC')->where('e_id', $employee)->get('view_employee_level');
            if ($levels->num_rows() == 0) {
                $data['last_level'] = "Not found";
                $data['budget'] = "-";
            } else {
                $data['last_level'] = $levels->row()->l_title;
                $data['budget'] = $levels->row()->l_price;
                $data['levels'] = $levels->result();
            }
            $data['report'] = $this->model->get_reports($employee);
            $this->render_page('employee/report', 'report', $data);
        }
    }

    function export()
    {
//        $this->load->model("excel_export_model");
        $this->load->library("excel");
        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);
        $table_columns = array("System Id", "First Name", "Last Name", "Email", "ID", "Note", "STATE");
//        $table_columns[]="ammar";
//        print_r($table_columns);
        $column = 0;
        $category = $this->db->get_where('category', array('s_id' => 1, 'c_employee' => 1))->result();
        foreach ($category as $value) {
//            $row[.""]  =$this->model->get_category($item->e_id,$value->c_id);
            $table_columns[] = $value->c_title;
        }
//        print_r($table_columns);
        foreach ($table_columns as $field) {
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }


//        $employee_data = $this->excel_export_model->fetch_data();

        $excel_row = 2;

        $col_ord = array(
            'e_id',
            'e_email',
            'e_first_name',
            'e_last_name',
            'e_date_added',
            'e_note',
            'e_identify_number',
            's_title',
            's_id'
        );
        $col_search = array(
            'e_id',
            'e_email',
            'e_first_name',
            'e_last_name',
            'e_date_added',
            'e_note',
            'e_identify_number',
            's_title',
            's_id'
        );
        $name_table = 'view_employee';
        $order = array('e_first_name' => 'ASC', 'e_last_name' => 'ASC');
        $where[0] = "3";
        $col_where[0] = 's_id !=';
        $joinTable = null;
        $joinCol = null;
        $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
        $data = array();
        $id = 1;
        foreach ($list as $item) {
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $item->e_id);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $item->e_first_name);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $item->e_last_name);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $item->e_email);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $item->e_identify_number);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $item->e_note);
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $item->s_title);
            $i = 7;
            foreach ($category as $value) {
                $object->getActiveSheet()->setCellValueByColumnAndRow($i, $excel_row, $this->model->get_category($item->e_id, $value->c_id));
                $i++;
            }
            $excel_row++;

        }

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Employee Data.xls"');
        $object_writer->save('php://output');
    }

}