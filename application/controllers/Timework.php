<?php
require_once 'Welcome.php';

class Timework extends Welcome
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('TimeWork_model');
        $this->model = $this->TimeWork_model;
    }

    function index()
    {
        if (check_permission(58))
            $this->render_page("TimeWork/index", 'Time Work');
    }
    function manger($pending = null){
        $data['pending'] = $pending;
        $this->render_page("project_manger/time_sheet", 'Time Sheet Manger',$data);
    }

    function datatable($manger = null, $pending = null)
    {


        $col_ord = array(
            'ph_id',
            'p_id',
            'e_id',
            'ht_id',
            'e_id',
            'le_id',
            'ph_hours',
            's_id',
            'ph_date_added',
            'employee',
            'hoursType',
            'project',
            'level_price',
            'level_title',
            'state', 'client', 'project_manger'

        );
        $col_search = array(
            'ph_id',
            'p_id',
            'e_id',
            'ht_id',
            'e_id',
            'le_id',
            'ph_hours',
            's_id',
            'ph_date_added',
            'employee',
            'hoursType',
            'project',
            'level_price',
            'level_title',
            'state',
            'client',
            'project_manger'
        );
        $name_table = 'view_hours_project';
        $order = array('s_id' => 'AEC');
        $where = array();
        $col_where = array();
        if (isset($manger)) {
            $where[0] = $this->session->userdata('id');
            $col_where[0] = 'project_manger';
        }
        if (isset($manger) && isset($pending)) {
            $where[1] = 4;
            $col_where[1] = 's_id';
        }
        $joinTable = null;
        $joinCol = null;
        $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
        $data = array();
        $id = 1;
        foreach ($list as $item) {
            $row = array();
            $row['date_added'] = $item->ph_date_added;
            $row['level'] = $item->level_title;
            $row['hoursType'] = $item->hoursType;
            $row['client'] = $item->client;
            $row['project'] = $item->project;
            $row['employee'] = $item->employee;
            $row['hours'] = $item->ph_hours;
            $row['level'] = $item->level_title;
            $row['level_price'] = $item->level_price;
            $row['num'] = $id;
            $row['state'] = $item->state;
            if ($item->s_id == '4') {
                $row['state'] = '<span class="kt-badge  kt-badge--warning kt-badge--inline kt-badge--pill" id="state_' . $item->ph_id . '">' . $item->state . '</span>';
            } else if ($item->s_id == '5') {
                $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" id="state_' . $item->ph_id . '">' . $item->state . '</span>';
            } else if ($item->s_id == '6') {
                $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" id="state_' . $item->ph_id . '">' . $item->state . '</span>';
            }


            $row['option'] = '
                <button class="btn btn-sm btn-outline-danger " onclick="reject(' . $item->ph_id . ')">
                              <i class="la la-remove"></i>reject
                            </button>
                            <button class="btn btn-sm btn-outline-success "  onclick="accept(' . $item->ph_id . ')">
                              <i class="la la-check"></i>accept
                            </button>
                            
                            ';
            $id++;
            $data[] = $row;
        }

        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
            "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
            "data" => $data,
        );
        echo json_encode($output);

    }

    function reject()
    {
        if (check_permission(58, false)) {
            $id = $this->input->post("id");
            $this->model->state($id, 6);
            if(isset($_SESSION['project_mange']) && $_SESSION['project_mange']) {

                $_SESSION['project_manger_time_sheet'] = $this->db->get_where('view_hours_project', array('project_manger' => $this->session->userdata('id'), 's_id' => 4))->num_rows();
                $_SESSION['project_manger_expanse'] = $this->db->get_where('view_costs', array('project_manger' => $this->session->userdata('id'), 's_id' => 4))->num_rows();
            }
        }
    }

    function accept()
    {
        if (check_permission(58, false)) {
            $id = $this->input->post("id");
            $this->model->state($id, 5);
            if(isset($_SESSION['project_mange']) && $_SESSION['project_mange']) {
                $_SESSION['project_manger_time_sheet'] = $this->db->get_where('view_hours_project', array('project_manger' => $this->session->userdata('id'), 's_id' => 4))->num_rows();
                $_SESSION['project_manger_expanse'] = $this->db->get_where('view_costs', array('project_manger' => $this->session->userdata('id'), 's_id' => 4))->num_rows();
            }

        }
    }


}
