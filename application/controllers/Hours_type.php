<?php
require_once 'Welcome.php';

class Hours_type extends Welcome
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Hours_type_model');
        $this->model = $this->Hours_type_model;
        $this->view=59;
        $this->add = 60;
        $this->edit=61;
        $this->active=62;
        $this->inactive =63;
        $this->delete=64;
    }
    function index()
    {
        if (check_permission($this->view) == true) {
            $this->render_page('hours_type/index', 'hours type');
        }
    }
    function datatable($delete = false)
    {
        if (check_permission($this->view, false)) {
            $edit = check_permission($this->edit, false);
            $active = check_permission($this->active, false);
            $inactive = check_permission($this->inactive, false);
            $deleted = check_permission($this->delete, false);
            $col_ord = array(
                'ht_id',
                'ht_title',
                'ht_date_added',
                's_title',
                's_id'
            );
            $col_search = array(
                'ht_id',
                'ht_title',
                'ht_date_added',
                's_title',
                's_id'
            );
            $name_table = 'view_hours_type';
            $order = array('ht_id' => 'DESC');
            $where[0] = "3";
            if ($delete)
                $col_where[0] = 's_id =';
            else
                $col_where[0] = 's_id !=';
            $joinTable = null;
            $joinCol = null;
            $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
            $data = array();
            $id = 1;
            foreach ($list as $item) {
                $row = array();
                $row['id'] = $item->ht_id;
                $row['title'] = $item->ht_title;;
                $row['date_register'] = $item->ht_date_added;

                $row['num'] = $id;
                $row['state'] = $item->s_title;
                if ($item->s_id == '1') {
                    $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" id="state_' . $item->ht_id . '">' . $item->s_title . '</span>';
                } else if ($item->s_id == '2') {
                    $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill"  id="state_' . $item->ht_id . '">' . $item->s_title . '</span>';
                }
                $row['option'] = '<span class="dropdown " id="option_' . $item->ht_id . '">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">';

                //check permission edit
                if ($edit)
                    $row['option'] .= '<a class="dropdown-item" href="' . base_url('hours_type/edit/' . $item->ht_id) . '"><i class="la la-edit"></i> Edit Details</a>';
                //check permission remove or delete
                if ($deleted)
                    $row['option'] .= '<a class="dropdown-item" onclick="delete_item(' . $item->ht_id . ')" ><i class="la la-remove"></i> Delete</a>';
                // check permission change state
                if ($item->s_id == '1' && $inactive)
                    $row['option'] .= '<a class="dropdown-item" onclick="item_inactive(' . $item->ht_id . ')"><i class="la la-times-circle"></i> inactive</a>';
                else if ($active)
                    $row['option'] .= '<a class="dropdown-item" onclick="item_active(' . $item->ht_id . ')"><i class="la la-check"></i> Active</a>';
                $row['option'] .= ' </div>
                        </span>';

                $id++;
                $data[] = $row;
            }

            $output = array(
                "draw" => @$_POST['draw'],
                "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
                "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
                "data" => $data,
            );
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    function add()
    {
        if (check_permission($this->add)) {
            $this->render_page('hours_type/add', 'New type hours');
        }
    }

    function create()
    {
        if (check_permission($this->add, false)) {
            $this->form_validation->set_rules('title', 'title', 'required|min_length[3]|max_length[99]');
            if ($this->form_validation->run() == true) {
                $data ["ht_title"] = $this->input->post("title");
                $this->model->set($data);
                $output = array('result' => "1");
            } else {
                $output = array('result' => "0", "title" => form_error('title'));
            }
            echo json_encode($output);
        }
    }

    function edit($id)
    {
        if (check_permission($this->edit)) {
            $expanse_type = $this->db->get_where('hours_type', array('ht_id' => $id));
            if ($expanse_type->num_rows() == 0) redirect(base_url('hours_type'));
            $data['hours_type'] = $expanse_type->row();
            $this->render_page('hours_type/edit', $data['hours_type']->ht_title, $data);

        }
    }

    function do_edit()
    {
        if (check_permission($this->edit, false)) {
            $this->form_validation->set_rules('title', 'title', 'required|min_length[3]|max_length[99]');
            $this->form_validation->set_rules('id', 'id', 'required');
            if ($this->form_validation->run() == true) {
                $data ["ht_title"] = $this->input->post("title");
                $id = $this->input->post('id');
                $this->model->update($id, $data);
                $output = array('result' => "1");
            } else {
                $output = array('result' => "0", "title" => form_error('title'));
            }
            echo json_encode($output);
        }
    }

    function list_delete(){
        if(check_permission($this->view)){
            $this->render_page('hours_type/list_delete','list deleted');
        }
    }
    function export($delete = false)
    {
//        $this->load->model("excel_export_model");
        $this->load->library("excel");
        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);
        $table_columns = array("System Id", "Hour type", "State", "date added");
        $column = 0;

        foreach ($table_columns as $field) {

            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }
        $excel_row = 2;
        $col_ord = array(
            'ht_id',
            'ht_title',
            'ht_date_added',
            's_title',
            's_id'
        );
        $col_search = array(
            'ht_id',
            'ht_title',
            'ht_date_added',
            's_title',
            's_id'
        );
        $name_table = 'view_hours_type';
        $order = array('ht_id' => 'DESC');
        $where[0] = "3";
        if ($delete)
            $col_where[0] = 's_id =';
        else
            $col_where[0] = 's_id !=';
        $joinTable = null;
        $joinCol = null;
        $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
        $data = array();
        $id = 1;
        foreach ($list as $item) {
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $item->ht_id);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $item->ht_title);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $item->s_title);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $item->ht_date_added);


            $excel_row++;
        }
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        $name_file = "Hour type Data";
        if ($delete)
            $name_file = "Hour type Deleted Data";

        header('Content-Disposition: attachment;filename="' . $name_file . '.xls"');
        $object_writer->save('php://output');

    }

}