<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Login extends CI_Controller
{
    public function __construct()
    {
       
        parent::__construct();

    }

    public function RandomString()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 10; $i++) {
            $randstring = $randstring . $characters[rand(0, strlen($characters))];

        }

        return $randstring;
    }

    function index()
    {
        
        if (isset($_SESSION['login']) && $_SESSION['login'] = true && isset($_SESSION['id'])) {
            redirect(base_url());
        }
        $this->load->view('login');
    }

    function submit()
    {
        if (isset($_SESSION['login']) && $_SESSION['login'] = true && isset($_SESSION['id'])) {
            redirect(base_url());
        }
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $result = array();
        if ($this->form_validation->run() == true) {
            $email = $this->input->post('email', true);
            $password = $this->input->post('password', true);
            $password = md5($password . "_deloitte");
//            echo $password;
            $value = $this->web->check_user($email, $password);
            if ($value == true)
                $result = array('result' => '1');
            else
                $result = array('result' => '0', 'message' => 'Incorrect username or password. Please try again.');
        } else {
            $result = array('result' => '0',
                'email' => form_error('email'),
                'password' => form_error('password'),
                'message' => ''
            );
        }
        echo json_encode($result);
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }

    function forget()
    {
        $this->form_validation->set_rules('email', 'email', 'required');
        if ($this->form_validation->run() == true) {
            $email = $this->input->post('email');
            $value = $this->web->found_user($email);
            if ($value) {
                $this->load->model("EmailModel");
                $password = $this->RandomString();
                $data['e_password'] = md5($password . "_deloitte");
                $where['e_email'] = $this->input->post('email');
                $this->db->where($where)->update('employee', $data);
                $this->EmailModel->create_new_user($this->input->post("email"), $password);
            }
        }
        $result = array('result' => '1');
        echo json_encode($result);
    }

}