<?php
require_once 'Welcome.php';

class Client extends Welcome
{
    private $view_project;
    private $report;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Client_model');
        $this->model = $this->Client_model;
        $this->add = 34;
        $this->edit = 31;
        $this->delete = 35;
        $this->active = 32;
        $this->inactive = 33;
        $this->view_project = 52;

        $this->view = 30;
    }

    function index()
    {
        if (check_permission($this->view)) {
            $category = $this->db->get_where('category', array('s_id' => 1, 'c_client' => 1))->result();

            $data['category'] = $category;


            $this->render_page('client/index', 'Clients',$data);
        }
    }

    function datatable()
    {
        if (check_permission($this->view, false)) {
            $add = check_permission($this->add, false);
            $edit = check_permission($this->edit, false);
            $delete = check_permission($this->delete, false);
            $active = check_permission($this->active, false);
            $inactive = check_permission($this->inactive, false);
            $view_project = check_permission($this->view_project, false);
            $report = check_permission($this->report, false);
            $view = check_permission($this->view, false);


            $col_ord = array(
                'c_id',
                'c_email',
                'c_name',
                'c_date_added',
                'c_note',
                's_title',
                's_id',
                'projects'
            );
            $col_search = array(
                'c_id',
                'c_email',
                'c_name',
                'c_date_added',
                'c_note',
                's_title',
                's_id',
                'projects'
            );
            $name_table = 'view_client';
            $order = array('c_id' => 'DESC');
            $where[0] = "3";
            $col_where[0] = 's_id !=';
            $joinTable = null;
            $joinCol = null;
            $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
            $data = array();
            $id = 1;
            $category = $this->db->get_where('category', array('s_id' => 1, 'c_client' => 1))->result();

            foreach ($list as $item) {
                $row = array();

                $row['email'] = $item->c_email;
                $row['id'] = $item->c_id;
                $row['projects'] = $item->projects;
                $row['name'] = $item->c_name;
                $row['date_register'] = $item->c_date_added;
                $row['num'] = $id;
                $row['note'] = $item->c_note;
                $row['state'] = $item->s_title;
                foreach ($category as $value){
                    $row[$value->c_title.""]  =$this->model->get_category($item->c_id,$value->c_id);
                }
                if ($item->s_id == '1') {
                    $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" id="state_' . $item->c_id . '">' . $item->s_title . '</span>';
                } else if ($item->s_id == '2') {
                    $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" id="state_' . $item->c_id . '">' . $item->s_title . '</span>';
                }

                $row['option'] = '<span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">';
                if ($edit)
                    $row['option'] .= '<a class="dropdown-item" href="' . base_url('client/edit/' . $item->c_id) . '"><i class="la la-edit"></i> Edit Details</a>';
                if ($delete)
                    $row['option'] .= '<a class="dropdown-item" onclick="delete_item(' . $item->c_id . ')"><i class="la la-remove"></i>Remove</a>';
                if ($item->s_id == 2 && $active == true)
                    $row['option'] .= '<a class="dropdown-item active-item "  onclick="item_active(' . $item->c_id . ')" id="active_' . $item->c_id . '"  ><i class="la la-check"></i> Active</a>';
                else if ($item->s_id == 1 && $inactive == true) $row['option'] .= '         <a class="dropdown-item   unactive-item"  onclick="item_inactive(' . $item->c_id . ')" id="inactive_' . $item->c_id . '" ><i class="la la-times-circle"></i> Inactive</a>';
                if ($view_project == true)
                    $row['option'] .= '<a class="dropdown-item" href="' . base_url('project/' . $item->c_id) . '"><i class="la la-cube"></i>Projects</a>';

                $row['option'] .= ' </div></span>';

                $id++;
                $data[] = $row;
            }

            $output = array(
                "draw" => @$_POST['draw'],
                "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
                "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
                "data" => $data,
            );
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    function create()
    {
        if (check_permission(34)) {
            $this->form_validation->set_rules('client_name', 'Client Name', 'required|min_length[2]|max_length[99]');
            $this->form_validation->set_rules('client_email', 'Client Email', 'required|min_length[2]|max_length[99]');
            if ($this->form_validation->run() == true) {
                $data['c_name'] = $this->input->post('client_name');
                $data['c_note'] = $this->input->post('client_note');
                $data['c_email'] = $this->input->post('client_email');
                $id =$this->model->set($data);
                $categories = $this->input->post('sub_category');
                $this->model->add_categories($id, $categories);
                $output = array('result' => "1");
            } else {
                $output = array('result' => "0"
                , 'client_name' => form_error('client_name')
                , 'client_email' => form_error('client_email')
                );
            }
            echo json_encode($output);
        }
    }

    function add()
    {
        if (check_permission(34)) {
            $category = $this->db->get_where('category', array('s_id' => 1, 'c_client' => 1))->result();
            $d = array();
            foreach ($category as $item) {
                $sub_Category = $this->db->get_where('sub_category', array('c_id' => $item->c_id , 's_id'=>1))->result();
                $array = array();
                $array['category'] = $item->c_title;
                $array['sub_category'] = $sub_Category;
                $d[] = $array;
            }
            $data['additional_data'] = $d;
            $this->render_page('client/add', 'New Client',$data);
        }
    }

    function edit($client = null)
    {
        if (!isset($client)) redirect(base_url('client'));
        if (check_permission(31)) {
            $data['client'] = $this->model->get($client);
            $category = $this->db->get_where('category', array('s_id' => 1, 'c_client' => 1))->result();
            $d = array();
            foreach ($category as $item) {
                $sub_Category = $this->db->get_where('sub_category', array('c_id' => $item->c_id))->result();
                $array = array();
                $array['category'] = $item->c_title;
                $array['sub_category'] = $sub_Category;
                $array['active'] = $this->model->get_category_active($client, $item->c_id);
                $d[] = $array;
            }
            $data['additional_data'] = $d;
            $data['id'] = $client;
            $this->render_page('client/edit', 'Edit Client', $data);
        }
    }

    function do_edit()
    {
        if (check_permission(31, false)) {
            $this->form_validation->set_rules('client_name', 'Client Name', 'required|min_length[2]|max_length[99]');
            $this->form_validation->set_rules('client_email', 'Client Email', 'required|min_length[2]|max_length[99]');
            $this->form_validation->set_rules('id', 'ID', 'required');
            if ($this->form_validation->run() == TRUE) {
                $data['c_name'] = $this->input->post('client_name');
                $data['c_note'] = $this->input->post('client_note');
                $data['c_email'] = $this->input->post('client_email');
                $id = $this->input->post('id');
                $this->model->update($id, $data);
                $this->model->remove_all_categry($id);
                $categories = $this->input->post('sub_category');
                $this->model->add_categories($id, $categories);
                $output = array('result' => "1");
            } else {
                $output = array('result' => "0"
                , 'client_name' => form_error('client_name')
                , 'client_email' => form_error('client_email')
                );
            }
            echo json_encode($output);
        }
    }

    function export($type, $deleted = false)
    {
        $this->load->library("excel");
        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);
        $name_file = "Categories Data";
        if (isset($deleted) && $deleted)
            $name_file = "Categories Deleted Data";
        if ($type == "client") {

            $table_columns = array("System Id", "Name", "Email", "Note", "State", "date added", "Count Project");
            $column = 0;

            foreach ($table_columns as $field) {

                $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
                $column++;
            }
            $excel_row = 2;
            $col_ord = array(
                'c_id',
                'c_email',
                'c_name',
                'c_date_added',
                'c_note',
                's_title',
                's_id',
                'projects'
            );
            $col_search = array(
                'c_id',
                'c_email',
                'c_name',
                'c_date_added',
                'c_note',
                's_title',
                's_id',
                'projects'
            );
            $name_table = 'view_client';
            $order = array('c_id' => 'DESC');
            $where[0] = "3";
            $col_where[0] = 's_id !=';
            if ($deleted) {
                $col_where[0] = 's_id';
            }
            $joinTable = null;
            $joinCol = null;
            $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
            $data = array();
            $id = 1;
            foreach ($list as $item) {
                $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $item->c_id);
                $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $item->c_name);
                $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $item->c_email);
                $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $item->c_note);
                $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $item->s_title);
                $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $item->c_date_added);
                $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $item->projects);

                $excel_row++;
            }
            $name_file = "Client Data";
            if (isset($deleted) && $deleted)
                $name_file = "Client Deleted Data";

        } else if ($type == "project") {

        } else if ($type == "payment") {
            $table_columns = array("System Id", "Client", "Project", "State", "date added", "Budget Hours" ,"Budget Price","Project Manger","Hours",'Cost Hours','Expanse');
            $column = 0;
            foreach ($table_columns as $field) {

                $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
                $column++;
            }
            $excel_row = 2;
            $col_ord = array(
                'p_id',
                'p_title',
                'p_budget_hours',
                'p_budget_price',
                'p_note',
                'p_date_added',
                'state',
                's_id',
                'c_id',
                'client',
                'sc_id',
                'sub_category',
                'category',
                'sum_hours',
                'cost_hours',
                'expanse',
                "project_manger",
                "e_first_name",
                "e_last_name",
            );
            $col_search = array(
                'p_id',
                'p_title',
                'p_budget_hours',
                'p_budget_price',
                'p_note',
                'p_date_added',
                'state',
                's_id',
                'c_id',
                'client',
                'sc_id',
                'sub_category',
                'category',
                'sum_hours',
                'cost_hours',
                'expanse',
                "project_manger",
                "e_first_name",
                "e_last_name",
                "e_email",
            );
            $name_table = 'view_project';
            $order = array('p_id' => 'DESC');
            $where[0] = "3";
            $col_where[0] = 's_id !=';
            $joinTable = null;
            $joinCol = null;
            $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
            $data = array();
            $id = 1;

            foreach ($list as $item) {
                $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $item->p_id);
                $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $item->client);
                $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $item->p_title);
                $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $item->state);
                $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $item->p_date_added);
                $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $item->p_budget_hours);
                $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $item->p_budget_price);
                $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $item->e_email);
                $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, (float)$item->sum_hours);
                $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, (float)$item->cost_hours);
                $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row,(float) $item->expanse);

                $excel_row++;
            }
            $name_file = "Client with Project Data";
            if (isset($deleted) && $deleted)
                $name_file = "Client with Project Deleted Data";

        }

        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $name_file . '.xls"');
        $object_writer->save('php://output');

    }


}