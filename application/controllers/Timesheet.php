<?php

require_once 'Welcome.php';

class  Timesheet extends Welcome
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Myproject_model');
        $this->model = $this->Myproject_model;
    }

    function index()
    {
        $this->load->model('cost_type_model');
        $this->load->model('Hours_type_model');
        $data['expense_type'] = $this->cost_type_model->get_list();
//        $data['myproject'] = $this->model->get_myproject();
        $data['myproject'] =array();
        $data['clients'] = $this->model->get_myclient();
        $data['hours_type'] = $this->Hours_type_model->get_list();
        $this->render_page('Timesheet/index', 'Time Sheet', $data);
    }

    function datatable_hours()
    {
        $col_ord = array(
            'ph_id',
            'p_id',
            'e_id',
            'ht_id',
            'e_id',
            'le_id',
            'ph_hours',
            's_id',
            'ph_date_added',
            'employee',
            'hoursType',
            'project', 'client',
            'level_price',
            'level_title',
            'state',
            'start_time',
            'end_time', 'ph_date'
        );
        $col_search = array(
            'ph_id',
            'p_id',
            'e_id',
            'ht_id',
            'e_id',
            'le_id',
            'ph_hours',
            's_id',
            'ph_date_added',
            'employee',
            'hoursType',
            'project', 'client',
            'level_price',
            'level_title',
            'state',
            'start_time',
            'end_time', 'ph_date'
        );
        $name_table = 'view_hours_project';
        $order = array('ph_id' => 'DESC');
        $where[0] = "3";
        $col_where[0] = 's_id >';
        $where[1] = $this->session->userdata('id');
        $col_where[1] = 'e_id';
        $where[2] = $_POST['date'];
        $col_where[2] = 'ph_date';
        $joinTable = null;
        $joinCol = null;
        $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
        $data = array();
        $id = 1;
        foreach ($list as $item) {
            $row = array();
            $row['date_added'] = $item->ph_date_added;
            $row['start_time'] = $item->start_time;
            $row['end_time'] = $item->end_time;
            $row['date'] = $item->ph_date;
            $row['client'] = $item->client;
            $row['project'] = $item->project;
            $row['date'] = $item->ph_date;
            $row['level'] = $item->level_title;
            $row['hoursType'] = $item->hoursType;
            $row['hours'] = $item->ph_hours / 3600;
            $row['num'] = $id;
            $row['state'] = $item->state;
            if ($item->s_id == '4') {
                $row['state'] = '<span class="kt-badge  kt-badge--warning kt-badge--inline kt-badge--pill" id="state_' . $item->ph_id . '">' . $item->state . '</span>';
            } else if ($item->s_id == '6') {
                $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" id="state_' . $item->ph_id . '">' . $item->state . '</span>';
            } else if ($item->s_id == '5') {
                $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" id="state_' . $item->ph_id . '">' . $item->state . '</span>';
            }

            if ($item->s_id == 4)
                $row['option'] = '<button class="btn btn-outline-hover-danger btn-sm btn-icon btn-circle" style="padding:9px"  id="item_remove_ph' . $item->ph_id . '" onclick="remove_ph(' . $item->ph_id . ')">
                              <i class="fa fa fa-trash"></i>
                            </button>';

            else $row['option'] = "";
            $id++;
            $data[] = $row;
        }

        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
            "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
            "data" => $data,
        );
        echo json_encode($output);

    }

    function datatable_expense()
    {
        $col_ord = array(
            'cp_id',
            'cp_note',
            'cp_price',
            'e_id',
            'p_id',
            'clint_id',
            'client',
            'payment_type',
            'employee',
            'project_id',
            'project',
            'cp_date_added', 's_id',
        );
        $col_search = array(
            'cp_id',

            'cp_note',
            'cp_price',
            'e_id',
            'p_id',
            'clint_id',
            'client',
            'payment_type',
            'employee',
            'project_id',
            'project',
            'cp_date_added',
            's_id',
        );
        $name_table = 'view_costs';
        $order = array('cp_id' => 'DESC');
        $where[2] = "3";
        $col_where[2] = 's_id >';
        $where[1] = $this->session->userdata('id');
        $col_where[1] = 'e_id';

        $where[0] = $_POST['date'];
        $col_where[0] = 'cp_date';

        $joinTable = null;
        $joinCol = null;
        $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
//        echo  $this->db->last_query();

        $data = array();
        $id = 1;
        foreach ($list as $item) {
            $row = array();
            $row['date_added'] = $item->cp_date_added;
            $row['note'] = $item->cp_note;
            $row['client'] = $item->client;
            $row['project'] = $item->project;

            $row['price'] = $item->cp_price;
            $row['payment_type'] = $item->payment_type;
            $row['date_added'] = $item->cp_date_added;
            $row['num'] = $id;
            $row['state'] = $item->state;
            if ($item->s_id == '4') {
                $row['state'] = '<span class="kt-badge  kt-badge--warning kt-badge--inline kt-badge--pill" id="state_' . $item->cp_id . '">' . $item->state . '</span>';
            } else if ($item->s_id == '6') {
                $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" id="state_' . $item->cp_id . '">' . $item->state . '</span>';
            } else if ($item->s_id == '5') {
                $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" id="state_' . $item->cp_id . '">' . $item->state . '</span>';
            }

            if ($item->s_id == 4)
                $row['option'] = '<button class="btn btn-outline-hover-danger btn-sm btn-icon btn-circle" style="padding:9px" id="item_remove_' . $item->cp_id . '" onclick="remove_cp(' . $item->cp_id . ')">
                              <i class="fa fa fa-trash"></i>
                            </button>';
            else $row['option'] = "";
            $id++;
            $data[] = $row;
        }

        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
            "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function create_expense()
    {
        $this->form_validation->set_rules('expense_project', 'project', 'required');
        $this->form_validation->set_rules('expense_type', 'expense type', 'required');
        $this->form_validation->set_rules('expense', 'expense', 'required|numeric|is_natural_no_zero');
        $this->form_validation->set_rules('expense_date', 'date', 'required');
        if ($this->form_validation->run() == TRUE or FALSE) {
            $data['cp_price'] = $this->input->post('expense');
            $data['cp_note'] = $this->input->post('expense_note');
            $data['cp_date'] = $this->input->post('expense_date');
            $data['ct_id'] = $this->input->post('expense_type');

            $data['p_id'] = $this->input->post('expense_project');
            $data['e_id'] = $this->session->userdata('id');
//            if (check_permission(57, false)) {
//                $data['s_id'] = 5;
//            }
            $this->model->set($data);
            echo json_encode(array(
                'result' => '1',
            ));
        } else {
            echo json_encode(array(
                'result' => '0',
                'type' => form_error('expense_type'),
                'date' => form_error('expense_date'),
                'expense' => form_error('expense'),
                'project' => form_error('expense_project'),
                'title' => form_error('expense_title')
            ));
        }
    }

    function create_hours()
    {
        $this->form_validation->set_rules('hour_project', 'project', 'required');
        $this->form_validation->set_rules('hours_type', 'type', 'required');
        $this->form_validation->set_rules('start_time', 'start time', 'required|callback_checktime_start_time');
        $this->form_validation->set_rules('end_time', 'end time', 'required|callback_checktime_end_time');
        $this->form_validation->set_rules('count_hours', '', 'required|greater_than[0]');
        $this->form_validation->set_rules('date_hour', 'date', 'required');
        if ($this->form_validation->run() == TRUE ) {
            $this->load->model('Employee_model');
            $level = $this->Employee_model->get_level($this->session->userdata('id'));
            if (count($level) != 0) {
                $level = $level[0]->le_id;
            } else {
                echo json_encode(array(
                    'result' => '-1',
                    "problem" => "NOT FOUND LEVEL"
                ));
                return;
            }

            $data['le_id'] = $level;
            $data['start_time'] = $this->input->post('start_time');
            $data['end_time'] = $this->input->post('end_time');
            $data['ph_date'] = $this->input->post('date_hour');
//            $data['ph_hours'] = $this->input->post('hour_project');
            $data['ht_id'] = $this->input->post('hours_type');
            $data['p_id'] = $this->input->post('hour_project');
            $data['e_id'] = $this->session->userdata('id');
            $this->model->add_hours($data);
            echo json_encode(array(
                'result' => '1',
            ));
        } else {
            echo json_encode(array(
                'result' => '0',
                'type' => form_error('hours_type'),
                'date' => form_error('date_hour'),
                'count_hours' => form_error('count_hours'),
                'project' => form_error('hour_project'),
                'start' => form_error('start_time'),
                'end' => form_error('end_time')
            ));
        }
    }

    function checktime()
    {
        $where = array(
            'e_id' => $this->session->userdata('id'),
            'ph_date' => $this->input->post('date_hour'),
            'start_time  <= ' => $this->input->post('time'),
            'end_time  >= ' => $this->input->post('time'),
        );
        $rows = $this->db->get_where('project_hours', $where);
        if ($rows->num_rows() > 0) {
            $hours = "";
            foreach ($rows->result() as $row) {
                $hours .= "(" . $row->start_time . ' || ' . $row->end_time . ") ,";
            }
            echo json_encode(array('result' => 0, 'error' => $hours));
//            return false;
        } else {
            echo json_encode(array('result' => 1));
//            return true;
        }
    }

    function checktime_start_time()
    {

        $where = array(
            'e_id' => $this->session->userdata('id'),
            'ph_date' => $this->input->post('date_hour'),
            'start_time  <= ' => $this->input->post('start_time'),
            'end_time  >= ' => $this->input->post('start_time'),
        );
        $rows = $this->db->get_where('project_hours', $where);
//        echo  $this->db->last_query();
        if ($rows->num_rows() > 0) {
            $hours = "";
            foreach ($rows->result() as $row) {
                $hours .= "(" . $row->start_time . ' || ' . $row->end_time . ") ,";
            }
            return false;
        } else {
            return true;
        }
    }

    function checktime_end_time()
    {

        $where = array(
            'e_id' => $this->session->userdata('id'),
            'ph_date' => $this->input->post('date'),
            'start_time  <= ' => $this->input->post('end_time'),
            'end_time  >= ' => $this->input->post('end_time'),
        );
        $rows = $this->db->get_where('project_hours', $where);
        if ($rows->num_rows() > 0) {
            $hours = "";
            foreach ($rows->result() as $row) {
                $hours .= "(" . $row->start_time . ' || ' . $row->end_time . ") ,";
            }
            return false;
        } else {
            return true;
        }
    }

    function export_data()
    {
        $this->form_validation->set_rules('data_type', 'type', 'trim|required');
        $this->form_validation->set_rules('start_date', 'Start Date', 'trim|required');
        $this->form_validation->set_rules('end_date', 'End Date', 'trim|required');
        if ($this->form_validation->run() == TRUE) {
            $table = "";
            $where = "";
            $this->load->library("excel");
            $object = new PHPExcel();
            $object->setActiveSheetIndex(0);
            $column = 0;
            if ($_POST['data_type'] == "expanse") {
                $table = "view_costs";
                $this->db->where('e_id', $this->session->userdata('id'));

                $this->db->where('cp_date >=', $this->input->post('start_date'));
                $this->db->where('cp_date <=', $this->input->post('end_date'));
                $data = $this->db->get($table)->result();
//                echo $this->db->last_query();
                $table_columns = array("Date", "Type", "Client", "Project", "value", 'note', 'state');
//                print_r($data);
                foreach ($table_columns as $field) {

                    $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
                    $column++;
                }
                $excel_row = 2;
                foreach ($data as $item) {
                    $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $item->cp_date);
                    $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $item->payment_type);
                    $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $item->client);
                    $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $item->project);
                    $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $item->cp_price);
                    $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $item->cp_note);
                    $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $item->state);

                    $excel_row++;
                }

                $table = "view_hours_project";
                $where = array('ph_date  <= ' => $this->input->post('start_date'), 'ph_date   >= ' => $this->input->post('end_date'));
                $data = $this->db->where($where)->get($table)->result();
                $name_file = "(Expanse)" . $this->input->post('start_date') . " To " . $this->input->post('end_date');
            } else if ($_POST['data_type'] == "worktime") {
                $table = "view_hours_project";
                $this->db->where('e_id', $this->session->userdata('id'));
                $this->db->where('ph_date >=', $this->input->post('start_date'));
                $this->db->where('ph_date <=', $this->input->post('end_date'));
                $data = $this->db->get($table)->result();
//                                echo $this->db->last_query();

//                print_r($data);
                $table_columns = array("Date", "Type", "Client", "Project", "Count hours", 'level','start time', 'end time','state' );
                foreach ($table_columns as $field) {

                    $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
                    $column++;
                }
                $excel_row = 2;
                foreach ($data as $item) {
                    $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $item->ph_date);
                    $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $item->hoursType);
                    $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $item->client);
                    $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $item->project);
                    $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $item->ph_hours/3600);
                    $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $item->level_title);
                    $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $item->start_time);
                    $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $item->end_time);
                    $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $item->state);

                    $excel_row++;
                }

                $table = "view_hours_project";
                $where = array('ph_date  <= ' => $this->input->post('start_date'), 'ph_date   >= ' => $this->input->post('end_date'));
                $data = $this->db->where($where)->get($table)->result();
                $name_file = "(Work Time)" . $this->input->post('start_date') . " To " . $this->input->post('end_date');
            }
            $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
            header('Content-Type: application/vnd.ms-excel');

            header('Content-Disposition: attachment;filename="' . $name_file . '.xls"');
            $object_writer->save('php://output');

        } else {
            echo json_encode(array(
                'result' => 0,
                'type' => form_error('type'),
                'start_date' => form_error('start_date'),
                'end_date' => form_error('end_date')
            ));
        }
    }
    function getmyproject(){
        $client_id = $this->input->post('client_id');
        $data = $this->model->get_myproject($client_id);
        foreach ($data as $item){
            echo '<option value="'.$item->p_id.'">'.$item->project.'</option>';
        }
    }
}
