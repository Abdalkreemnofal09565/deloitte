<?php
require 'Welcome.php';

class Myproject extends Welcome
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Myproject_model');
        $this->model = $this->Myproject_model;
    }

    function index()
    {
        $this->render_page('myproject/index', 'My project');
    }

    function datatable()
    {


        $col_ord = array(
            'employee',
            'project',
            'pe_date_added',
            'pe_date_start',
            'pe_date_active',
            'pe_note',
            'pe_id',
            'e_id',
            'p_id',
            'state',
            'state_employee',
            'state_employee_no',
            'state_project',
            'state_project_no',
            'c_id',
        );
        $col_search = array(
            'employee',
            'project',
            'pe_date_added',
            'pe_date_start',
            'pe_date_active',
            'pe_note',
            'pe_id',
            'e_id',
            'p_id',
            'state',
            'state_employee',
            'state_employee_no',
            'state_project',
            'state_project_no',
            'c_id',
        );
        $name_table = 'view_employee_project';
        $order = array('p_id' => 'DESC');
        $where[0] = "1";
        $col_where[0] = 's_id';
        $where[1] = "1";
        $col_where[1] = 'state_project_no';
        $where[2] = "1";
        $col_where[2] = 'state_employee_no';
        $where[3] = $this->session->userdata('id');;
        $col_where[3] = 'e_id';
        $joinTable = null;
        $joinCol = null;
        $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
        $data = array();
        $id = 1;
        foreach ($list as $item) {
            $row = array();
            $row['date_added'] = $item->pe_date_added;
            $row['date_start'] = "<span class='text-info'>" . $item->pe_date_start . "</span>";
            $row['date_end'] = "<span class='text-danger'>" . $item->pe_date_active . "</span>";
            $row['client'] = $item->client;
            $row['project'] = $item->project;
            $row['num'] = $id;
            $row['state'] = $item->state;
            if ($item->s_id == '1') {
                $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" id="state_' . $item->p_id . '">' . $item->state . '</span>';
            } else if ($item->s_id == '2') {
                $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" id="state_' . $item->p_id . '">' . $item->state . '</span>';
            }

            $row['option'] = '<a href="' . base_url('myproject/show/' . $item->p_id) . '" class="btn btn-sm btn-outline-primary  ">
                              <i class="la la-gears"></i> Show <b>.Info</b> 
                            </a>';

            $id++;
            $data[] = $row;
        }

        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
            "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
            "data" => $data,
        );
        echo json_encode($output);

    }

    function show($project = null)
    {
        if (!isset($project)) redirect(base_url('myproject'));

        $this->load->model('cost_type_model');
        $this->load->model('Hours_type_model');
        $data['id'] = $project;
        $data['payment_type'] = $this->cost_type_model->get_list();
        $data['hours_type'] = $this->Hours_type_model->get_list();
        $data['can_added'] = 0;
        $this->load->model('Employee_model');
        $level =$this->Employee_model->get_level($this->session->userdata('id'));

        if (count($level) > 0) {
            $data['can_added'] = 1;
        }
        $this->render_page('myproject/work', 'Time work', $data);
    }

    function payments_datatable($project)
    {


        $col_ord = array(
            'cp_id',
            'cp_title',
            'cp_note',
            'cp_price',
            'e_id',
            'p_id',
            'clint_id',
            'client',
            'payment_type',
            'employee',
            'project_id',
            'project',
            'cp_date_added', 's_id',
        );
        $col_search = array(
            'cp_id',
            'cp_title',
            'cp_note',
            'cp_price',
            'e_id',
            'p_id',
            'clint_id',
            'client',
            'payment_type',
            'employee',
            'project_id',
            'project',
            'cp_date_added',
            's_id',
        );
        $name_table = 'view_costs';
        $order = array('cp_id' => 'DESC');
        $where[0] = "3";
        $col_where[0] = 's_id>';
        $where[1] = $this->session->userdata('id');
        $col_where[1] = 'e_id';

        $where[2] = $project;
        $col_where[2] = 'project_id';
        $joinTable = null;
        $joinCol = null;
        $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
        $data = array();
        $id = 1;
        foreach ($list as $item) {
            $row = array();
            $row['date_added'] = $item->cp_date_added;
            $row['note'] = $item->cp_note;
            $row['title'] = $item->cp_title;
            $row['price'] = $item   ->cp_price;
            $row['payment_type'] = $item->payment_type;
            $row['num'] = $id;
            $row['state'] = $item->state;
            if ($item->s_id == '4') {
                $row['state'] = '<span class="kt-badge  kt-badge--warning kt-badge--inline kt-badge--pill" id="state_' . $item->cp_id . '">' . $item->state . '</span>';
            } else if ($item->s_id == '6') {
                $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" id="state_' . $item->cp_id . '">' . $item->state . '</span>';
            } else if ($item->s_id == '5') {
                $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" id="state_' . $item->cp_id . '">' . $item->state . '</span>';
            }

            if ($item->s_id == 4)
                $row['option'] = '<button class="btn btn-sm btn-outline-danger btn-sm  " id="item_remove_' . $item->cp_id . '" onclick="remove_cp(' . $item->cp_id . ')">
                              <i class="la la-remove"></i>Remove
                            </button>';
            else $row['option'] = "";
            $id++;
            $data[] = $row;
        }

        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
            "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
            "data" => $data,
        );
        echo json_encode($output);

    }

    function hours_datatable($project)
    {


        $col_ord = array(
            'ph_id',
            'p_id',
            'e_id',
            'ht_id',
            'e_id',
            'le_id',
            'ph_hours',
            's_id',
            'ph_date_added',
            'employee',
            'hoursType',
            'project',
            'level_price',
            'level_title',
            'state',
        );
        $col_search = array(
            'ph_id',
            'p_id',
            'e_id',
            'ht_id',
            'e_id',
            'le_id',
            'ph_hours',
            's_id',
            'ph_date_added',
            'employee',
            'hoursType',
            'project',
            'level_price',
            'level_title',
            'state',
        );
        $name_table = 'view_hours_project';
        $order = array('ph_id' => 'DESC');
        $where[0] = "3";
        $col_where[0] = 's_id >';
        $where[1] = $this->session->userdata('id');
        $col_where[1] = 'e_id';
        $where[2] = $project;
        $col_where[2] = 'p_id';
        $joinTable = null;
        $joinCol = null;
        $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
        $data = array();
        $id = 1;
        foreach ($list as $item) {
            $row = array();
            $row['date_added'] = $item->ph_date_added;
            $row['level'] = $item->level_title;
            $row['hoursType'] = $item->hoursType;
            $row['hours'] = $item->ph_hours;
            $row['num'] = $id;
            $row['state'] = $item->state;
            if ($item->s_id == '4') {
                $row['state'] = '<span class="kt-badge  kt-badge--warning kt-badge--inline kt-badge--pill" id="state_' . $item->ph_id . '">' . $item->state . '</span>';
            } else if ($item->s_id == '6') {
                $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" id="state_' . $item->ph_id . '">' . $item->state . '</span>';
            } else if ($item->s_id == '5') {
                $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" id="state_' . $item->ph_id . '">' . $item->state . '</span>';
            }

            if ($item->s_id == 4)
                $row['option'] = '<button class="btn btn-sm btn-outline-danger btn-sm  " id="item_remove_ph' . $item->ph_id . '" onclick="remove_ph(' . $item->ph_id . ')">
                              <i class="la la-remove"></i>Remove
                            </button>';
            else $row['option'] = "";
            $id++;
            $data[] = $row;
        }

        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
            "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
            "data" => $data,
        );
        echo json_encode($output);

    }

    function add_payment()
    {
        $this->form_validation->set_rules('project', 'project', 'required');
        $this->form_validation->set_rules('title_payment', 'Title', 'required');
        $this->form_validation->set_rules('type_payment', 'Payment type', 'required');
        $this->form_validation->set_rules('payment', 'Payment', 'required|numeric|is_natural_no_zero');
        if ($this->form_validation->run() == TRUE or FALSE) {
            $data['cp_price'] = $this->input->post('payment');
            $data['cp_note'] = $this->input->post('note');
            $data['ct_id'] = $this->input->post('type_payment');
            $data['cp_title'] = $this->input->post('title_payment');
            $data['p_id'] = $this->input->post('project');
            $data['e_id'] = $this->session->userdata('id');
            if (check_permission(57, false)) {
                $data['s_id'] = 5;
            }
            $this->model->set($data);
            echo json_encode(array(
                'result' => '1',
            ));
        } else {
            echo json_encode(array(
                'result' => '0',
                'type' => form_error('type_payment'),
                'payment' => form_error('payment'),
                'project' => form_error('project'),
                'title_payment' => form_error('title_payment')
            ));
        }
    }

    function remove_payment($id)
    {
        $this->model->remove_payment($id);
    }    function remove_hours($id)
    {
        $this->model->remove_hours($id);
    }

    function add_hours()
    {
        $this->form_validation->set_rules('project', 'project', 'required');
        $this->form_validation->set_rules('type_hours', 'Type hours', 'required');
        $this->form_validation->set_rules('hours', 'hours', 'required|numeric|is_natural_no_zero');
        if ($this->form_validation->run() == TRUE) {
            $this->load->model('Employee_model');
            $level = $this->Employee_model->get_level($this->session->userdata('id'))[0]->le_id;
            $data['ht_id'] = $this->input->post('type_hours');
            $data['ph_hours'] = $this->input->post('hours');
            $data['p_id'] = $this->input->post('project');
            $data['e_id'] = $this->session->userdata('id');
            $data['le_id'] = $level;
            if (check_permission(58, false)) {
                $data['s_id'] = 5;
            }
            $this->model->add_hours($data);
            echo json_encode(array(
                'result' => '1',
            ));
        } else {
            echo json_encode(array(
                'result' => '0',
                'type' => form_error('type_hours'),
                'hours' => form_error('hours'),
                'project' => form_error('project'),
            ));

        }
    }


}