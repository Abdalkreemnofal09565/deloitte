<?php

require_once 'Welcome.php';

class Sub_category extends Welcome
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('SubCategory_model');
        $this->model = $this->SubCategory_model;
        $this->active = 20;
        $this->inactive = 21;
        $this->delete = 22;
        $this->edit = 19;
        $this->add = 18;
    }

    function index($id = null)
    {
        if (!isset($id)) {
            redirect(base_url('category'));
        }
        if (check_permission(18)) {
            $data['id'] = $id;
            $this->render_page('category/sub_category/index', 'sub category', $data);
        }
    }

    function add($id = null)
    {
        if (!isset($id)) {
            redirect(base_url('category'));
        }
        if (check_permission(18)) {
            $this->load->model('Category_model');
            $row = $this->Category_model->get($id);
            $data['title'] = $row->c_title;
            $data['id'] = $row->c_id;
            $this->render_page('category/sub_category/add', 'New Sub Category', $data);
        }

    }

    function create()
    {
        $this->form_validation->set_rules('category_id', 'Category', 'required');
        $this->form_validation->set_rules('SubCategory', 'Sub Category', 'required|min_length[3]|max_length[99]');
        if ($this->form_validation->run() == true) {
            $data ["c_id"] = $this->input->post("category_id");
            $data['sc_title'] = $this->input->post('SubCategory');
            $this->model->set($data);
            $output = array('result' => "1");
        } else {
            $output = array('result' => "0", "category" => form_error('category_id'), 'sub_category' => form_error('SubCategory'));
        }
        echo json_encode($output);
    }

    function datatable($id)
    {
        $output = array();
        if (check_permission(18, false)) {
            $col_ord = array(
                'c_id',
                'c_title',
                'sc_date_added',
                's_id',
                's_title',
                'sc_title'

            );
            $col_search = array(
                'c_id',
                'c_title',
                'sc_date_added',
                's_id',
                's_title',
                'sc_title'
            );
            $name_table = 'view_sub_category';
            $order = array('sc_id' => 'DESC');
            $where[0] = "3";
            $col_where[0] = 's_id !=';
            $where[1] = $id;
            $col_where[1] = 'c_id';
            $joinTable = null;
            $joinCol = null;
            $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
            $data = array();
            $id = 1;
            foreach ($list as $item) {
                $row = array();
                $row['title'] = $item->sc_title;
                $row['date_register'] = $item->sc_date_added;
                $row['num'] = $id;
                $row['category'] = $item->c_title;
                $row['state'] = $item->s_title;
                if ($item->s_id == '1') {
                    $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" id="state_' . $item->sc_id . '">' . $item->s_title . '</span>';
                } else if ($item->s_id == '2') {
                    $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" id="state_' . $item->sc_id . '">' . $item->s_title . '</span>';
                }

                $row['option'] = '<span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">';
                //edit
                if (check_permission($this->edit)) {
                    $row['option'] .= '                <a class="dropdown-item" href="' . base_url("Sub_category/edit/" . $item->sc_id) . '"><i class="la la-edit"></i> Edit Details</a>';
                }
                //inactive
                if ($item->s_id == '1' && check_permission($this->inactive)) {
                    $row['option'] .= ' <a class="dropdown-item" onclick="item_inactive(' . $item->sc_id . ')"   ><i class="la la-times-circle"></i> Inactive</a>';
                } //active
                else if (check_permission($this->active)) {
                    $row['option'] .= '  <a class="dropdown-item" onclick="item_active(' . $item->sc_id . ')"  ><i class="la la-check"></i> Active</a>';
                }

                //delete
                if (check_permission($this->delete)) {
                    $row['option'] .= '  <a class="dropdown-item" onclick="delete_item(' . $item->sc_id . ')"  ><i class="la la-remove"></i> Delete</a>';
                }
                $row['option'] .= '</div></span>';

                $id++;
                $data[] = $row;
            }

            $output = array(
                "draw" => @$_POST['draw'],
                "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
                "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
                "data" => $data,
            );
        }
        echo json_encode($output);

    }

    function get_options()
    {
        $category = $this->input->post('category');
        $sub_categories = $this->db->get_where('sub_category', array('c_id' => $category))->result();
        $response = "";
        foreach ($sub_categories as $item) {
            $response .= "<option value='" . $item->sc_id . "'>" . $item->sc_title . "</option>";
        }
        echo $response;
    }

    function edit($id)
    {
        $sub_category = $this->db->get_where('view_sub_category', array('sc_id' => $id));
        if ($sub_category->num_rows() == 0) {
            redirect(base_url('Category'));
        }

        $data['sub_category'] = $sub_category->row();
        $this->render_page('category/sub_category/edit', $sub_category->row()->sc_title, $data);
    }

    function do_edit()
    {
        $this->form_validation->set_rules('sub_category_id', 'sub category id', 'trim|required');
        $this->form_validation->set_rules('SubCategory', 'Sub Category', 'trim|required');
        if ($this->form_validation->run() == TRUE or FALSE) {
            $id = $this->input->post('sub_category_id');
            $data['sc_title'] = $this->input->post('SubCategory');
            $bool = $this->model->update($id, $data);
            if ($bool) {
                echo json_encode(array('result' => 1));
            } else {
                echo json_encode(array('result' => 0));
            }

        } else {
            echo json_encode(array('result' => 0, 'sub_category' => form_error('SubCategory')));
        }
    }

    function get_options_array()
    {
        $sub_categories = $this->db->where_in('sc_id', $_POST['category'])->where('s_id',1)->get('sub_category')->result();
        $response="";
        foreach ($sub_categories as $item) {
            $response .= "<option value='" . $item->sc_id . "'>" . $item->sc_title . "</option>";
        }
        echo $response;

    }


}