<?php
require_once 'Welcome.php';

class Report extends Welcome
{
    private $client;
    private $project;
    private $employee;

    public function __construct()
    {
        parent::__construct();
        $this->client = 83;
        $this->employee = 84;
        $this->project = 85;
    }

    function client()
    {
        if (check_permission($this->client)) {
            $this->load->model('Category_model');
            $this->load->model('Client_model');
            $data['categories'] = $this->db->get_where('category', array('s_id' => 1, 'c_project' => 1))->result();
            $data['clients'] = $this->Client_model->get_list();
            $this->render_page('report/client', 'Reports', $data);
        }
    }

    function employee()
    {
        if (check_permission($this->employee)) {
            $this->load->model('Employee_model');
            $data['employee'] = $this->Employee_model->get_list();
            $this->render_page('report/employee', 'Reports', $data);
        }
    }

    function get_report_client()
    {
        $client = array();
        $category = array();
        $sub_category = array();
        $projects = array();
        if (isset($_POST['client'])) $client = $_POST['client'];
//        if (isset($_POST['category'])) $category = $_POST['category'];
        if (isset($_POST['sub_category'])) $sub_category = $_POST['sub_category'];
        if (isset($_POST['projects'])) $projects = $_POST['projects'];
//        print_r($sub_category);
        $this->load->model('Report_model');
        $type = $this->input->post('type');
        $data['reports'] = $this->Report_model->client($client, $projects, $category, $sub_category, $type);
        if ($type == "all") {
            $data['categories'] = $cate = $this->db->get_where('category', array('s_id' => 1, 'c_project' => 1))->result();
            $this->load->view('report/report_client', $data);
        } else if ($type == "employee_expanse") {
            $this->load->view('report/client_employee_expanse', $data);
        } else if ($type == "employee_worktime") {
            $this->load->view('report/client_employee_worktime', $data);
        } else if ($type == "client_statement_account") {
            $this->load->view('report/client_statement_account', $data);
        }

    }

    function get_report_employee()
    {
        $this->load->model('Report_model');
        $type = $this->input->post('option');
        $employee = array();
        if (isset($_POST['employees']))
            $employee = $_POST['employees'];

        $data['reports'] = $this->Report_model->get_employee($employee, $type);

        if ($type == 1) {
            $this->load->view('report/employees_report', $data);
        } else if ($type == 2) {
            $this->load->view('report/expanse_employee', $data);
        } else if ($type == 3) {
            $this->load->view('report/hours_employees', $data);
        }
    }
}