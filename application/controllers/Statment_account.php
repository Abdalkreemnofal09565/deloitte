<?php

require_once 'Welcome.php';

class Statment_account extends Welcome
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Payment_model');
        $this->model = $this->Payment_model;
        $this->add = 82;
        $this->view = 77;
        $this->edit = 81;
        $this->delete = 78;
        $this->active = 79;
        $this->inactive = 80;
    }

    function index($id = null, $manger = null)
    {
        if (!isset($id)) {
            redirect(base_url('client'));
        }
        if (check_permission($this->view) || (isset($manger) && $manger == "manger" && $this->db->get_where('project', array('p_id' => $id, 'project_manger' => $this->session->userdata('id')))->num_rows() == 1)) {
            $data['id'] = $id;
            $this->render_page('payment/index', 'Payment', $data);
        }
    }

    function add($id = null)
    {
        if (!isset($id)) {
            redirect(base_url('client'));
        }
        if (check_permission($this->add)) {
            $this->load->model('Payment_type_model');
            $type = $this->Payment_type_model->get_list($id);
            $data['types'] = $type;
            $data['id'] = $id;
            $this->render_page('payment/add', 'New Payment', $data);
        }

    }

    function create()
    {
        if (check_permission($this->add)) {
            $this->form_validation->set_rules('date', 'date', 'required');
            $this->form_validation->set_rules('payment', 'payment', 'required');
            $this->form_validation->set_rules('Type', 'Type', 'required');
            $this->form_validation->set_rules('project', 'project', 'required');
            if ($this->form_validation->run() == true) {
                $data ["p_date"] = $this->input->post("date");
                $data ["pt_id"] = $this->input->post("Type");
                $data ["p_id"] = $this->input->post("project");
                $data ["reference"] = $this->input->post("reference");
                $data['pay_payment'] = $this->input->post('payment');
                $this->model->set($data);
                $output = array('result' => "1");
            } else {
                $output = array('result' => "0",
                    "title" => form_error('title'),
                    "project" => form_error('project'),
                    "payment" => form_error('payment'),
                    'Type' => form_error('Type'));
            }
            echo json_encode($output);
        }
    }

    function do_edit()
    {
        if (check_permission($this->edit)) {
            $this->form_validation->set_rules('date', 'date', 'required');
            $this->form_validation->set_rules('payment', 'payment', 'required');
            $this->form_validation->set_rules('Type', 'Type', 'required');
            $this->form_validation->set_rules('id', 'project', 'required');
            if ($this->form_validation->run() == true) {
                $data ["pt_id"] = $this->input->post("Type");
                $data ["p_date"] = $this->input->post("date");
                $data['pay_payment'] = $this->input->post('payment');
                $data ["reference"] = $this->input->post("reference");
                $this->model->update($this->input->post('id'), $data);
                $output = array('result' => "1");
            } else {
                $output = array('result' => "0",
                    "project" => form_error('project'),
                    "payment" => form_error('payment'),
                    'Type' => form_error('Type'));
            }
            echo json_encode($output);
        }
    }

    function datatable($id)
    {
        $output = array();
        if (check_permission($this->view, false)) {
            $p_edit = check_permission($this->edit, false);
            $p_active = check_permission($this->active, false);
            $p_inactive = check_permission($this->inactive, false);
            $p_delete = check_permission($this->delete, false);
            $col_ord = array(
                'pay_id',
                'p_date',
                'pay_date_added',
                's_id',
                's_title',
                'pt_id',
                'pt_title',
                'pt_type',
                'p_title',
                'c_name',
                'reference',

            );
            $col_search = array(
                'pay_id',
                'p_date',
                'pay_date_added',
                's_id',
                's_title',
                'pt_id',
                'pt_title',
                'pt_type',
                'p_title',
                'c_name',
                'reference',
            );
            $name_table = 'view_payment';
            $order = array('p_date' => 'AEC');
            $where[0] = "3";
            $col_where[0] = 's_id !=';
            $where[1] = $id;
            $col_where[1] = 'p_id';
            $joinTable = null;
            $joinCol = null;
            $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
            $data = array();
            $id = 1;
            $balance = 0;
            foreach ($list as $item) {
                $row = array();
                $row['date'] = $item->p_date;
                $row['payment'] = $item->pay_payment;
                $row['date_register'] = $item->pay_date_added;
                $row['reference'] = $item->reference;
                $row['num'] = $id;
                $row['type'] = $item->pt_title;
                if ($item->pt_type == 0) {
                    $row['Credit'] = "<i class='la la-minus'></i>";
                    $row['Debit'] = $item->pay_payment;
                    if ($item->s_id == 1)
                        $balance += $item->pay_payment;
                } else {
                    $row['Credit'] = $item->pay_payment;
                    $row['Debit'] = "<i class='la la-minus'></i>";
                    if ($item->s_id == 1)
                        $balance -= $item->pay_payment;
                }
                $row['Balance'] = $balance;
                $row['state'] = $item->s_title;
                if ($item->s_id == '1') {
                    $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" id="state_' . $item->pay_id . '">' . $item->s_title . '</span>';
                } else if ($item->s_id == '2') {
                    $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" id="state_' . $item->pay_id . '">' . $item->s_title . '</span>';
                }
                $row['s'] = $item->s_id;
                $row['option'] = '<span class="dropdown"><a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"><i class="la la-ellipsis-h"></i></a><div class="dropdown-menu dropdown-menu-right">';
                if ($p_edit) $row['option'] .= '<a class="dropdown-item" href="' . base_url('Statment_account/edit/' . $item->pay_id) . '"><i class="la la-edit"></i> Edit Details</a>';
                if ($p_delete) $row['option'] .= '<a class="dropdown-item" onclick="delete_item(' . $item->pay_id . ')"><i class="la la-remove" ></i > Delete</a >';
                if ($p_active && $item->s_id == '2') $row['option'] .= '<a class="dropdown-item" onclick="item_active(' . $item->pay_id . ')"><i class="la la-check" ></i > Active</a >';
                if ($p_inactive && $item->s_id == '1') $row['option'] .= '<a class="dropdown-item" onclick="item_inactive(' . $item->pay_id . ')"><i class="la la-times-circle" ></i > Inactive</a >';
                $row['option'] .= '</div ></span > ';


                $id++;
                $data[] = $row;
            }

            $output = array(
                "draw" => @$_POST['draw'],
                "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
                "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
                "data" => $data,
            );
        }
        echo json_encode($output);

    }

    function get_options()
    {
        $category = $this->input->post('category');
        $sub_categories = $this->db->get_where('sub_category', array('c_id' => $category))->result();
        $response = "";
        foreach ($sub_categories as $item) {
            $response .= "<option value='" . $item->sc_id . "'>" . $item->sc_title . "</option>";
        }
        echo $response;
    }

    function edit($id)
    {
        if (check_permission($this->edit)) {
            $payment = $this->db->get_where('payments', array('pay_id' => $id));
            if ($payment->num_rows() == 0) redirect(base_url('client'));
            $this->load->model('Payment_type_model');
            $type = $this->Payment_type_model->get_list($id);
            $data['types'] = $type;
            $data['id'] = $id;
            $data['payment'] = $payment->row();
            $data ["reference"] = $this->input->post("reference");
            $this->render_page('payment/edit', 'New Payment', $data);
        }
    }

    function export($project)
    {
        $this->load->library("excel");
        $object = new PHPExcel();
        $styleArray = array(
            'font'  => array(
                'bold' => true,
            ));
        $object->setActiveSheetIndex(0);
        $object->getActiveSheet()->setCellValue('A1', 'Statement of account');
        $object->getActiveSheet()->setCellValue('A2', 'Client');
        $object->getActiveSheet()->setCellValue('A3', 'Project');
        $object->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
        $object->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
        $object->getActiveSheet()->getStyle('A3')->applyFromArray($styleArray);
        $object->getActiveSheet()->getStyle('B5')->applyFromArray($styleArray);
        $object->getActiveSheet()->getStyle('C5')->applyFromArray($styleArray);
        $object->getActiveSheet()->getStyle('D5')->applyFromArray($styleArray);
        $object->getActiveSheet()->getStyle('E5')->applyFromArray($styleArray);
        $object->getActiveSheet()->getStyle('F5')->applyFromArray($styleArray);

        $p = $this->db->get_where('view_project', array('p_id' => $project))->row();
        $object->getActiveSheet()->setCellValue('B3', $p->p_title);
        $object->getActiveSheet()->setCellValue('B2', $p->client);
        $column = 0;

        $table_columns = array("", "Reference", "Date", "Debit", "Credit", 'Balance');
        foreach ($table_columns as $field) {
            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 5, $field);
            $column++;
        }


        $data = $this->db->get_where('view_payment', array('p_id' => $project))->result();
        $excel_row = 7;
        $balance = 0;
        foreach ($data as $item) {
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $item->pt_title);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $item->reference);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $item->p_date);
            if ($item->pt_type == 0) {
                $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $item->pay_payment);
                $balance += $item->pay_payment;
            } else {
                $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $item->pay_payment);
                $balance -= $item->pay_payment;
            }

            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $balance);
            $excel_row++;
        }
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');

        header('Content-Disposition: attachment;filename="' .  $p->p_title."(".date('y/m/d',time()).")" . '.xls"');
        $object_writer->save('php://output');
    }
}