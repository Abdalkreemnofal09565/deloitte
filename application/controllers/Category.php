<?php

include_once 'Welcome.php';

class  Category extends Welcome
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Category_model');
        $this->model = $this->Category_model;
        $this->view = "11";
        $this->edit = "12";
        $this->add = "13";
        $this->active = "14";
        $this->inactive = "15";
        $this->delete = "16";
    }


    function index()
    {
        if (check_permission($this->view) == true) {
            $this->render_page('category/index', 'Categories');
        }
    }

    function not_found_dot()
    {
        $this->form_validation->set_message('not_found_dot', 'Title should not contain a dot');

        $s = $this->input->post('Category');
        $c = strlen($s);
        for ($i = 0; $c > $i; $i++) {
            if ($s[$i] == '.') return false;
        }
        return true;
    }

    function create()
    {

        $this->form_validation->set_rules('Category', 'Category', 'required|min_length[3]|max_length[99]|callback_not_found_dot');
        if ($this->form_validation->run() == true) {
            $data ["c_title"] = $this->input->post("Category");
            if (isset($_POST['client']) && $_POST['client'] == "on") {
                $data['c_client'] = 1;
            } else {
                $data['c_client'] = 0;

            }
            if (isset($_POST['project']) && $_POST['project'] == "on") {
                $data['c_project'] = 1;
            } else {
                $data['c_project'] = 0;

            }
            if (isset($_POST['employee']) && $_POST['employee'] == "on") {
                $data['c_employee'] = 1;
            } else {
                $data['c_employee'] = 0;

            }
            $this->model->set($data);
            $output = array('result' => "1");
        } else {
            $output = array('result' => "0", "category" => form_error('Category'));
        }
        echo json_encode($output);
    }

    function add()
    {
        if (check_permission($this->add)) {
            $this->render_page('category/add', 'New Category');
        }
    }

    function datatable($delete = null)
    {
        if (check_permission($this->view, false)) {
            $edit = check_permission($this->edit, false);
            $active = check_permission($this->active, false);
            $inactive = check_permission($this->inactive, false);
            $deleted = check_permission($this->delete, false);
            $sub_category = check_permission('17', false);
            $col_ord = array(
                'c_id',
                'c_title',
                'c_employee',
                'c_client',
                'c_project',
                'c_date_added',
                's_id',
                's_title',
                'cnt_sub_category'

            );
            $col_search = array(
                'c_id',
                'c_title',
                'c_employee',
                'c_client',
                'c_project',
                'c_date_added',
                's_id',
                's_title',
                'cnt_sub_category'
            );
            $name_table = 'view_category';
            $order = array('c_title' => 'DESC');
            if (!isset($delete)) {
                $where[0] = "3";
                $col_where[0] = 's_id !=';
            } elseif ($delete == "deleted") {
                $where[0] = "3";
                $col_where[0] = 's_id';
            }
            $joinTable = null;
            $joinCol = null;
            $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
            $data = array();
            $id = 1;
            foreach ($list as $item) {
                $row = array();
                $row['title'] = $item->c_title;
                $row['date_register'] = $item->c_date_added;
                $row['num'] = $id;
                $row['count_sub_category'] = $item->cnt_sub_category;
                $row['state'] = $item->s_title;
                $row['project'] = "<i class='la-close   la text-danger'></i>";
                if ($item->c_project) $row['project'] = "<i class='la-check la  text-success'></i>";
                $row['client'] = "<i class='la-close   la text-danger'></i>";
                if ($item->c_client) $row['client'] = "<i class='la-check la  text-success'></i>";
                $row['employee'] = "<i class='la-close   la text-danger'></i>";
                if ($item->c_employee) $row['employee'] = "<i class='la-check la  text-success'></i>";
                if ($item->s_id == '1') {
                    $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" id="state_' . $item->c_id . '">' . $item->s_title . '</span>';
                } else if ($item->s_id == '2') {
                    $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" id="state_' . $item->c_id . '">' . $item->s_title . '</span>';
                }
                $row['option'] = '<span class="dropdown " id="option_' . $item->c_id . '">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">';

                //check permission edit
                if ($edit)
                    $row['option'] .= '<a class="dropdown-item" href="' . base_url('category/edit/' . $item->c_id) . '"><i class="la la-edit"></i> Edit Details</a>';
                //check permission remove or delete
                if ($deleted)
                    $row['option'] .= '<a class="dropdown-item" onclick="delete_item(' . $item->c_id . ')" ><i class="la la-remove"></i> Delete</a>';
                // check permission change state
                if ($item->s_id == '1' && $inactive)
                    $row['option'] .= '<a class="dropdown-item" onclick="item_inactive(' . $item->c_id . ')"><i class="la la-times-circle"></i> UnActive</a>';
                else if ($active)
                    $row['option'] .= '<a class="dropdown-item" onclick="item_active(' . $item->c_id . ')"><i class="la la-check"></i> Active</a>';
                // check permission sub category view
                if ($sub_category)
                    $row['option'] .= '<a class="dropdown-item" href="' . base_url('Sub_category/index/' . $item->c_id) . '"><i class="la la-clone"></i> Values</a>';
                $row['option'] .= ' </div>
                        </span>';

                $id++;
                $data[] = $row;
            }

            $output = array(
                "draw" => @$_POST['draw'],
                "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
                "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
                "data" => $data,
            );
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    function edit($id)
    {
        if (check_permission($this->edit) == true) {
            $category = $this->db->where('c_id', $id)->get('category')->row();
            if ($category == null) redirect(base_url('category'));
            $data['category'] = $category;
            $this->render_page('category/edit', "Edit Category", $data);
        }
    }

    function do_edit()
    {
        if (check_permission($this->edit, false)) {
            $this->form_validation->set_rules('Category', 'Category', 'required|min_length[3]|max_length[99]|callback_not_found_dot');
            $this->form_validation->set_rules('category_id', 'id', 'required');
            if ($this->form_validation->run() == true) {
                $data['c_title'] = $this->input->post('Category');
                if (isset($_POST['client']) && $_POST['client'] == "on") {
                    $data['c_client'] = 1;
                } else {
                    $data['c_client'] = 0;

                }
                if (isset($_POST['project']) && $_POST['project'] == "on") {
                    $data['c_project'] = 1;
                } else {
                    $data['c_project'] = 0;

                }
                if (isset($_POST['employee']) && $_POST['employee'] == "on") {
                    $data['c_employee'] = 1;
                } else {
                    $data['c_employee'] = 0;

                }
                $id = $this->input->post("category_id");
                $this->model->update($id, $data);
                echo json_encode(array('result' => '1'));
            } else {
                echo json_encode(array('result' => "0", "category" => form_error('Category')));
            }
        } else {
            echo json_encode(array('result' => '0'));
        }

    }

    function deleted()
    {
        if (check_permission($this->view)) {
            $this->render_page('category/list_deleted', 'List Deleted');
        }
    }

    function export($delete = null)
    {
//        $this->load->model("excel_export_model");
        $this->load->library("excel");
        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);
        $table_columns = array("System Id", "Category", "State", "date added", "count sub category");
        $column = 0;

        foreach ($table_columns as $field) {

            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }
        $excel_row = 2;
        $col_ord = array(
            'c_id',
            'c_title',
            'c_date_added',
            's_id',
            's_title',
            'cnt_sub_category'

        );
        $col_search = array(
            'c_id',
            'c_title',
            'c_date_added',
            's_id',
            's_title',
            'cnt_sub_category'
        );
        $name_table = 'view_category';
        $order = array('c_id' => 'DESC');
        if (!isset($delete)) {
            $where[0] = "3";
            $col_where[0] = 's_id !=';
        } elseif ($delete == "deleted") {
            $where[0] = "3";
            $col_where[0] = 's_id';
        }
        $joinTable = null;
        $joinCol = null;
        $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
        $data = array();
        $id = 1;
        foreach ($list as $item) {
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $item->c_id);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $item->c_title);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $item->s_title);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $item->c_date_added);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $item->cnt_sub_category);

            $excel_row++;
        }
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        $name_file = "Categories Data";
        if (isset($delete) && $delete == "deleted")
            $name_file = "Categories Deleted Data";

        header('Content-Disposition: attachment;filename="' . $name_file . '.xls"');
        $object_writer->save('php://output');

    }

    function export_with_sub_categries($delete = null)
    {
//        $this->load->model("excel_export_model");
        $this->load->library("excel");
        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);
        $table_columns = array("System Id (Category)", "Category", "System Id (Sub Category)", "Sub Category", "State", "date added");
        $column = 0;

        foreach ($table_columns as $field) {

            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }
        $excel_row = 2;
        $col_ord = array(
            'c_id',
            'c_title',
            'sc_date_added',
            's_id',
            's_title',
            'sc_title',
            'sc_id'

        );
        $col_search = array(
            'c_id',
            'c_title',
            'sc_date_added',
            's_id',
            's_title',
            'sc_title', 'sc_id'
        );
        $name_table = 'view_sub_category';
        $order = array('sc_id' => 'DESC');
        $where[0] = "3";
        $col_where[0] = 's_id !=';
        $joinTable = null;
        $joinCol = null;
        $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
        $data = array();
        $id = 1;
        foreach ($list as $item) {
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $item->c_id);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $item->c_title);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $item->sc_id);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $item->sc_title);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $item->s_title);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $item->sc_date_added);
            $excel_row++;
        }
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        $name_file = "Categories Data";
        if (isset($delete) && $delete == "deleted")
            $name_file = "Categories Deleted Data";

        header('Content-Disposition: attachment;filename="' . $name_file . '.xls"');
        $object_writer->save('php://output');

    }

}