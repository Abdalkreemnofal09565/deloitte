<?php
require_once 'Welcome.php';

class  Project extends Welcome
{
    private $view_emplyee;
    private $view_hours;
    private $payment;

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Project_model");
        $this->model = $this->Project_model;
        $this->view = 46;
        $this->edit = 42;
        $this->add = 47;
        $this->active = 44;
        $this->inactive = 43;
        $this->delete = 45;
        $this->view_emplyee = 48;
        $this->view_hours = 56;
        $this->payment = 77;

        $this->inactive = 55;
    }

    function index($id = null)
    {
        if (!isset($id)) redirect(base_url('client'));
        if (check_permission(46) == true) {
            $data['id'] = $id;
            $category = $this->db->get_where('category', array('s_id' => 1, 'c_project' => 1))->result();
            $data['category'] = $category;
            $this->render_page('project/index', 'Project', $data);
        }
    }


    function manger()
    {
        if (isset($_SESSION['project_manger']) && $_SESSION['project_manger']) {
            $category = $this->db->get_where('category', array('s_id' => 1, 'c_project'=>1))->result();
            $data['categories'] = $category;
            $this->render_page('project_manger/index', 'project manger',$data);
        }

    }

    function add($id)
    {
        if (check_permission(47) == true) {
            $data['client'] = $id;
            $this->load->model('Category_model');
            $data['categories'] = $this->Category_model->get_list();
            $data['project_manger'] = $this->db->get_where('employee', array('s_id' => 1))->result();
            $category = $this->db->get_where('category', array('s_id' => 1, 'c_project' => 1))->result();
            $d = array();
            foreach ($category as $item) {
                $sub_Category = $this->db->get_where('sub_category', array('c_id' => $item->c_id, 's_id' => 1))->result();
                $array = array();
                $array['category'] = $item->c_title;
                $array['sub_category'] = $sub_Category;
                $array['active'] = "";

                $d[] = $array;
            }
            $data['additional_data'] = $d;
            $this->render_page('project/add', 'Project', $data);
        }
    }

    function edit($id)
    {
        if (check_permission($this->edit) == true) {
            $project = $this->db->get_where('view_project', array('p_id' => $id));
            if ($project->num_rows() == 0) redirect(base_url('client'));
            $project = $project->row();
            $data['project_id'] = $id;
            $this->load->model('Category_model');
            $data['categories'] = $this->Category_model->get_list();
            $data['project_manger'] = $this->db->get_where('employee', array('s_id' => 1))->result();
            $data['project'] = $project;
            $data['client'] = $project->c_id;
            $category = $this->db->get_where('category', array('s_id' => 1, 'c_project' => 1))->result();
            $d = array();
            foreach ($category as $item) {
                $sub_Category = $this->db->get_where('sub_category', array('c_id' => $item->c_id))->result();
                $array = array();
                $array['category'] = $item->c_title;
                $array['sub_category'] = $sub_Category;
                $array['active'] = $this->model->get_category_active($id, $item->c_id);
                $d[] = $array;
            }
            $data['additional_data'] = $d;
            $this->render_page('project/edit', 'Project', $data);
        }
    }

    function datatable($id)
    {
        if (check_permission($this->view, false)) {
            $p_employee = check_permission($this->view_emplyee, false);
            $p_payment = check_permission($this->payment, false);
            $p_active = check_permission($this->active, false);
            $p_deleted = check_permission($this->delete, false);
            $p_inactive = check_permission($this->inactive, false);
            $p_edit = check_permission($this->edit, false);
            $p_statement_Account = check_permission(95, false);

            $col_ord = array(
                'p_id',
                'p_title',
                'p_budget_hours',
                'p_budget_price',
                'p_note',
                'p_date_added',
                'state',
                's_id',
                'c_id',
                'client',
                'e_first_name',
                'e_last_name'
            );
            $col_search = array(
                'p_id',
                'p_title',
                'p_budget_hours',
                'p_budget_price',
                'p_note',
                'p_date_added',
                'state',
                's_id',
                'c_id',
                'client',
                'e_first_name',
                'e_last_name'

            );
            $name_table = 'view_project';
            $order = array('p_id' => 'DESC');
            $where[0] = "3";
            $col_where[0] = 's_id !=';
            $col_where[1] = "c_id";
            $where[1] = $id;
            $joinTable = null;
            $joinCol = null;
            $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
            $data = array();
            $id = 1;
            $this->load->model('Report_model');
            $category = $this->db->get_where('category', array('s_id' => 1, 'c_project' => 1))->result();

            foreach ($list as $item) {
                $row = array();
                $row['id'] = $item->p_id;
                $row['title'] = $item->p_title;;
                $row['budget_hours'] = $item->p_budget_hours;;
                $row['budget_price'] = $item->p_budget_price;;
                $row['date_register'] = $item->p_date_added;
                $row['client'] = $item->client;
                foreach ($category as $item3) {
                    $row[$item3->c_title . ''] = $this->model->get_category($item->p_id, $item3->c_id);
                }
                $row['note'] = $item->p_note;
                $row['hours'] = $this->Report_model->hours($item->p_id);
                $row['cost_hours'] = $this->Report_model->cost_hours($item->p_id);
                $row['expanse'] = $this->Report_model->expanse($item->p_id);
                $row['payment_d'] = $this->Report_model->payment($item->p_id, true);
                $row['payment_i'] = $this->Report_model->payment($item->p_id);
                $row['invoice'] = "";
                $row['project_manger'] = "<span class='text-muted'>" . $item->e_first_name . " " . $item->e_last_name . "</span>";

                $row['num'] = $id;
                $row['state'] = $item->state;
                if ($item->s_id == '1') {
                    $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill  " id="state_' . $item->p_id . '">' . $item->state . '</span>';
                } else if ($item->s_id == '2') {
                    $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" id="state_' . $item->p_id . '">' . $item->state . '</span>';
                }

                $row['option'] = '<span class="dropdown"><a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"><i class="la la-ellipsis-h"></i></a><div class="dropdown-menu dropdown-menu-right">';
                if ($p_employee)
                    $row['option'] .= '<a class="dropdown-item" href="' . base_url('project/employee/' . $item->p_id) . '"><i class="la la-users "></i> Employees</a>';

                if ($p_statement_Account)
                    $row['option'] .= '  <a class="dropdown-item" href="' . base_url('Statment_account/' . $item->p_id) . '"><i class=" flaticon flaticon-coins "></i> Statement account</a>';
                if ($p_statement_Account)
                    $row['option'] .= '  <a class="dropdown-item" href="' . base_url('Statment_account/export/' . $item->p_id) . '"><i class=" flaticon flaticon-coins "></i>Exprot Statement account</a>';

                if ($p_edit)
                    $row['option'] .= ' <a class="dropdown-item" href="' . base_url('project/edit/' . $item->p_id) . '"><i class="la la-edit"></i> Edit Details</a>';
                if ($p_deleted)
                    $row['option'] .= '  <a class="dropdown-item text-muted" onclick="delete_item(' . $item->p_id . ')"><i class="la la-remove"></i> Delete</a>';
                if ($p_active && $item->s_id == '2')
                    $row['option'] .= ' <a class="dropdown-item text-muted" onclick="item_active(' . $item->p_id . ')"><i class="la la-check" ></i> Active</a>';
                if ($p_inactive && $item->s_id == '1')
                    $row['option'] .= '  <a class="dropdown-item text-muted" onclick="item_inactive(' . $item->p_id . ')"><i class="la la-times-circle" ></i> InActive</a>';


                $row['option'] .= '</div></span>';

                $id++;
                $data[] = $row;
            }
            $output = array(
                "draw" => @$_POST['draw'],
                "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
                "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
                "data" => $data,
            );
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    function datatable_project_manger()
    {
        if (check_permission($this->view, false)) {

            $this->load->model('Report_model');

            $col_ord = array(
                'p_id',
                'p_title',
                'p_budget_hours',
                'p_budget_price',
                'p_note',
                'p_date_added',
                'state',
                's_id',
                'c_id',
                'client',
//                'sc_id',
//                'sub_category',
//                'category',

                'e_first_name',
                'e_last_name'
            );
            $col_search = array(
                'p_id',
                'p_title',
                'p_budget_hours',
                'p_budget_price',
                'p_note',
                'p_date_added',
                'state',
                's_id',
                'c_id',
                'client',
//                'sc_id',
//                'sub_category',
//                'category',

                'e_first_name',
                'e_last_name'

            );
            $name_table = 'view_project';
            $order = array('p_id' => 'DESC');
            $where[0] = "3";
            $col_where[0] = 's_id !=';
            $col_where[1] = "project_manger";
            $where[1] = $this->session->userdata('id');
            $joinTable = null;
            $joinCol = null;
            $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
            $data = array();
            $id = 1;
            $category = $this->db->get_where('category', array('s_id' => 1, 'c_project'=>1))->result();
            foreach ($list as $item) {
                $row = array();
                $row['id'] = $item->p_id;
                $row['title'] = $item->p_title;;
                $row['budget_hours'] = $item->p_budget_hours;;
                $row['budget_price'] = $item->p_budget_price;;
                $row['date_register'] = $item->p_date_added;
                $row['client'] = $item->client;
                $row['category'] = "";
                $row['sub_category'] = "";
                foreach ($category as $item2) {
                    $row[$item2->c_title . ""] = $this->model->get_category_active($item->p_id,$item2->c_id);
                }
                $row['p_id'] = $item->p_id;
                $row['note'] = $item->p_note;
                $row['hours'] = $this->Report_model->hours($item->p_id);
                $row['cost_hours'] = $this->Report_model->cost_hours($item->p_id);
                $row['expanse'] = $this->Report_model->expanse($item->p_id);
                $row['payment_d'] = $this->Report_model->payment($item->p_id, true);
                $row['payment_i'] = $this->Report_model->payment($item->p_id);

                $row['project_manger'] = "<span class='text-muted'>" . $item->e_first_name . " " . $item->e_last_name . "</span>";

                $row['num'] = $id;
                $row['state'] = $item->state;
                if ($item->s_id == '1') {
                    $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill  " id="state_' . $item->p_id . '">' . $item->state . '</span>';
                } else if ($item->s_id == '2') {
                    $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" id="state_' . $item->p_id . '">' . $item->state . '</span>';
                }

                $row['option'] = '<span class="dropdown"><a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"><i class="la la-ellipsis-h"></i></a><div class="dropdown-menu dropdown-menu-right">';

                $row['option'] .= '<a class="dropdown-item" href="' . base_url('project/employee/' . $item->p_id . '/manger') . '"><i class="la la-users "></i> Employees</a>';
//                $row['option'] .= '<a class="dropdown-item" href="' . base_url('Invoice/' . $item->p_id . '/manger') . '"><i class="la la-info "></i> Invoice</a>';
//                $row['option'] .= '  <a class="dropdown-item" href="' . base_url('payment/' . $item->p_id . '/manger') . '"><i class=" flaticon flaticon-coins "></i> payments</a>';


                $row['option'] .= '</div></span>';

                $id++;
                $data[] = $row;
            }
            $output = array(
                "draw" => @$_POST['draw'],
                "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
                "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
                "data" => $data,
            );
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    function create()
    {
        if (check_permission(47, false) == true) {
            $this->form_validation->set_rules('client', 'Client', 'required');
            $this->form_validation->set_rules('project_name', 'Project Name', 'required|min_length[2]|max_length[99]');
            //            $this->form_validation->set_rules('category', 'Category', 'required');
            //            $this->form_validation->set_rules('sub_category', 'Sub category', 'required');
            $this->form_validation->set_rules('budget_hours', 'Budget Hours', 'required|numeric');
            $this->form_validation->set_rules('budget_price', 'Budget Price', 'required|numeric');
            $this->form_validation->set_rules('project_manger', 'project mange', 'required');
            if ($this->form_validation->run() == true) {
                $data ["c_id"] = $this->input->post("client");
                $data ["p_title"] = $this->input->post("project_name");
//                $data ["sc_id"] = $this->input->post("sub_category");

                $data ["p_budget_hours"] = $this->input->post("budget_hours");
                $data ["p_budget_price"] = $this->input->post("budget_price");
                $data ["p_note"] = $this->input->post("note");
                $data ["project_manger"] = $this->input->post("project_manger");
                $id = $this->model->set($data);
                $categories = $this->input->post('sub_category');
                $this->model->add_categories($id, $categories);
                $output = array('result' => "1");
            } else {
                $output = array('result' => "0",
                    "client" => form_error('client'),
                    "project_name" => form_error('project_name'),
                    "category" => form_error('category'),
                    "sub_category" => form_error('sub_category'),
                    "budget_hours" => form_error('budget_hours'),
                    "budget_price" => form_error('budget_price')

                );
            }
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    function update()
    {
        if (check_permission(47, false) == true) {
            $this->form_validation->set_rules('id', 'id', 'required');
            $this->form_validation->set_rules('client', 'Client', 'required');
            $this->form_validation->set_rules('project_name', 'Project Name', 'required|min_length[2]|max_length[99]');
//            $this->form_validation->set_rules('category', 'Category', 'required');
//            $this->form_validation->set_rules('sub_category', 'Sub category', 'required');
            $this->form_validation->set_rules('budget_hours', 'Budget Hours', 'required|numeric');
            $this->form_validation->set_rules('budget_price', 'Budget Price', 'required|numeric');
            $this->form_validation->set_rules('project_manger', 'project mange', 'required');
            if ($this->form_validation->run() == true) {
                $data ["c_id"] = $this->input->post("client");
                $data ["p_title"] = $this->input->post("project_name");
//                $data ["sc_id"] = $this->input->post("sub_category");
                $data ["p_budget_hours"] = $this->input->post("budget_hours");
                $data ["p_budget_price"] = $this->input->post("budget_price");
                $data ["p_note"] = $this->input->post("note");
                $data ["project_manger"] = $this->input->post("project_manger");
                $id = $this->input->post('id');
                $this->model->update($id, $data);
                $this->model->remove_all_categry($id);
                $categories = $this->input->post('sub_category');
                $this->model->add_categories($id, $categories);
                $output = array('result' => "1");
            } else {
                $output = array('result' => "0",
                    "client" => form_error('client'),
                    "project_name" => form_error('project_name'),
                    "category" => form_error('category'),
                    "sub_category" => form_error('sub_category'),
                    "budget_hours" => form_error('budget_hours'),
                    "budget_price" => form_error('budget_price')

                );
            }
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    function employee($project, $manger = null)
    {

        if (check_permission(48) == true || (isset($manger) && $manger == "manger" && $this->db->get_where('project', array('p_id' => $project, 'project_manger' => $this->session->userdata('id')))->num_rows() == 1)) {
            $this->load->model('Employee_model');
            $data['id'] = $project;
            $data['employees'] = $this->Employee_model->get_list();
            $this->render_page('project/employee', 'Project Employees', $data);
        }
    }

    function employees_datatable($project)
    {
        if (check_permission($this->view_emplyee, false)) {

            $col_ord = array(
                'pe_id',
                'e_id',
                'p_id',
                's_id',
                'pe_note',
                'pe_date_active',
                'project',
                'client',
                'cnt_hours',
                'employee',
                'state',
                'pe_date_added',
                'pe_date_start'
            );
            $col_search = array(
                'pe_id',
                'e_id',
                'p_id',
                's_id',
                'pe_note',
                'pe_date_active',
                'project',
                'client',
                'cnt_hours',
                'employee',
                'state',
                'pe_date_added',
                'pe_date_start'
            );
            $name_table = 'view_employee_project';
            $order = array('pe_id' => 'DESC');
            $where[0] = "3";
            $col_where[0] = 's_id !=';
            $col_where[1] = "p_id";
            $where[1] = $project;
            $joinTable = null;
            $joinCol = null;
            $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
            $data = array();
            $id = 1;
            foreach ($list as $item) {
                $row = array();
                $row['num'] = $id;
                $row['id'] = $item->pe_id;
                $row['employee'] = $item->employee;;
                $row['hours'] = $item->cnt_hours;;
                $row['project'] = $item->project;;
                $row['end_date'] = $item->pe_date_active;
                $row['date_added'] = $item->pe_date_added;
                $row['note'] = $item->pe_note;
                $row['start_date'] = $item->pe_date_start;
                $row['client'] = $item->client;
                $row['state'] = $item->state;
                if ($item->s_id == '1') {
                    $row['state'] = '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" id="state_' . $item->pe_id . '">' . $item->state . '</span>';
                } else if ($item->s_id == '2') {
                    $row['state'] = '<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" id="state_' . $item->pe_id . '">' . $item->state . '</span>';
                }

                $row['option'] = '<span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" onclick="item_active(' . $item->pe_id . ')"><i class="la la-check"></i> Active</a>
                                <a class="dropdown-item" onclick="item_inactive(' . $item->pe_id . ')"><i class="la la-times-circle"></i> InActive</a>
                   </div>
                        </span>';

                $id++;
                $data[] = $row;
            }
            $output = array(
                "draw" => @$_POST['draw'],
                "recordsTotal" => $this->dataTable->count_all($name_table, $col_where, $where, $joinTable, $joinCol),
                "recordsFiltered" => $this->dataTable->count_filtered($name_table, $col_ord, $col_search, $col_where, $where, $order, $joinTable, $joinCol),
                "data" => $data,
            );
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    function set_employee()
    {
        if (check_permission(53, false) == true) {
            $this->form_validation->set_rules('project', 'Project', 'required');
            $this->form_validation->set_rules('employees[]', 'Employee', 'required');
            $this->form_validation->set_rules('end_date', 'End Date', 'required');
            if ($this->form_validation->run() == TRUE) {
                $data = array();
                $start_date = date('Y-m-d', strtotime($this->input->post('start_date')));
                $end_date = date('Y-m-d', strtotime($this->input->post('end_date')));
                $project = $this->input->post('project');
                $employees = $this->input->post('employees');
                $note = $this->input->post('Note');
                foreach ($employees as $employee) {
                    $data[] = array(
                        'p_id' => $project,
                        'pe_date_start' => $start_date,
                        'pe_date_active' => $end_date,
                        'e_id' => $employee,
                        'pe_note' => $note
                    );
                }
                $this->model->set_employees($data);
                echo json_encode(array(
                    'result' => '1',
                    'project' => form_error('project'),
                    'employee' => form_error('employees[]'),
                    'end_date' => form_error('end_date')
                ));
            } else {
                echo json_encode(array(
                    'result' => '0',
                    'project' => form_error('project'),
                    'employee' => form_error('employees[]'),
                    'end_date' => form_error('end_date')
                ));
            }


        }
    }

    function export($id, $deleted = false)
    {
        $this->load->library("excel");
        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);
        $table_columns = array("System Id", "Client", "Project", "State", "date added", "Budget Hours", "Budget Price", "Project Manger", "Hours", 'Cost Hours', 'Expanse');
        $column = 0;
        foreach ($table_columns as $field) {

            $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }
        $excel_row = 2;
        $col_ord = array(
            'p_id',
            'p_title',
            'p_budget_hours',
            'p_budget_price',
            'p_note',
            'p_date_added',
            'state',
            's_id',
            'c_id',
            'client',
            'sc_id',
            'sub_category',
//            'category',
//            'sum_hours',
//            'cost_hours',
            'expanse',
            "project_manger",
            "e_first_name",
            "e_last_name",
        );
        $col_search = array(
            'p_id',
            'p_title',
            'p_budget_hours',
            'p_budget_price',
            'p_note',
            'p_date_added',
            'state',
            's_id',
            'c_id',
            'client',
            'sc_id',
            'sub_category',
            'category',
//            'sum_hours',
//            'cost_hours',
//            'expanse',
            "project_manger",
            "e_first_name",
            "e_last_name",
            "e_email",
        );
        $name_table = 'view_project';
        $order = array('p_id' => 'DESC');
        $where[0] = "3";
        $col_where[0] = 's_id !=';
        $where[1] = $id;
        $col_where[1] = 'c_id';
        $joinTable = null;
        $joinCol = null;
        $list = $this->dataTable->get_datatables($name_table, null, $col_search, $col_where, $where, $order, $joinTable, $joinCol);
        $data = array();
        foreach ($list as $item) {
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $item->p_id);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $item->client);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $item->p_title);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $item->state);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $item->p_date_added);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $item->p_budget_hours);
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $item->p_budget_price);
            $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $item->e_email);
            $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, (float)$item->sum_hours);
            $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, (float)$item->cost_hours);
            $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, (float)$item->expanse);

            $excel_row++;
        }
        $name_file = "Client with Project Data";
        if (isset($deleted) && $deleted)
            $name_file = "Client with Project Deleted Data";


        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $name_file . '.xls"');
        $object_writer->save('php://output');

    }

    function get_option_array()
    {
        $sub_categories = $this->db->where_in('c_id', $_POST['clients'])->where('s_id', 1)->get('project')->result();
        $response = "";
        foreach ($sub_categories as $item) {
            $response .= "<option value='" . $item->p_id . "'>" . $item->p_title . "</option>";
        }
        echo $response;
    }
}
