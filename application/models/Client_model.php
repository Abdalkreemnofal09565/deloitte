<?php
require_once 'Base.php';
class Client_model extends Base{

    function __construct()
    {
        parent::__construct();
        $this->table = "client";
        $this->p_k= 'c_id';

    }
    function add_categories($client, $categories)
    {
        foreach ($categories as $item) {
            if ($item != "") {
                $set['sub_category_id'] = $item;
                $set['client_id'] = $client;
                $this->db->insert('category_client', $set);
            }
        }
    }

    function get_category($client, $category)
    {
        $d = $this->db->get_where('view_category_client', array('client_id' => $client, 'category_id' => $category));
        if ($d->num_rows() == 0) {
            return "<i class='la-exclamation-circle
la '>Not found</i>";
        }
        $d = $d->row();
        return $d->sc_title;
    }

    function get_category_active($client, $category)
    {
        $d = $this->db->get_where('view_category_client', array('category_id' => $category, 'client_id' => $client));
        if ($d->num_rows() == 0) return 0;
        return $d->row()->sub_category_id;

    }

    function remove_all_categry($client)
    {
        $this->db->where('client_id', $client);
        $this->db->delete('category_client');
    }
}