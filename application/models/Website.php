<?php

class  Website extends CI_Model
{

    function check_user($email, $password)
    {
        $row = $this->db->where(array(
            'e_email' => $email,
            'e_password' => $password
        ))->get('employee')->result();
        if (count($row) == 0) return false;
        $_SESSION['login'] = true;
        $_SESSION['username'] = $row[0]->e_first_name . ' ' . $row[0]->e_last_name;
        $_SESSION['id'] = $row[0]->e_id;

        return true;
    }

    function found_user($email)
    {
        $row = $this->db->where(array(
            'e_email' => $email

        ))->get('employee')->result();
        if (count($row) == 0) return false;
        return true;
    }

    function check_permission($permission)
    {
        $user = $this->session->userdata("id");
        if ($user == null) {

            return false;
        }
        $val = $this->db->where(array(
            'e_id' => $user,
            'sr_id' => $permission
        ))->get("permission")->num_rows();

        if ($val > 0) {
            return true;
        }
        return false;
    }

}