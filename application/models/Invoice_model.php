<?php
require_once 'Base.php';
class Invoice_model extends Base {
    public function __construct()
    {
        parent::__construct();
        $this->table = "invoice";
        $this->p_k = "i_id";
    }
}