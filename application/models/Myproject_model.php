<?php
require_once 'Base.php';

class Myproject_model extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->table = "cost_project";
        $this->p_k = "cp_id";
    }

    function remove_payment($id)
    {
        $this->db->where(
            array(
                'e_id' => $this->session->userdata('id'),
                'cp_id' => $id
            )
        )->delete($this->table);
    }

    function remove_hours($id)
    {
        $this->db->where(
            array(
                'e_id' => $this->session->userdata('id'),
                'ph_id' => $id
            )
        )->delete("project_hours");
    }

    function add_hours($data)
    {
        $this->db->insert('project_hours', $data);
        return true;
    }

    function get_myproject($client_id = 0)
    {
        $this->db->where(array(
            'e_id' => $_SESSION['id'],
            'state_employee_no' => 1,
            'state_project_no' => 1,
            's_id' => 1,
            'pe_date_start<=' => date("Y/m/d"),
            'pe_date_active>=' => date("Y/m/d"),
            'c_id'=>$client_id
        ));
        $this->db->select(array('p_id', 'project', 'client'));
        $this->db->group_by('p_id');
        $data = $this->db->get('view_employee_project')->result();
        return $data;
    }
    function get_myclient(){
        $this->db->where(array(
            'e_id' => $_SESSION['id'],
            'state_employee_no' => 1,
            'state_project_no' => 1,
            's_id' => 1,
            'pe_date_start<=' => date("Y/m/d"),
            'pe_date_active>=' => date("Y/m/d")
        ));
        $this->db->select(array('c_id', 'client'));
        $this->db->group_by('c_id');
        $data = $this->db->get('view_employee_project')->result();
        return $data;
    }
}