<?php
require_once 'Base.php';

class Report_model extends Base
{
    public function hours($project_id, $employee = null)
    {
        if (isset($_POST['from_date']))
            $this->db->where('ph_date >=', $this->input->post('from_date'));
        if (isset($_POST['to_date']))
            $this->db->where('ph_date <=', $this->input->post('to_date'));


        $this->db->select('sum(ph_hours) as sum ');
        $this->db->where('p_id', $project_id);
        $this->db->where('s_id', 5);
        if (isset($employee)) {
            $this->db->where('e_id', $employee);
        }
        $data = $this->db->get('project_hours');
        $data = $data->row()->sum;
        return $data;
    }

    public function cost_hours($project_id, $employee = null)
    {

        if (isset($_POST['from_date']))
            $this->db->where('ph_date >=', $this->input->post('from_date'));
        if (isset($_POST['to_date']))
            $this->db->where('ph_date <=', $this->input->post('to_date'));
        $this->db->select('sum(ph_hours)*l_price as sum ');
        $this->db->join('view_employee_level', 'view_employee_level.le_id=project_hours.le_id');
        $this->db->where('p_id', $project_id);
        $this->db->where('s_id', 5);
        if (isset($employee)) {
            $this->db->where('project_hours.e_id', $employee);
        }
        $data = $this->db->get('project_hours');
        $data = $data->row()->sum;
        if ($data == null || $data == "") $data = 0;
        return $data;
    }

    public function expanse($project_id, $employee = null)
    {

        $this->db->select_sum('cp_price', 'sum');
        $this->db->where('p_id', $project_id);
        $this->db->where('s_id', 5);
        if (isset($_POST['from_date']))
            $this->db->where('cp_date >=', $this->input->post('from_date'));
        if (isset($_POST['to_date']))
            $this->db->where('cp_date <=', $this->input->post('to_date'));

        if (isset($employee)) {
            $this->db->where('e_id', $employee);
        }
        $data = $this->db->get('cost_project');

        $data = $data->row()->sum;
        if ($data == null || $data == "") $data = 0;
        return $data;
    }

    public function expanse_type($project_id, $expanse_type, $employee = null)
    {

        $this->db->select_sum('cp_price', 'sum');
        if (isset($employee))
            $this->db->where('e_id', $employee);
        $this->db->where(array('p_id' => $project_id, 'ct_id' => $expanse_type, 's_id' => 5));
        if (isset($_POST['from_date']))
            $this->db->where('cp_date >=', $this->input->post('from_date'));
        if (isset($_POST['to_date']))
            $this->db->where('cp_date <=', $this->input->post('to_date'));
        $data = $this->db->get('cost_project');
        $data = $data->row()->sum;
        if ($data == null || $data == "") $data = 0;
        return $data;
    }

    public function invoice($project_id)
    {

        $data = $this->db->select_sum('i_value', 'sum')->where('p_id', $project_id)->where('s_id', 1)->get('invoice');
        $data = $data->row()->sum;
        if ($data == null || $data == "") $data = 0;
        return $data;
    }

    public function payment($project_id, $decrease = false)
    {

        if (isset($_POST['from_date']))
            $this->db->where('p_date >=', $this->input->post('from_date'));
        if (isset($_POST['to_date']))
            $this->db->where('p_date <=', $this->input->post('to_date'));

        $this->db->select_sum('pay_payment', 'sum');
        $this->db->where('p_id', $project_id);
        $this->db->where('s_id', 1);
        if (!$decrease)
            $this->db->where('pt_type', 0);
        else
            $this->db->where('pt_type', 1);

        $data = $this->db->get('view_payment');
        $data = $data->row()->sum;
        if ($data == null || $data == "") $data = 0;
        return $data;
    }

    public function payment_type($project_id, $payment_type)
    {
        $this->db->select_sum('pay_payment', 'sum');
        $this->db->where('p_id', $project_id);
        $this->db->where('s_id', 1);
        if (isset($_POST['from_date']))
            $this->db->where('p_date >=', $this->input->post('from_date'));
        if (isset($_POST['to_date']))
            $this->db->where('p_date <=', $this->input->post('to_date'));

        $this->db->where('pt_id', $payment_type);
        $data = $this->db->get('view_payment');
        if ($data->num_rows() == 0 || $data->row()->sum == null || $data->row()->sum == "") $data = 0;
        else $data = $data->row()->sum;
        return $data;
    }

    public function count_employee($project_id)
    {
        $data = $this->db->select('count(*) as count')->where('p_id', $project_id)->group_by('e_id')->get('project_emplyee');
//        echo $this->db->last_query();
        if ($data->num_rows() != 0)
            $data = $data->row()->count;
        else          $data = 0;
        return $data;
    }

    public function hours_type($project_id, $hours_id, $employee = null)
    {
        $this->db->select('sum(ph_hours) as sum');
        $this->db->group_by('p_id');
        $this->db->where(array('p_id' => $project_id, 'ht_id' => $hours_id, 's_id' => 5));

        if (isset($_POST['from_date']))
            $this->db->where('ph_date >=', $this->input->post('from_date'));
        if (isset($_POST['to_date']))
            $this->db->where('ph_date <=', $this->input->post('to_date'));

        if (isset($employee)) {
            $this->db->where('e_id', $employee);
        }

        $data = $this->db->get('project_hours');
        if ($data->num_rows() != 0) {
            $data = $data->row()->sum;
        } else {
            $data = 0;
        }
        return $data / 3600;
    }

    function category_get_value($category, $object, $type)
    {
        $cm = null;
        if ($type == "project") {
            $this->load->model('Project_model');
            $cm = $this->Project_model;
        }

        $d = $cm->get_category_active($object, $category);
        return $d;
    }

    function get_project_sc($sc = array())
    {
        $this->db->where_in('sc_id', $sc);
        $this->db->select('p_id');
        $this->db->group_by('p_id');
        $data = $this->db->get('category_project')->result();
        $a = array();
        foreach ($data as $item) {
            $a[] = $item->p_id;
        }
        return $a;
    }

    function client($client = null, $project = null, $categories = null, $sub_categories = null, $type = null)
    {
        if (!isset($type) || (isset($type) && $type == "all")) {
            if (isset($client) && !empty($client)) {
                $this->db->where_in('client_id', $client);
            }
            if (isset($categories) && !empty($categories)) {
                $this->db->where_in('category_id', $categories);
            }
            if (isset($sub_categories) && !empty($sub_categories)) {
                $this->db->where_in('p_id', $this->get_project_sc($sub_categories));
            }
            if (isset($project) && !empty($project)) {
                $this->db->where_in('p_id', $project);
            }
            $base_data = $this->db->get('view_project');

            $data = array();
            $return_data['num_row'] = $base_data->num_rows();
            $return_data['result'] = array();
            $d = $base_data->result();
            $cate = $this->db->get_where('category', array('s_id' => 1, 'c_project' => 1))->result();
            foreach ($d as $item) {
                $row = array();
                $row['client'] = $item->client;
                $row['project'] = $item->p_title;
                $row['project_id'] = $item->p_id;
                foreach ($cate as $vv) {
                    $row['' . $vv->c_title] = $this->category_get_value($vv->c_id, $item->p_id, 'project');
                }
                $row['balance_d'] = $this->payment($item->p_id, true);
                $row['balance_i'] = $this->payment($item->p_id);
                $row['hours'] = $this->hours($item->p_id);
                $row['cost_hours'] = $this->cost_hours($item->p_id);
                $row['expanse'] = $this->expanse($item->p_id);
                $row['budget_hours'] = $item->p_budget_hours;
                $row['budget_price'] = $item->p_budget_price;
//                $row['invoice'] = $this->invoice($item->p_id);
                $row['count_employee'] = $this->count_employee($item->p_id);
                $row['project_manger'] = $item->project_manger_name;
                $data[] = $row;
            }
            return $data;
        } else if (isset($type) && $type == "employee_expanse") {
            if (isset($client) && !empty($client)) {
                $this->db->where_in('clint_id', $client);
            }
            if (isset($sub_categories) && !empty($sub_categories)) {
                $s = $this->get_project_sc($sub_categories);

                if (empty($s)) $s[] = 0;
                $this->db->where_in('p_id', $s);
            }
            if (isset($project) && !empty($project)) {
                $this->db->where_in('p_id', $project);
            }
            if (isset($_POST['from_date']))
                $this->db->where('cp_date >=', $this->input->post('from_date'));
            if (isset($_POST['to_date']))
                $this->db->where('cp_date <=', $this->input->post('to_date'));

            $data = $this->db->get('view_costs')->result();
            $result = array();
            foreach ($data as $item) {
                $row = array();
                $row['client'] = $item->client;
                $row['project'] = $item->project;
                $row['project_id'] = $item->project_id;
                $row['type'] = $item->payment_type;
                $row['expanse'] = $item->cp_price;
                $row['date'] = $item->cp_date;
                $row['date_added'] = $item->cp_date_added;
                $row['employee'] = $item->employee;
                $row['expnase_note'] = $item->cp_note;
                $row['state'] = $item->state;
                $result[] = $row;
            }
            return $result;


        } else if (isset($type) && $type == "employee_worktime") {
            if (isset($client) && !empty($client)) {
                $this->db->where_in('client_id', $client);
            }
            if (isset($sub_categories) && !empty($sub_categories)) {
                $s = $this->get_project_sc($sub_categories);

                if (empty($s)) $s[] = 0;
                $this->db->where_in('p_id', $s);
            }
            if (isset($project) && !empty($project)) {
                $this->db->where_in('p_id', $project);
            }
            if (isset($_POST['from_date']))
                $this->db->where('ph_date >=', $this->input->post('from_date'));
            if (isset($_POST['to_date']))
                $this->db->where('ph_date <=', $this->input->post('to_date'));

            $data = $this->db->get('view_hours_project')->result();
            $result = array();
            foreach ($data as $item) {
                $row = array();
                $row['client'] = $item->client;
                $row['project'] = $item->project;
                $row['project_id'] = $item->p_id;
                $row['type'] = $item->hoursType;
                $row['date'] = $item->ph_date;
                $row['date_added'] = $item->ph_date_added;
                $row['employee'] = $item->employee;
                $row['level'] = $item->level_title;
                $row['level_price'] = $item->level_price;
                $row['start_time'] = $item->start_time;
                $row['end_time'] = $item->end_time;
                $row['hours'] = $item->ph_hours / 3600;
                $row['price_hours'] = $row['hours'] * $row['level_price'];

                $row['state'] = $item->state;
                $result[] = $row;
            }
            return $result;
        } else if (isset($type) && $type == "client_statement_account") {
            if (isset($client) && !empty($client)) {
                $this->db->where_in('client_id', $client);
            }
            if (isset($sub_categories) && !empty($sub_categories)) {
                $s = $this->get_project_sc($sub_categories);

                if (empty($s)) $s[] = 0;
                $this->db->where_in('p_id', $s);
            }
            if (isset($project) && !empty($project)) {
                $this->db->where_in('p_id', $project);
            }
            if (isset($_POST['from_date']))
                $this->db->where('p_date >=', $this->input->post('from_date'));
            if (isset($_POST['to_date']))
                $this->db->where('p_date <=', $this->input->post('to_date'));

            $data = $this->db->get('view_payment')->result();
            $result = array();
            foreach ($data as $item) {
                $row = array();
                $row['client'] = $item->c_name;
                $row['project'] = $item->p_title;
                $row['project_id'] = $item->p_id;
                $row['type'] = $item->pt_title;
                $row['date'] = $item->p_date;
                $row['date_added'] = $item->pay_date_added;
                $row['statement'] = $item->pay_payment;
                $row['reference'] = $item->reference;
                $row['Debit'] = "-";
                $row['Credit'] = "-";
                if ($item->pt_type == 0) {
                    $row['Debit'] = $item->pay_payment;
                } else {
                    $row['Credit'] = $item->pay_payment;

                }
                $row['state'] = $item->s_title;
                $result[] = $row;
            }
            return $result;


        }

    }


    function get_employee($employee, $type)
    {
        if ($type == 1) {
            if (!empty($employee))
                $this->db->where_in('e_id', $employee);
            $this->db->group_by(array('p_id', 'e_id'));
            $this->db->order_by('e_id');
            $employees = $this->db->get('view_employee_project')->result();
            $data = array();
            foreach ($employees as $item) {
                $row = array();
                $row['name'] = $item->employee;
                $row['e_id'] = $item->e_id;
                $row['project'] = $item->project;
                $row['project_id'] = $item->p_id;
                $row['client'] = $item->client;
                $row['hours'] = $this->hours($item->p_id, $item->e_id);
                $row['cost_hours'] = $this->cost_hours($item->p_id, $item->e_id);
                $row['expanse'] = $this->expanse($item->p_id, $item->e_id);
                $data [] = $row;
            }

            return $data;
        } else if ($type == 2) {
            if (isset($_POST['from_date']))
                $this->db->where('cp_date >=', $this->input->post('from_date'));
            if (isset($_POST['to_date']))
                $this->db->where('cp_date <=', $this->input->post('to_date'));
            if (!empty($employee))
                $this->db->where_in('e_id', $employee);
            $this->db->where('s_id', 5);
            $data = $this->db->get('view_costs')->result();
            return $data;

        } else if ($type == 3) {
            if (isset($_POST['from_date']))
                $this->db->where('ph_date >=', $this->input->post('from_date'));
            if (isset($_POST['to_date']))
                $this->db->where('ph_date <=', $this->input->post('to_date'));
            if (!empty($employee))
                $this->db->where_in('e_id', $employee);
            $this->db->where('s_id', 5);
            $data = $this->db->get('view_hours_project')->result();
            return $data;
        }
    }


}
