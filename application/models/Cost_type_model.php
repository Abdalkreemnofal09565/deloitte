<?php

require_once 'Base.php';
class Cost_type_model extends  Base
{
    public function __construct()
    {
        parent::__construct();
        $this->table = "cost_type";
        $this->p_k="ct_id";
    }
}