<?php
require_once 'Base.php';

class Project_model extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->table = "project";
        $this->p_k = "p_id";
    }

    function set_employees($data)
    {
        $this->db->insert_batch('project_emplyee',$data);
    }
    function add_categories($project, $categories)
    {
        foreach ($categories as $item) {
            if ($item != "") {
                $set['sc_id'] = $item;
                $set['p_id'] = $project;
                $this->db->insert('category_project', $set);
            }
        }
    }

    function get_category($project, $category)
    {
        $d = $this->db->get_where('view_category_project', array('p_id' => $project, 'c_id' => $category));
        if ($d->num_rows() == 0) {
            return "<i class='la-exclamation-circle
la '>Not found</i>";
        }
        $d = $d->row();
        return $d->sc_title;
    }

    function get_category_active($project, $category)
    {
        $d = $this->db->get_where('view_category_project', array('c_id' => $category, 'p_id' => $project));
        if ($d->num_rows() == 0) return "Not Found";
        return $d->row()->sc_title;

    }

    function remove_all_categry($employee)
    {
        $this->db->where('p_id', $employee);
        $this->db->delete('category_project');
    }
}