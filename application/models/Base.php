<?php

class Base extends CI_Model
{
    public $table;
    public $p_k;
    public $state = "s_id";

    public function set($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function get($id = null, $where = null)
    {
        if (isset($id)) {
            $this->db->where($this->p_k, $id);
        }
        if (isset($where)) {
            $this->db->where($where);
        }

        $row = $this->db->get($this->table);
        if ($row->num_rows() > 0) {
            if (isset($id))
                return $row->row();
            return $row->result();
        }
        return array();
    }

    public function update($id, $info)
    {
        if ($this->db->where(array($this->p_k => $id))->update($this->table, $info))
            return true;
        return false;

    }

    public function state($id, $state)
    {
        if ($this->db->where($this->p_k, $id)->update($this->table, array($this->state => $state)))
            return true;

        return false;
    }

    public function count($where = null)
    {
        if (isset($where))
            $this->db->where($where);
        $Value = $this->db->count_all($this->table);
        return $Value;
    }

    public function get_list()
    {
        $data = $this->db->where(array(
            $this->state => "1"
        ))->get($this->table)->result();
        return $data;
    }

    public function found($id)
    {
        $cnt = $this->db->where(array($this->p_k=>$id))->get($this->table)->num_rows();
        if ($cnt == 0) return false;
        return true;
    }

}