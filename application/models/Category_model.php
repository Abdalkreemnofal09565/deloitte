<?php
require_once 'Base.php';
class Category_model extends  Base{
    public function __construct()
    {
        parent::__construct();
        $this->table = "category";
        $this->p_k = "c_id";
    }
}