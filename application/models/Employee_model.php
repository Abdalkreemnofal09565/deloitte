<?php

require_once 'Base.php';

class  Employee_model extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->table = "employee";
        $this->p_k = "e_id";

    }

    function set_level($data)
    {
        $this->db->insert('level_employee', $data);
        $id = $this->db->insert_id();
        return $id;
    }

    function get_level($id)
    {
        $data = $this->db->order_by('le_id', 'DESC')->get_where('view_employee_level', array('e_id' => $id))->result();
        return $data;
    }

    function get_levels()
    {
        $data = $this->db->where('s_id', 1)->get('level')->result();
        return $data;
    }

    function get_info($employee)
    {
        $data['info'] = $this->get($employee);
        $data['level'] = $this->get_level($employee);
        return $data;
    }

    function get_all_permission($employee)
    {
        $data = array();
        $roles = $this->db->get('role')->result();
        foreach ($roles as $role) {
            $row = array();
            $row['role'] = $role;
            $row['sub_role'] = $this->sub_role($role->r_id, $employee);
            $data[] = $row;
        }
        return $data;
    }

    function sub_role($role, $employee)
    {
        $data = array();
        $sub_roles = $this->db->where('r_id', $role)->get('sub_role')->result();
        foreach ($sub_roles as $sub_role) {
            $row = array();
            $row['sub_title'] = $sub_role->sr_title;
            $row['sub_id'] = $sub_role->sr_id;
            $row['role_id'] = $sub_role->r_id;
            $row['is_active'] = $this->is_permission($sub_role->sr_id, $employee);
            $data[] = $row;
        }
        return $data;
    }

    function is_permission($sub_role, $employee)
    {
        $value = $this->db->get_where('permission', array('sr_id' => $sub_role, 'e_id' => $employee))->num_rows();
        if ($value == 0)
            return 0;
        return 1;
    }

    function update_permission($employee, $persmission)
    {
        $cnt = $this->db->where(array('e_id' => $employee, 'sr_id' => $persmission))->get('permission')->num_rows();
        if ($cnt == 0) {
            $this->db->insert('permission', array('e_id' => $employee, 'sr_id' => $persmission));
        } else {
            $this->db->where(array('e_id' => $employee, 'sr_id' => $persmission))->delete('permission');
        }
    }

    function get_reports($id)
    {
        $data['all_expanse'] = $this->db->select_sum('cp_price', 'expanse')->where(array('e_id' => $id, 's_id' => 5))->get('cost_project')->row()->expanse;
        $data['all_hours'] = $this->db->select_sum('ph_hours', "hours")->where(array('s_id' => 5, 'e_id' => $id))->get('view_hours_project')->row()->hours / 3600;
        $data['all_price_hours'] = $this->db->select('sum((ph_hours/3600)*level_price) as total_price')->where(array('s_id' => 5, 'e_id' => $id))->get('view_hours_project')->row()->total_price;;
        $data['all_project'] = $this->db->select('count(*) as cnt')->group_by('p_id')->get_where('project_emplyee', array('e_id' => $id));
        if ($data['all_project']->num_rows() != 0) {
            $data['all_project'] = $data['all_project']->row()->cnt;
        } else {
            $data['all_project'] = 0;
        }

        $projects = $this->db->group_by('p_id')->get_where('view_employee_project', array('e_id' => $id))->result();
        $years = $this->db->group_by('year_project')->get_where('view_hours_project', array('e_id' => $id))->result();
        $result_project = array();
        $result_date = array();
        foreach ($projects as $project) {
            $row = array();
            $row['info'] = $project;
            $row['hours'] = $this->db->select_sum('ph_hours', "hours")->where(array('s_id' => 5, 'e_id' => $id, 'p_id' => $project->p_id))->get('view_hours_project')->row()->hours / 3600;;
            $row['price_hours'] = $this->db->select('sum((ph_hours/3600)*level_price) as total_price')->where(array('s_id' => 5, 'e_id' => $id, 'p_id' => $project->p_id))->get('view_hours_project')->row()->total_price;;
            $row['all_expanse'] = $this->db->select_sum('cp_price', 'expanse')->where(array('e_id' => $id, 's_id' => 5, 'p_id' => $project->p_id))->get('cost_project')->row()->expanse;

            $result_project[] = $row;
        }
        $data['projects'] = $result_project;

        foreach ($years as $year) {
            $row = array();
            $row['year'] = $year->year_project;
            $row['hours'] = $this->db->select_sum('ph_hours', "hours")->where(array('s_id' => 5, 'e_id' => $id, 'year_project' => $year->year_project))->get('view_hours_project')->row()->hours / 3600;;
            $row['price_hours'] = $this->db->select('sum((ph_hours/3600)*level_price) as total_price')->where(array('s_id' => 5, 'e_id' => $id, 'year_project' => $year->year_project))->get('view_hours_project')->row()->total_price;;


            $row['all_expanse'] = $this->db->select_sum('cp_price', 'expanse')->where(array('e_id' => $id, 's_id' => 5, 'year_project' => $year->year_project))->get('view_costs')->row()->expanse;
            $months = $this->db->group_by('month_project')->get_where('view_hours_project', array('e_id' => $id, 'year_project' => $year->year_project))->result();
            $result_month = array();
            foreach ($months as $month) {
                $row1 = array();

                $row1['month'] = $month->month_project;
                $row1['hours'] = $this->db->select_sum('ph_hours', "hours")->where(array('s_id' => 5, 'e_id' => $id, 'year_project' => $year->year_project, 'month_project' => $month->month_project))->get('view_hours_project')->row()->hours / 3600;;
                $row1['price_hours'] = $this->db->select('sum((ph_hours/3600)*level_price) as total_price')->where(array('s_id' => 5, 'e_id' => $id, 'year_project' => $year->year_project, 'month_project' => $month->month_project))->get('view_hours_project')->row()->total_price;;
                $row1['all_expanse'] = $this->db->select_sum('cp_price', 'expanse')->where(array('e_id' => $id, 's_id' => 5, 'year_project' => $year->year_project, 'month_project' => $month->month_project))->get('view_costs')->row()->expanse;
                $result_month[] = $row1;
            }
            $row['month'] = $result_month;
            $result_date[] = $row;

        }
        $data['dates'] = $result_date;
        return $data;
    }

    function add_categories($employee_id, $categories)
    {
        foreach ($categories as $item) {
            if ($item != "") {
                $set['sc_id'] = $item;
                $set['e_id'] = $employee_id;
                $this->db->insert('category_employee', $set);
            }
        }
    }

    function get_category($employee_id, $category)
    {
        $d = $this->db->get_where('view_category_employee', array('e_id' => $employee_id, 'c_id' => $category));
        if ($d->num_rows() == 0) {
            return "<i class='la-exclamation-circle
la '>Not found</i>";
        }
        $d = $d->row();
        return $d->sc_title;
    }

    function get_category_active($employee, $category)
    {
        $d = $this->db->get_where('view_category_employee', array('c_id' => $category, 'e_id' => $employee));
        if ($d->num_rows() == 0) return 0;
        return $d->row()->sc_id;

    }

    function remove_all_categry($employee)
    {
        $this->db->where('e_id', $employee);
        $this->db->delete('category_employee');
    }

}