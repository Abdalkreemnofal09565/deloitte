<?php
require_once 'Base.php';

class  Payment_model extends Base{
    public function __construct()
    {
        parent::__construct();
        $this->table = "payments";
        $this->p_k = "pay_id";
    }
}