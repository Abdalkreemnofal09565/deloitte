<?php
require_once 'Base.php';
class payment_type_model extends  Base{
    public function __construct()
    {
        parent::__construct();
        $this->table = "payment_type";
        $this->p_k = "pt_id";
    }
}