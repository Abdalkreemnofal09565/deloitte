<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon-layers"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    Expanse Type
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <?php if (check_permission(41, false)) { ?>
                            <a href="<?= base_url('expanse_type/add') ?>" class="btn btn-brand btn-elevate btn-icon-sm">
                                <i class="la la-plus"></i>
                                New Record
                            </a>
                        <?php }
                        if (check_permission(90, false)) { ?>
                            <a href="<?= base_url('expanse_type/export') ?>"
                               class="btn btn-outline-primary btn-elevate btn-icon-sm" target="_blank">
                                <i class="la la-download"></i>
                                Export excel
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>

                    <th>Date Added</th>
                    <th>State</th>
                    <th>Option</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function () {

        table = $('#table').DataTable({

            "responsive": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "searching": true,
            "select": false,
            "ordering": false,
            "ajax": {
                "url": "<?=base_url('Expanse_type/datatable')?>",
                "type": "POST"
            },

            "columns": [
                {
                    "data": "num"
                },
                {
                    "data": "title"
                },
                {
                    "data": "date_register"
                },
                {
                    "data": "state"
                }
                ,
                {
                    "data": "option"
                }

            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });

    });
    var name_table = "expanse_type";


</script>