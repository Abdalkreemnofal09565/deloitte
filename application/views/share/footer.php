</div>
<!-- begin:: Footer -->
<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
    <div class="kt-footer__copyright">
        <?= date('Y', time()) ?>&nbsp;&copy;&nbsp;<a href="https://syrian-center.com/" target="_blank" class="kt-link">syrian-cente</a>
    </div>

</div>

<!-- end:: Footer -->
</div>
</div>
</div>
<script>
    $(document).ready(function () {
        $(".numeric").numeric({ decimal : ".",  negative : false, scale: 3 });

    });
    function item_active(id) {
        $("#state_" + id).text('').removeClass().addClass('kt-spinner kt-spinner--sm kt-spinner--brand');

        $.ajax({
            url: "<?=base_url()?>" + name_table + '/active',
            data: {id: id},
            method: "post",
            success: function () {

                setTimeout(function () {

                    $("#state_" + id).text('active').removeClass().addClass(' kt-badge  kt-badge--success kt-badge--inline kt-badge--pill');
                }, 1000);
                $('#table').DataTable().ajax.reload();
            }
        });
    }

    function item_inactive(id) {

        $("#state_" + id).text('').removeClass().addClass('kt-spinner kt-spinner--sm kt-spinner--brand');
        $.ajax({
            url: "<?=base_url()?>" + name_table + '/Inactive',
            data: {id: id},
            method: "post",
            success: function () {
                setTimeout(function () {

                    $("#state_" + id).text('inactive').removeClass().addClass(' kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill');

                }, 1000);
                $('#table').DataTable().ajax.reload();

            }
        });
    }
    function delete_item(id) {

        $("#state_" + id).text('').removeClass().addClass('kt-spinner kt-spinner--sm kt-spinner--danger');
        $.ajax({
            url: "<?=base_url()?>" + name_table + '/remove',
            data: {id: id},
            method: "post",
            success: function () {
                setTimeout(function () {

                    $("#state_" + id).text('Deleted').removeClass().addClass(' kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill');
                    $("#option_"+id).hide();
                }, 1000);
                setTimeout(function () {

                    $('#table').DataTable().ajax.reload();
                    }, 3000);

            }
        });
    }

</script>

<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>

<!--<script src="--><? //=base_url('template')?><!--/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>-->
<script src="<?= base_url('template') ?>/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap/dist/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/tooltip.js/dist/umd/tooltip.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/jquery-form/dist/jquery.form.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/block-ui/jquery.blockUI.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/js/vendors/bootstrap-datepicker.init.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/js/vendors/bootstrap-timepicker.init.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-daterangepicker/daterangepicker.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-select/dist/js/bootstrap-select.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/js/vendors/bootstrap-switch.init.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/typeahead.js/dist/typeahead.bundle.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/handlebars/dist/handlebars.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/inputmask/dist/jquery.inputmask.bundle.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/nouislider/distribute/nouislider.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/owl.carousel/dist/owl.carousel.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/autosize/dist/autosize.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/clipboard/dist/clipboard.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/dropzone/dist/dropzone.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/summernote/dist/summernote.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/markdown/lib/markdown.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-markdown/js/bootstrap-markdown.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/js/vendors/bootstrap-markdown.init.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-notify/bootstrap-notify.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/js/vendors/bootstrap-notify.init.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/jquery-validation/dist/jquery.validate.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/jquery-validation/dist/additional-methods.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/js/vendors/jquery-validation.init.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/toastr/build/toastr.min.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/raphael/raphael.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/morris.js/morris.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/vendors/jquery-idletimer/idle-timer.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/waypoints/lib/jquery.waypoints.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/counterup/jquery.counterup.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/es6-promise-polyfill/promise.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/sweetalert2/dist/sweetalert2.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/js/vendors/sweetalert2.init.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/jquery.repeater/src/lib.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/jquery.repeater/src/jquery.input.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/jquery.repeater/src/repeater.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/dompurify/dist/purify.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/js/demo1/scripts.bundle.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/fullcalendar/fullcalendar.bundle.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/js/demo1/pages/dashboard.js" type="text/javascript"></script>

<script src="<?= base_url('template') ?>/js/demo1/scripts.bundle.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/select2/dist/js/select2.full.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/js/demo1/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/js/demo1/pages/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>

<script src="<?= base_url('template') ?>/js/demo1/pages/crud/datatables/basic/basic.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/datatables/datatables.bundle.js"  type="text/javascript"></script>

</body>
</html>