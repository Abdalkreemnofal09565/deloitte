<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?= base_url() ?>">
    <meta charset="utf-8"/>
    <title><?= @$title ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="<?= base_url('template') ?>/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet"
          type="text/css"/>

    <link href="<?= base_url('template') ?>/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css"
          rel="stylesheet" type="text/css"/>
<link href="<?= base_url('template') ?>/vendors/general/tether/dist/css/tether.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/bootstrap-daterangepicker/daterangepicker.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/bootstrap-select/dist/css/bootstrap-select.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/select2/dist/css/select2.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/nouislider/distribute/nouislider.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/owl.carousel/dist/assets/owl.theme.default.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/dropzone/dist/dropzone.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/summernote/dist/summernote.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/animate.css/animate.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/toastr/build/toastr.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/morris.js/morris.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/@fortawesome/fontawesome-free/css/all.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/css/demo1/style.bundle.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css"/>
    <script src="<?= base_url('template') ?>/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>

    <link rel="shortcut icon" href="<?=base_url('icon2.png')?>" />

    <script src="<?= base_url('template') ?>/js/demo1/pages/crud/forms/widgets/select2.js"
            type="text/javascript"></script>
    <script src="<?= base_url('template') ?>/js/demo1/pages/crud/forms/widgets/bootstrap-datepicker.js"
            type="text/javascript"></script>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
    <div class="kt-header-mobile__logo">

    </div>
    <div class="kt-header-mobile__toolbar">
        <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler">
            <span></span></button>

        <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i
                    class="flaticon-more"></i></button>
    </div>
</div>

<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <!-- begin:: Aside -->
        <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
        <div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop"
             id="kt_aside">

            <!-- begin:: Aside -->
            <div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
                <div class="kt-aside__brand-logo">
                    <a href="<?=base_url('timesheet')?>">
                        <img alt="Logo" src="<?= base_url('icon.png') ?>" width="100"/>
                    </a>
                </div>
                <div class="kt-aside__brand-tools">
                    <button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler">
								<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                           width="24px" height="24px" viewBox="0 0 24 24" version="1.1"
                                           class="kt-svg-icon">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<polygon id="Shape" points="0 0 24 0 24 24 0 24"/>
											<path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z"
                                                  id="Path-94" fill="#000000" fill-rule="nonzero"
                                                  transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999) "/>
											<path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z"
                                                  id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3"
                                                  transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999) "/>
										</g>
									</svg></span>
                        <span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                   width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<polygon id="Shape" points="0 0 24 0 24 24 0 24"/>
											<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z"
                                                  id="Path-94" fill="#000000" fill-rule="nonzero"/>
											<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z"
                                                  id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3"
                                                  transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
										</g>
									</svg></span>
                    </button>


                </div>
            </div>
            <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
                <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1"
                     data-ktmenu-dropdown-timeout="500">
                    <ul class="kt-menu__nav ">
                        <!--                   classfication-->
                        <?php  if(isset($pre_11) || isset($pre_24) || isset($pre_40)  || isset($pre_59)|| isset($pre_65) ){?>
                        <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true"
                            data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                                                                  class="kt-menu__link kt-menu__toggle"><span
                                        class="kt-menu__link-icon"><svg xmlns="http://www.w3.org/2000/svg"
                                                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                        width="24px" height="24px" viewBox="0 0 24 24"
                                                                        version="1.1" class="kt-svg-icon">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect id="bound" x="0" y="0" width="24" height="24"></rect>
													<rect id="Rectangle-7" fill="#000000" x="4" y="4" width="7"
                                                          height="7" rx="1.5"></rect>
													<path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z"
                                                          id="Combined-Shape" fill="#000000" opacity="0.3"></path>
												</g>
											</svg></span><span class="kt-menu__link-text">Classification</span><i
                                        class="kt-menu__ver-arrow la la-angle-right"></i></a>
                            <div class="kt-menu__submenu " kt-hidden-height="120" style=""><span
                                        class="kt-menu__arrow"></span>
                                <ul class="kt-menu__subnav">
                                    <?php if(isset($pre_11)){?>
                                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                        data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                                                                              class="kt-menu__link kt-menu__toggle"><i
                                                    class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span
                                                    class="kt-menu__link-text">Categories</span><i
                                                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                            <ul class="kt-menu__subnav">
                                                <li class="kt-menu__item " aria-haspopup="true"><a
                                                            href="<?= base_url('Category') ?>" class="kt-menu__link "><i
                                                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                                class="kt-menu__link-text">List</span></a></li>
                                                <li class="kt-menu__item " aria-haspopup="true"><a
                                                            href="<?= base_url('Category/deleted') ?>"
                                                            class="kt-menu__link "><i
                                                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                                class="kt-menu__link-text">List deleted</span></a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <?php } if(isset($pre_24)){?>
                                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                        data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                                                                              class="kt-menu__link kt-menu__toggle"><i
                                                    class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span
                                                    class="kt-menu__link-text">Level</span><i
                                                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                            <ul class="kt-menu__subnav">
                                                <li class="kt-menu__item " aria-haspopup="true"><a
                                                            href="<?= base_url('level') ?>"
                                                            class="kt-menu__link "><i
                                                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                                class="kt-menu__link-text">List</span></a></li>
                                                <li class="kt-menu__item " aria-haspopup="true"><a
                                                            href="<?= base_url('level/list_deleted') ?>"
                                                            class="kt-menu__link "><i
                                                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                                class="kt-menu__link-text">List Deleted</span></a></li>

                                            </ul>
                                        </div>
                                    </li>
                                    <?php } if(isset($pre_40)){?>
                                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                        data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                                                                              class="kt-menu__link kt-menu__toggle"><i
                                                    class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span
                                                    class="kt-menu__link-text">Expanse Type</span><i
                                                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                            <ul class="kt-menu__subnav">
                                                <li class="kt-menu__item " aria-haspopup="true"><a
                                                            href="<?= base_url('Expanse_type') ?>"
                                                            class="kt-menu__link "><i
                                                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                                class="kt-menu__link-text">List</span></a></li>
                                                <li class="kt-menu__item " aria-haspopup="true"><a
                                                            href="<?= base_url('expanse_type/list_delete') ?>"
                                                            class="kt-menu__link "><i
                                                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                                class="kt-menu__link-text">List Deleted</span></a></li>

                                            </ul>
                                        </div>
                                    </li>
                                    <?php } if(isset($pre_59)){?>
                                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                        data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                                                                              class="kt-menu__link kt-menu__toggle"><i
                                                    class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span
                                                    class="kt-menu__link-text">Hours Type </span><i
                                                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                            <ul class="kt-menu__subnav">
                                                <li class="kt-menu__item " aria-haspopup="true"><a
                                                            href="<?= base_url('hours_type') ?>"
                                                            class="kt-menu__link "><i
                                                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                                class="kt-menu__link-text">List</span></a></li>
                                                <li class="kt-menu__item " aria-haspopup="true"><a
                                                            href="<?= base_url('hours_type/list_delete') ?>"
                                                            class="kt-menu__link "><i
                                                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                                class="kt-menu__link-text">List Deleted</span></a></li>

                                            </ul>
                                        </div>
                                    </li>
                                    <?php } if(isset($pre_65)){?>
                                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                        data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                                                                              class="kt-menu__link kt-menu__toggle"><i
                                                    class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span
                                                    class="kt-menu__link-text">Statment account type </span><i
                                                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                            <ul class="kt-menu__subnav">
                                                <li class="kt-menu__item " aria-haspopup="true"><a
                                                            href="<?= base_url('Statment_account_type') ?>"
                                                            class="kt-menu__link "><i
                                                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                                class="kt-menu__link-text">List</span></a></li>
                                                <li class="kt-menu__item " aria-haspopup="true"><a
                                                            href="<?= base_url('Statment_account_type/list_deleted') ?>"
                                                            class="kt-menu__link "><i
                                                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                                class="kt-menu__link-text">List Deleted</span></a></li>

                                            </ul>
                                        </div>
                                    </li>
                                    <?php }?>
                                </ul>
                            </div>
                        </li>
                        <?php } if(isset($pre_10)){?>
                        <!--                       uesrs-->
                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                            data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                                                                  class="kt-menu__link kt-menu__toggle"><i
                                        class="kt-menu__link-icon flaticon-users"></i><span class="kt-menu__link-text">Employees</span><i
                                        class="kt-menu__ver-arrow la la-angle-right"></i></a>
                            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                <ul class="kt-menu__subnav">
                                    <li class="kt-menu__item " aria-haspopup="true"><a
                                                href="<?= base_url('employee') ?>"
                                                class="kt-menu__link "><i
                                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                    class="kt-menu__link-text">List </span></a></li>


                                </ul>
                            </div>
                        </li>
                        <?php }if(isset($pre_30)){ ?>
                        <!--                        client-->
                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                            data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                                                                  class="kt-menu__link kt-menu__toggle"><i
                                        class="kt-menu__link-icon flaticon-suitcase
"></i><span class="kt-menu__link-text">Clients</span><i
                                        class="kt-menu__ver-arrow la la-angle-right"></i></a>
                            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                <ul class="kt-menu__subnav">
                                    <li class="kt-menu__item " aria-haspopup="true"><a
                                                href="<?= base_url('client') ?>"
                                                class="kt-menu__link "><i
                                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                    class="kt-menu__link-text">List </span></a></li>
                                </ul>
                            </div>
                        </li>

                        <?php } if (isset($_SESSION['project_manger']) && $_SESSION['project_manger']) { ?>
                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                                                                      class="kt-menu__link kt-menu__toggle"><i
                                            class="kt-menu__link-icon flaticon-presentation-1"></i><span
                                            class="kt-menu__link-text">Project Manger</span>
                                    <span
                                            class="kt-menu__link-badge"><span
                                                class="kt-badge kt-badge--rounded kt-badge--danger"><?= ($_SESSION['project_manger_time_sheet'] + $_SESSION['project_manger_expanse']) ?></span></span>
                                    <i
                                            class="kt-menu__ver-arrow la la-angle-right"></i>

                                </a>
                                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item " aria-haspopup="true"><a
                                                    href="<?= base_url('project/manger') ?>"
                                                    class="kt-menu__link "><i
                                                        class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                        class="kt-menu__link-text">Projects </span><span
                                                        class="kt-menu__link-badge"><span
                                                            class="kt-badge kt-badge--rounded kt-badge--warning"><?= @$_SESSION['project_manger_num'] ?></span></span>

                                            </a></li>
                                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                            data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                                                                                  class="kt-menu__link kt-menu__toggle"><i
                                                        class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span
                                                        class="kt-menu__link-text">Expanse </span>
                                                <?php if ($_SESSION['project_manger_expanse'] != 0) { ?>
                                                    <span class="kt-menu__link-badge"><span
                                                                class="kt-badge kt-badge--rounded kt-badge--info"><?= @$_SESSION['project_manger_expanse'] ?></span></span>
                                                <?php } ?>
                                                <i
                                                        class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                                <ul class="kt-menu__subnav">
                                                    <li class="kt-menu__item " aria-haspopup="true"><a
                                                                href="<?= base_url('cost/manger/pending') ?>"
                                                                class="kt-menu__link "><i
                                                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                                    class="kt-menu__link-text">Pending</span></a></li>
                                                    <li class="kt-menu__item " aria-haspopup="true"><a
                                                                href="<?= base_url('cost/manger') ?>"
                                                                class="kt-menu__link "><i
                                                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                                    class="kt-menu__link-text">History List</span></a>
                                                    </li>

                                                </ul>
                                            </div>
                                        </li>
                                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                            data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                                                                                  class="kt-menu__link kt-menu__toggle"><i
                                                        class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span
                                                        class="kt-menu__link-text">TimeSheet </span>
                                                <?php if ($_SESSION['project_manger_time_sheet'] != 0) { ?>
                                                    <span class="kt-menu__link-badge"><span
                                                                class="kt-badge kt-badge--rounded kt-badge--success"><?= @$_SESSION['project_manger_time_sheet'] ?></span></span>
                                                <?php } ?>

                                                <i
                                                        class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                                <ul class="kt-menu__subnav">
                                                    <li class="kt-menu__item " aria-haspopup="true"><a
                                                                href="<?= base_url('timework/manger/pending') ?>"
                                                                class="kt-menu__link "><i
                                                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                                    class="kt-menu__link-text">Pending</span></a></li>
                                                    <li class="kt-menu__item " aria-haspopup="true"><a
                                                                href="<?= base_url('timework/manger') ?>"
                                                                class="kt-menu__link "><i
                                                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                                    class="kt-menu__link-text">List </span></a></li>

                                                </ul>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                            </li>
                        <?php } if(isset($pre_83) || isset($pre_84) ){ ?>

                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                            data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                                                                  class="kt-menu__link kt-menu__toggle"><i
                                        class="kt-menu__link-icon flaticon-graphic"></i><span
                                        class="kt-menu__link-text">Report</span>

                                <i
                                        class="kt-menu__ver-arrow la la-angle-right"></i>

                            </a>
                            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                <ul class="kt-menu__subnav">
                                    <?php if(isset($pre_83)){?>
                                    <li class="kt-menu__item " aria-haspopup="true"><a
                                                href="<?= base_url('report/Client') ?>"
                                                class="kt-menu__link "><i
                                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                    class="kt-menu__link-text">Client </span>

                                        </a>
                                    </li>
                                    <?php }if (isset($pre_84)){?>
                                    <li class="kt-menu__item " aria-haspopup="true"><a
                                                href="<?= base_url('report/employee') ?>"
                                                class="kt-menu__link "><i
                                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                    class="kt-menu__link-text">Employee </span>

                                        </a>
                                    </li>
                                    <?php }?>

                                </ul>
                            </div>
                        </li>
                        <?php }?>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('Myproject') ?>"
                                                                           class="kt-menu__link "><i
                                        class="kt-menu__link-icon flaticon-notes"></i><span class="kt-menu__link-text">My projects</span>
                                <span class="kt-menu__link-badge"><span
                                            class="kt-badge kt-badge--rounded kt-badge--brand"><?= @$my_project ?></span></span>
                            </a>
                        </li>
                        <?php if (isset($pre_58)){?>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('timework') ?>"
                                                                           class="kt-menu__link "><i
                                        class="kt-menu__link-icon flaticon-dashboard"></i><span
                                        class="kt-menu__link-text">Work Time </span></a>
                        </li>
                        <?php } if(isset($pre_86)){?>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('cost') ?>"
                                                                           class="kt-menu__link "><i
                                        class="kt-menu__link-icon flaticon-open-box"></i><span
                                        class="kt-menu__link-text">Expanses</span></a>
                        </li>
                        <?php }?>


                        <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('timesheet') ?>"
                                                                           class="kt-menu__link "><i
                                        class="kt-menu__link-icon flaticon-calendar-with-a-clock-time-tools"></i><span
                                        class="kt-menu__link-text">Time Sheet</span></a>
                        </li>
                        <?php  if(isset($pre_86)){?>
                            <li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('welcome/backup') ?>"
                                                                               class="kt-menu__link "><i
                                        class="kt-menu__link-icon  fa fa-database
"></i><span
                                        class="kt-menu__link-text">Backup</span></a>
                            </li>
                        <?php }?>

                    </ul>
                </div>
            </div>

            <!-- end:: Aside Menu -->
        </div>

        <!-- end:: Aside -->
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <!-- begin:: Header -->
            <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

                <!-- begin:: Header Menu -->
                <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i
                            class="la la-close"></i></button>
                <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">

                </div>

                <!-- end:: Header Menu -->

                <!-- begin:: Header Topbar -->
                <div class="kt-header__topbar">

                    <!--begin: User Bar -->
                    <div class="kt-header__topbar-item kt-header__topbar-item--user">
                        <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                            <div class="kt-header__topbar-user">
                                <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
                                <span class="kt-header__topbar-username kt-hidden-mobile"><?= @$this->session->userdata('username') ?></span>
                                <img class="kt-hidden" alt="Pic"
                                     src="<?= base_url('template') ?>/media/users/300_25.jpg"/>

                                <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?= substr($this->session->userdata('username'), 0, 2)
                                    ?></span>
                            </div>
                        </div>
                        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

                            <!--begin: Head -->
                            <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"
                                 style="background-image: url(<?= base_url('template') ?>/media/misc/bg-1.jpg)">
                                <div class="kt-user-card__avatar">
                                    <img class="kt-hidden" alt="Pic"
                                         src="<?= base_url('template') ?>/media/users/300_25.jpg"/>

                                    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                    <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success"><?= substr($this->session->userdata('username'), 0, 1) ?></span>
                                </div>
                                <div class="kt-user-card__name">
                                    <?= @$this->session->userdata("username") ?>
                                </div>

                            </div>

                            <!--end: Head -->

                            <!--begin: Navigation -->
                            <div class="kt-notification">
                                <a href="<?= base_url('profile') ?>" class="kt-notification__item">
                                    <div class="kt-notification__item-icon">
                                        <i class="flaticon2-calendar-3 kt-font-success"></i>
                                    </div>
                                    <div class="kt-notification__item-details">
                                        <div class="kt-notification__item-title kt-font-bold">
                                            My Profile
                                        </div>
                                        <div class="kt-notification__item-time">
                                            Account settings and more
                                        </div>
                                    </div>
                                </a>
                                <a href="<?= base_url('Myproject') ?>" class="kt-notification__item">
                                    <div class="kt-notification__item-icon">
                                        <i class="flaticon2-hourglass kt-font-brand"></i>
                                    </div>
                                    <div class="kt-notification__item-details">
                                        <div class="kt-notification__item-title kt-font-bold">
                                            My Tasks
                                        </div>
                                        <div class="kt-notification__item-time">
                                            count my tasks : <?= $my_project ?>
                                        </div>
                                    </div>
                                </a>

                                <div class="kt-notification__custom kt-space-between">
                                    <a href="<?= base_url('logout') ?>"
                                       class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>

                                </div>
                            </div>

                            <!--end: Navigation -->
                        </div>
                    </div>

                    <!--end: User Bar -->
                </div>

                <!-- end:: Header Topbar -->
            </div>

            <!-- end:: Header -->
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

                <style>
                    .select2-container--default {
                        width: 100%;
                    }
                    a:not([href]):not([tabindex]){
                        color: #a7abc3;
                    }
                </style>