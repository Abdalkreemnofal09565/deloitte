<link href="<?= base_url('template') ?>/css/demo1/pages/general/error/error-6.css" rel="stylesheet" type="text/css" />

<div class="kt-grid__item kt-grid__item--fluid kt-grid  kt-error-v6" >
    <div class="kt-error_container">
        <div class="kt-error_subtitle kt-font-dark">
            <h1>Oops...</h1>
        </div>
        <p class="kt-error_description kt-font-dark">
            Looks like something went wrong.<br>
            We're working on it
        </p>
    </div>
</div>