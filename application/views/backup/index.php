<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand  flaticon2 flaticon2-open-box"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    Backup
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        &nbsp; <?php if (check_permission(96, false)) { ?>
                            <a href="<?= base_url('welcome/export_datebase') ?>" class="btn btn-brand btn-elevate btn-icon-sm">
                                <i class="la la-database"></i>
                                New Backup
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Employee</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i= 1;
                foreach ($list as $item) { ?>
                    <tr>
                        <td><?=$i++?></td>
                        <td><?=$item->e_first_name .' '.$item->e_last_name?></td>
                        <td><?=$item->date?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function () {

        table = $('#table').DataTable({

            "responsive": true,
            "processing": true,
            "order": [],
            "searching": true,
            "select": false,
            "ordering": false,

        });

    });
    var name_table = "client";

</script>