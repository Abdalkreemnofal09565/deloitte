<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon-open-box"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    Costs
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Client</th>
                    <th>Project</th>
                    <th>Employee</th>
                    <th>Date</th>
                    <th>Cost</th>
                    <th>Note</th>
                    <th>Date Added</th>
                    <th>State</th>
                    <th>Option</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function () {

        table = $('#table').DataTable({

            "responsive": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "searching": true,
            "select": false,
            "ordering":false,
            "ajax": {
                "url": "<?=base_url('cost/datatable')?>",
                "type": "POST"
            },

            "columns": [
                {"data": "num"},
                {"data": "client"},
                {"data": "project"},
                {"data": "employee"},
                {"data": "title"},
                {"data": "price"},
                {"data": "note"},
                {"data": "date_added"},
                {"data": "state"},
                {"data": "option"},


            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });

    });

    function reject(id) {
        $("#state_"+id).addClass("kt-badge--danger ").removeClass("kt-badge--success ").removeClass('kt-badge--warning').html("reject");
        $.ajax({
            url:"<?=base_url('cost/reject')?>",
            method:"post",
            data:{id:id}
        });
    }  function accept(id) {
        $("#state_"+id).addClass("kt-badge--success ").removeClass("kt-badge--danger ").removeClass('kt-badge--warning').html("accept");
        $.ajax({
            url:"<?=base_url('cost/accept')?>",
            method:"post",
            data:{id:id}
        });
    }
</script>