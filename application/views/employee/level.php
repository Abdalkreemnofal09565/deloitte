<div class="row  mt-5">

    <div class="col-md-10 offset-1">

        <!--Begin::Portlet-->
        <div class="kt-portlet kt-portlet--height-fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        History Level
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="dropdown dropdown-inline">
                        <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="modal"
                                data-target="#kt_modal_1">
                            <i class="flaticon2-plus"></i>
                        </button>

                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                <!--Begin::Timeline 3 -->
                <div class="kt-timeline-v2">
                    <div class="kt-timeline-v2__items  kt-padding-top-50 kt-padding-bottom-30">
                        <?php
                        foreach ($history as $item) {

                            ?>
                            <div class="kt-timeline-v2__item ">
                                <span class="kt-timeline-v2__item-time "><?=@$item->l_title?></span>
                                <div class="kt-timeline-v2__item-cricle mr-3">
                                    <i class="fa fa-genderless kt-font-brand"></i>
                                </div>
                                <div class="kt-timeline-v2__item-text  kt-padding-top-5">
                                    <?=@$item->le_date_added?> | <b> <?=@$item->l_price?> S.P</b>
                                    <br>
                                    <?=@$item->le_note?>
                                </div>
                            </div>

                            <?php

                        } ?>


                    </div>
                </div>

                <!--End::Timeline 3 -->
            </div>
        </div>

        <!--End::Portlet-->
    </div>
</div>
<div class="modal fade" id="kt_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url('employee/set_level') ?>" method="post">
            <input type="hidden" name="id" value="<?= $id ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="level">Level</label>
                        <select name="level" id="level" class="form-control">
                            <?php foreach ($level as $item) {
                                ?>
                                <option value="<?=$item->l_id?>"><?=$item->l_title?></option>

                                <?php

                            } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="note">Note</label>
                        <textarea name="note" id="note" rows="4" placeholder="Note" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>

