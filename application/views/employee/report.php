<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

    <!--Begin::App-->
    <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

        <!--Begin:: App Aside Mobile Toggle-->
        <button class="kt-app__aside-close" id="kt_user_profile_aside_close">
            <i class="la la-close"></i>
        </button>

        <!--End:: App Aside Mobile Toggle-->

        <!--Begin:: App Aside-->
        <div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside" style="opacity: 1;">

            <div class="kt-portlet kt-portlet--height-fluid-">
                <div class="kt-portlet__head  kt-portlet__head--noborder">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Infromation Employee
                        </h3>
                    </div>

                </div>
                <div class="kt-portlet__body kt-portlet__body--fit-y">

                    <!--begin::Widget -->
                    <div class="kt-widget kt-widget--user-profile-1">

                        <div class="kt-widget__body">
                            <div class="kt-widget__content">
                                <div class="kt-widget__info">
                                    <span class="kt-widget__label">Name:</span>
                                    <a href="#"
                                       class="kt-widget__data"><?= $employee->e_first_name . ' ' . $employee->e_last_name ?></a>
                                </div>
                                <div class="kt-widget__info">
                                    <span class="kt-widget__label">Identify Number:</span>
                                    <a href="#" class="kt-widget__data"><?= $employee->e_identify_number ?></a>
                                </div>
                                <div class="kt-widget__info">
                                    <span class="kt-widget__label">Email:</span>
                                    <a href="#" class="kt-widget__data"><?= $employee->e_email ?></a>
                                </div>
                                <div class="kt-widget__info">
                                    <span class="kt-widget__label">Level:</span>
                                    <a href="#" class="kt-widget__data"><?= $last_level ?></a>
                                </div>

                                <div class="kt-widget__info">
                                    <span class="kt-widget__label">budget:</span>
                                    <a href="#" class="kt-widget__data"><?= $budget ?> <b>per.hour</b></a>
                                </div>

                                <div class="kt-widget__info">
                                    <span class="kt-widget__label">Note:</span>
                                    <span class="kt-widget__data"><?= $employee->e_note ?></span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="kt-widget kt-widget--user-profile-1">
                        <div class="kt-widget__body">
                            <h5> History level
                            </h5>
                        </div>
                        <hr>
                        <div class="kt-widget__body">
                            <div class="kt-timeline-v2">
                                <div class="kt-timeline-v2__items  kt-padding-top-50 kt-padding-bottom-30">
                                    <?php
                                    foreach ($levels as $item) {

                                        ?>
                                        <div class="kt-timeline-v2__item ">
                                            <span class="kt-timeline-v2__item-time "><?= @$item->l_title ?></span>
                                            <div class="kt-timeline-v2__item-cricle mr-3">
                                                <i class="fa fa-genderless kt-font-brand"></i>
                                            </div>
                                            <div class="kt-timeline-v2__item-text  kt-padding-top-5">
                                                <?= @$item->le_date_added ?> | <b> <?= @$item->l_price ?> S.P</b>
                                                <br>
                                                <?= @$item->le_note ?>
                                            </div>
                                        </div>

                                        <?php

                                    } ?>


                                </div>
                            </div>

                        </div>
                    </div>


                </div>
            </div>
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="kt-widget1 kt-widget1--fit">
                        <div class="kt-widget1__item">
                            <div class="kt-widget1__info">
                                <h3 class="kt-widget1__title">Count project</h3>
                                <span class="kt-widget1__desc"></span>
                            </div>
                            <span class="kt-widget1__number kt-font-muted"><?= $report['all_project'] ?></span>
                        </div>
                        <div class="kt-widget1__item">
                            <div class="kt-widget1__info">
                                <h3 class="kt-widget1__title">Hours</h3>
                                <span class="kt-widget1__desc">In all project</span>
                            </div>
                            <span class="kt-widget1__number kt-font-muted"><?= $report['all_hours'] ?></span>
                        </div>
                        <div class="kt-widget1__item">
                            <div class="kt-widget1__info">
                                <h3 class="kt-widget1__title">Cost Hours</h3>
                                <span class="kt-widget1__desc">In all project</span>
                            </div>
                            <span class="kt-widget1__number "><?= (float)$report['all_price_hours'] ?>  <sub class="text-muted">S.P</sub></span>
                        </div>
                        <div class="kt-widget1__item">
                            <div class="kt-widget1__info">
                                <h3 class="kt-widget1__title">Expanse</h3>
                                <span class="kt-widget1__desc">In all project</span>
                            </div>
                            <span class="kt-widget1__number text-muted"><?= $report['all_expanse'] ?>  <sub class="text-muted">S.P</sub></span>
                        </div>


                        <div class="kt-widget1__item">
                            <div class="kt-widget1__info">
                                <h3 class="kt-widget1__title">Expanse+Cost Hours</h3>
                                <span class="kt-widget1__desc">In all project</span>
                            </div>
                            <span class="kt-widget1__number kt-font-muted"><?= (float)$report['all_price_hours'] + $report['all_expanse'] ?> <sub class="text-muted">S.P</sub></span>
                        </div>






                    </div>
                </div>
            </div>
        </div>

        <!--End:: App Aside-->

        <!--Begin:: App Content-->
        <div class="kt-grid__item kt-grid__item--fluid kt-app__content">
            <div class="row">

                <div class="col-xl-12">

                    <div class="kt-portlet kt-portlet--height-fluid">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Projects
                                </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">
                                <div class="dropdown dropdown-inline">
                                    <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="flaticon-more-1"></i>
                                    </button>

                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div class="kt-widget6">
                                <div class="kt-widget6__head">
                                    <div class="kt-widget6__item">
                                        <span> Client</span>
                                        <span> Project</span>
                                        <span>Count Hours</span>
                                        <span>Cost Hours</span>
                                        <span>Expanse</span>
                                        <span>Cost</span>
                                    </div>
                                </div>
                                <div class="kt-widget6__body">
                                    <?php
                                    foreach ($report['projects'] as $project) {
                                        ?>
                                        <div class="kt-widget6__item">
                                            <span><?= $project['info']->client ?></span>
                                            <span><?= $project['info']->project ?></span>

                                            <span><?= $project['hours'] ?></span>
                                            <span><?= (float)$project['price_hours'] . '.SP' ?></span>
                                            <span><?= (float)$project['all_expanse'] . '.SP' ?></span>
                                            <span><?= (float)$project['all_expanse'] + (float)$project['price_hours'] . '.SP' ?></span>
                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php foreach ($report['dates'] as $date) { ?>
                    <div class="col-xl-12">

                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        <?= $date['year'] ?>
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-xl-4">

                                        <!--begin:: Widgets/Stats2-1 -->
                                        <div class="kt-widget1">
                                            <div class="kt-widget1__item">
                                                <div class="kt-widget1__info">
                                                    <h3 class="kt-widget1__title">Hours</h3>

                                                </div>
                                                <span class="kt-widget1__number kt-font-brand">    <?= $date['hours'] ?></span>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-12 col-lg-12 col-xl-4">

                                        <!--begin:: Widgets/Stats2-1 -->
                                        <div class="kt-widget1">
                                            <div class="kt-widget1__item">
                                                <div class="kt-widget1__info">
                                                    <h3 class="kt-widget1__title">Cost Hours</h3>

                                                </div>
                                                <span class="kt-widget1__number kt-font-danger"> <?= (float)$date['price_hours'] ?></span>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-12 col-lg-12 col-xl-4">

                                        <!--begin:: Widgets/Stats2-1 -->
                                        <div class="kt-widget1">
                                            <div class="kt-widget1__item">
                                                <div class="kt-widget1__info">
                                                    <h3 class="kt-widget1__title">Expanse</h3>

                                                </div>
                                                <span class="kt-widget1__number kt-font-success">    <?= $date['all_expanse'] ?></span>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                                <div class="kt-scroll ps ps--active-y" data-scroll="true"
                                     style="height: 120px; overflow: hidden;">
                                    <table class="table">
                                        <thead>
                                        <th>Month</th>
                                        <th>Count.Hours</th>
                                        <th>Expanse</th>
                                        <th>Cost</th>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($date['month'] as $datum){?>
                                        <tr>
                                            <td><?=$datum['month']?></td>
                                            <td><?=$datum['hours']?></td>
                                            <td><?=$datum['all_expanse']?></td>
                                            <td><?=$datum['all_expanse']+(float)$datum['price_hours']?></td>
                                        </tr>
                                        <?php }?>
                                        </tbody>
                                    </table>
                                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                                    </div>
                                    <div class="ps__rail-y" style="top: 0px; height: 200px; right: 0px;">
                                        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 40px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <!--End:: App Content-->
    </div>

    <!--End::App-->
</div>