<div class="row mt-5">
    <div class="col-md-10 offset-1">

        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        New Employee
                    </h3>
                </div>
            </div>

            <!--begin::Form-->
            <form class="kt-form" id="form_category">
                <div class="kt-portlet__body">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" name="first_name" id="first_name" class="form-control"

                                       placeholder="First Name">
                                <b id="error_first_name" class="text-danger"></b>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" name="last_name" id="last_name" class="form-control"
                                       placeholder="Last Name">
                                <b id="error_last_name" class="text-danger"></b>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="identify_number">Identify Number</label>
                                <input type="text" name="identify_number" id="identify_number" class="form-control"
                                       placeholder="Identify Number">
                                <b id="error_identify_number" class="text-danger"></b>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" name="email" id="email" class="form-control"
                                       aria-describedby="emailHelp"
                                       placeholder="Email ">
                                <b id="error_email" class="text-danger"></b>
                            </div>
                        </div>

                        <?php
                        foreach ($additional_data as $item){
                            ?>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="<?=$item['category']?>"><?=$item['category']?></label>

                                    <select name="sub_category[]" id="<?=$item['category']?>"   class="form-control select2" required>
                                        <?php foreach ($item['sub_category'] as $item2 ){
                                            ?>
                                            <option value="<?=$item2->sc_id?>"> <?=$item2->sc_title?></option>
                                            <?php
                                        }?>
                                        <option selected value="">empty</option>
                                    </select>

                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="note">Note</label>
                             <textarea rows="3" onresize="false" class="form-control" name="note" placeholder="Note"></textarea>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-primary" id="submit">Submit</button>
                        <a href="<?= base_url('employee') ?>" class="btn btn-secondary">Cancel</a>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

    </div>

</div>
<script>
    $(document).ready(function () {
        $('.select2').select2({
            width: '100%'
        });
    });
    $("#error").hide();
    $("#form_category").submit(function (event) {
        event.preventDefault();
        $("#submit").attr("disabled", "disabled").addClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").html("in processing");
        $.ajax({
            url: "<?=base_url('employee/create')?>",
            method: "post",
            data: $("#form_category").serialize(),
            success: function (data) {
                var response = JSON.parse(data);
                if (response.result == "1") {
                    $("#submit").html("Done").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").addClass("btn-outline-success").removeClass("btn-primary");
                    $("#error").hide();
                    setTimeout(function () {
                        window.location.href = "<?=base_url('employee')?>";
                    }, 2000)
                } else {
                    $("#submit").html("Submit").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").removeAttr("disabled");
                    $("#error_first_name").html(response.first_name).show();
                    $("#error_last_name").html(response.last_name).show();
                    $("#error_identify_number").html(response.identify_number).show();
                    $("#error_email").html(response.email).show();

                }
            }
        })
    });
</script>
