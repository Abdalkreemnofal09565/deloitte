
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon-users"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    Employees
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">

                        <a href="<?=base_url('employee/add')?>" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            New Record
                        </a>
                        <a href="<?=base_url('employee/export')?>" class="btn btn-outline-brand btn-elevate btn-icon-sm">
                            <i class="la la-download"></i> Export Excel
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Id</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Email</th>
                    <th>Date added</th>
                    <th>Note</th>
                    <?php
                    foreach ($category as $item){
                        ?>
                        <th><?=$item->c_title?></th>
                    <?php
                    }
                    ?>
                    <th>State</th>
                    <th>Option</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function () {

        table = $('#table').DataTable({

            "responsive": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "searching": true,
            "select": false,
            "ordering": false,
            "ajax": {
                "url": "<?=base_url('employee/datatable')?>",
                "type": "POST"
            },

            "columns": [
                {
                    "data": "num"
                },
                {
                    "data": "id_employee"
                },
                {
                    "data": "first_name"
                },
                {
                    "data": "last_name"
                },
                {
                    "data": "email"
                },
                {
                    "data": "date_register"
                },
                {
                    "data": "note"
                }
                 <?php
                    foreach ($category as $item){
                        ?>
                ,

                {
                    "data": "<?=$item->c_title?>"
                }                    <?php
                    }?>
                ,

                {
                    "data": "state"
                }
                ,
                {
                    "data": "option"
                }

            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });

    });
var name_table = "employee";

</script>