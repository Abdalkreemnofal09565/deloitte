<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">

                    <div class="row">

                        <div class="col">
                            <h4><b>First Name : </b> <?= $info_employee['info']->e_first_name; ?>
                            </h4>
                            <hr>
                        </div>
                        <div class="col">
                            <h4><b>Last Name : </b> <?= $info_employee['info']->e_last_name; ?>
                            </h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col">
                            <h4><b> Identify Number: </b> <?= $info_employee['info']->e_identify_number; ?>
                            </h4>
                            <hr>
                        </div>
                        <div class="col">
                            <h4><b>Email : </b> <?= $info_employee['info']->e_email; ?>
                            </h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col">
                            <h4><b> Date Added: </b> <?= $info_employee['info']->e_date_added; ?>
                            </h4>
                            <hr>
                        </div>
                        <div class="col">
                            <h4><b>Level : </b> <?php
                                if (count($info_employee['level']) != 0) {
                                    echo $info_employee['level'][0]->l_title;
                                } else {
                                    ?>
                                    <a href="<?= base_url('employee/level/' . $info_employee['info']->e_id) ?>">You must
                                        adjust the level</a>

                                    <?php
                                }

                                ?>
                            </h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col">
                            <h4><b> Note: </b><?= $info_employee['info']->e_note; ?>
                            </h4>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row">
        <input type="hidden" id="employee" value="<?= $info_employee['info']->e_id; ?>">
        <?php foreach ($permission as $per) {
            if (count($per['sub_role']) != 0) {
                ?>
                <div class="col-md-3">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    <?= $per['role']->r_title ?>
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <!--begin::Form-->
                            <form class="kt-form">
                                <div class="form-group">
                                    <div class="kt-checkbox-list">
                                        <?php foreach ($per['sub_role'] as $sub_role) { ?>
                                            <label class="kt-checkbox">
                                                <input type="checkbox"
                                                       onchange="permission(<?= $sub_role['sub_id'] ?>)" <?php if ($sub_role['is_active'] == '1') echo 'checked'; ?> > <?= $sub_role['sub_title'] ?>
                                                <span></span>
                                            </label>
                                            <span class="form-text text-muted"
                                                  id="message_<?= $sub_role['sub_id'] ?>"></span>

                                        <?php } ?>

                                    </div>

                                </div>
                            </form>

                            <!--end::Form-->
                        </div>
                    </div>

                    <!--end::Portlet-->
                </div>
            <?php }
        } ?>
    </div>
</div>
<style>

</style>
<script>
    function permission(persmission) {
        $("#message_" + persmission).html("<i class='kt-spinner kt-spinner--v2 kt-spinner--sm kt-spinner--success' ></i><span style='margin-left :20px'>   please wite</span>")
        $.ajax({
            url: "<?=base_url('employee/set_permission')?>",
            method: "post",
            data: {persmission: persmission, employee: $("#employee").val()},
            success: function (data) {
                var response = JSON.parse(data);
                if(response.result == "1"){
                    $("#message_" + persmission).html("<i class='text-success flaticon2 flaticon2-check-mark '></i>Done");
                    setTimeout(function () {
                        $("#message_" + persmission).html("");
                    },3000);
                }else{
                    $("#message_" + persmission).html("<i class='text-danger flaticon flaticon-cancel '></i>Something error");

                }
            }

        });
    }
</script>
