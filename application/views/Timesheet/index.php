<link href="<?= base_url('vendors/general/sweetalert2/dist/sweetalert2.css') ?>" rel="stylesheet" type="text/css"/>

<script src="<?= base_url('template/vendors/general/sweetalert2/dist/sweetalert2.min.js') ?>"
        type="text/javascript"></script>
<script src="<?= base_url('template/vendors/custom/js/vendors/sweetalert2.init.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('template/js/demo1/pages/components/extended/sweetalert2.js') ?>"
        type="text/javascript"></script>

<div class="kt-content  " id="kt_content">
    <div class="row">
        <div class="col-12">
            <div class="kt-portlet kt-portlet--tabs">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Export Data <b style="font-size: x-small" class="text-danger">You must specify the time
                                period</b>

                        </h3>
                    </div>

                </div>
                <div class="kt-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_portlet_base_demo_5_tab_content" role="tabpanel">
                            <form id="export_data" action="<?= base_url('Timesheet/export_data') ?>" method="post">
                                <div class="row">
                                    <div class="col form-group">
                                        <label for="type">Type</label>
                                        <select name="data_type" id="type" class="form-control">

                                            <option value="expanse">Expanse</option>
                                            <option value="worktime">Work Time</option>
                                        </select>
                                    </div>
                                    <div class="col form-group">
                                        <label for="start_date">Start Date</label>
                                        <input id="start_date" name="start_date" class="form-control" type="text">
                                    </div>
                                    <div class="col form-group">
                                        <label for="start_date">End Date</label>
                                        <input id="end_date" name="end_date" class="form-control" type="text">
                                    </div>
                                    <div class="col-12">
                                        <button class="btn btn-primary" type="submit" id="button_export">Export Data
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="kt-portlet kt-portlet--tabs">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button class="btn btn-default" id="prev" type="button"><i
                                            class="flaticon2-back flaticon2"></i>
                                </button>
                            </div>
                            <input type="text" class="form-control" placeholder="Select date"
                                   id="date">
                            <div class="input-group-append">
                                <button class="btn btn-default" id="next" type="button"><i
                                            class="flaticon2-next flaticon2"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-right" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link " data-toggle="tab" href="#kt_portlet_base_demo_1_tab_content"
                                   role="tab" aria-selected="true">
                                    <i class="flaticon-price-tag"></i> Expense
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab"
                                   href="#kt_portlet_base_demo_2_tab_content"
                                   role="tab" aria-selected="false">
                                    <i class="flaticon2-crisp-icons-1"></i> Work Time
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane " id="kt_portlet_base_demo_1_tab_content" role="tabpanel">

                            <form id="form_expense">
                                <input id="expense_date" name="expense_date" type="hidden">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <label for="expense_type"><b>E</b>xpense <b>T</b>ype</label>
                                            </div>
                                            <div class="col-12">
                                                <select name="expense_type" id="expense_type"
                                                        class="form-control kt-select2">
                                                    <?php
                                                    foreach ($expense_type as $ex_type) {
                                                        ?>
                                                        <option value="<?= $ex_type->ct_id ?>"><?= $ex_type->ct_title ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                <b class="text-danger" id="expense_type_error"></b>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <label for="expense_client"><b>C</b>lient</label>
                                            </div>
                                            <div class="col-12">
                                                <select name="expense_project" id="expense_client"
                                                        class="form-control kt-select2">
                                                    <option value="0" selected>select client</option>
                                                    <?php
                                                    foreach ($clients as $client) {
                                                        ?>
                                                        <option value="<?= $client->c_id ?>"><?= $client->client ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                <b class="text-danger" id=""></b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <label for="expense_project"><b>E</b>xpense <b>P</b>roject</label>
                                            </div>
                                            <div class="col-12">
                                                <select name="expense_project" id="expense_project"
                                                        class="form-control kt-select2">
                                                    <?php
                                                    foreach ($myproject as $project) {
                                                        ?>
                                                        <option value="<?= $project->p_id ?>"><?= $project->client . '-' . $project->project ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                <b class="text-danger" id="expense_project_error"></b>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <label for="expense"><b>Expense</b></label>
                                            </div>
                                            <div class="col-12">
                                                <div class="input-group">

                                                    <input type="number" class="form-control is-invalid" min="0"
                                                           name="expense" id="expense" value="1">
                                                    <div class="input-group-prepend"><span class="input-group-text"
                                                                                           id="basic-addon1">S.P</span>
                                                    </div>


                                                </div>
                                                <b class="text-danger" id="expense_error"></b>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <label for="expense_note"><b>E</b>xpense <B>N</B>ote</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="text" class="form-control" id="expense_note"
                                                       name="expense_note">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 text-right">
                                        <button class="btn btn-outline-brand " id="btn_submit_expense"
                                                type="submit">Add
                                            Expense
                                        </button>
                                    </div>
                                </div>

                            </form>
                            <div class="kt-separator kt-separator--dashed"></div>
                            <div class="row">
                                <div class="col-12">
                                    <table class="table" id="table_expense">
                                        <thead>
                                        <tr>
                                            <th><b>#</b></th>
                                            <th><b>C</b>lient</th>
                                            <th><b>P</b>oject</th>
                                            <th><b>E</b>xpense <b>T</b>ype</th>

                                            <th><b>E</b>xpense</th>
                                            <th><b>E</b>xpense <b>N</b>ote</th>
                                            <th><b>D</b>ate <b>A</b>dded</th>
                                            <th><b>S</b>tate</th>
                                            <th><b>O</b>ption</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane active" id="kt_portlet_base_demo_2_tab_content" role="tabpanel">


                            <form id="form_work_time">
                                <input type="hidden" id="date_hour" name="date_hour">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <label for="hours_type"><b>H</b>our <b>T</b>ype</label>
                                            </div>
                                            <div class="col-12">
                                                <select name="hours_type" id="hours_type"
                                                        class="form-control kt-select2">
                                                    <?php
                                                    foreach ($hours_type as $ht_type) {
                                                        ?>
                                                        <option value="<?= $ht_type->ht_id ?>"><?= $ht_type->ht_title ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                <b class="text-danger" id="hours_type_error"></b>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <label for="worktime_client"><b>C</b>lient</label>
                                            </div>
                                            <div class="col-12">
                                                <select name="expense_project" id="worktime_client"
                                                        class="form-control kt-select2">
                                                    <option value="0" selected>select client</option>
                                                    <?php
                                                    foreach ($clients as $client) {
                                                        ?>
                                                        <option value="<?= $client->c_id ?>"><?= $client->client ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                <b class="text-danger" id=""></b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">


                                        <div class="form-group row">
                                            <div class="col-12">
                                                <label for="hour_project"> <b>P</b>roject</label>
                                            </div>
                                            <div class="col-12">
                                                <select name="hour_project" id="hour_project"
                                                        class="form-control kt-select2">
                                                    <?php
                                                    foreach ($myproject as $project) {
                                                        ?>
                                                        <option value="<?= $project->p_id ?>"><?= $project->client . '-' . $project->project ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                <b class="text-danger" id="expense_project_error"></b>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="col-12">
                                            <label for="start_time"><b>S</b>tart <b>T</b>ime</label>
                                        </div>
                                        <div class="col-12">
                                            <input class="form-control" onchange="start_Time()" id="start_time"
                                                   name="start_time"
                                                   readonly="" value="10:30"
                                                   type="text">
                                        </div>
                                        <b id="start_time_error"></b>
                                    </div>
                                    <div class="col">
                                        <div class="col-12">
                                            <label for="start_time"><b>E</b>nd <b>T</b>ime</label>
                                        </div>
                                        <div class="col-12">
                                            <input class="form-control" onchange="end_Time()" id="end_time"
                                                   readonly=""
                                                   value="10:30" name="end_time"
                                                   type="text">
                                            <b id="end_time_error"></b>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="col-12">
                                            <label for="start_time"><b>C</b>ount <b>H</b>ours </label>
                                        </div>
                                        <div class="col-12">
                                            <input class="form-control" id="count_hours" readonly="" value="0"
                                                   type="text" name="count_hours">
                                            <b class="text-danger" id="count_hours_error"></b>
                                        </div>

                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-12 text-right">
                                        <button class="btn btn-outline-brand" type="submit" id="btn_add_to_work">Add
                                            Work Time
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <div class="kt-separator kt-separator--dashed"></div>
                            <div class="row">
                                <div class="col-12">
                                    <table class="table" id="table_workTime">
                                        <thead>
                                        <tr>
                                            <th><b>#</b></th>
                                            <th><b>C</b>lient</th>
                                            <th><b>P</b>roject</th>
                                            <th><b>S</b>tart <b>T</b>ime</th>
                                            <th><b>E</b>nd <b>T</b>ime</th>
                                            <th><b>C</b>ount <b>H</b>our</th>
                                            <th><b>T</b>ype <b>H</b>our</th>
                                            <th><b>D</b>ate <b>A</b>dded</th>
                                            <th><b>S</b>tate</th>
                                            <th><b>O</b>ption</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    var table;
    var MYdate;

    function table_expense(valueDate) {

        $('#table_expense').DataTable({

            "responsive": true,
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "ordering": false,
            "searching": true,
            "select": false,
            "paging": true,
            "ajax": {
                "url": "<?=base_url('timesheet/datatable_expense')?>",
                "type": "POST",
                "data": {
                    "date": valueDate,
                }
            },

            "columns": [
                {"data": "num"},
                {"data": "client"},
                {"data": "project"},
                {"data": "payment_type"},

                {"data": "price"},
                {"data": "note"},
                {"data": "date_added"},
                {"data": "state"},
                {"data": "option"},
            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });
        $("#expense_date").val(valueDate);

    }

    function table_workTime(valueDate) {

        $('#table_workTime').DataTable({

            "responsive": true,
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "ordering": false,
            "searching": true,
            "select": false,
            "paging": true,
            "ajax": {
                "url": "<?=base_url('timesheet/datatable_hours')?>",
                "type": "POST",
                "data": {
                    "date": valueDate,
                }
            },

            "columns": [
                {"data": "num"},
                {"data": "client"},
                {"data": "project"},
                {"data": "hoursType"},
                {"data": "start_time"},
                {"data": "end_time"},
                {"data": "hours"},
                {"data": "date_added"},
                {"data": "state"},
                {"data": "option"},
            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });
        $("#expense_date").val(valueDate);
        $("#date_hour").val(valueDate);

    }

    function remove_cp(id) {
        $("#item_remove_" + id).hide();
        $.ajax({
            url: "<?=base_url('myproject/remove_payment/')?>" + id
        });
        table_expense($("#date").val());
    }

    function remove_ph(id) {
        $("#item_remove_ph" + id).hide();
        $.ajax({
            url: "<?=base_url('myproject/remove_hours/')?>" + id
        });
        table2.ajax.reload();
    }

    function start_Time() {
        var errorStartTime = $("#start_time_error");
        var start_time = $("#start_time");
        errorStartTime.html('<i class="kt-spinner kt-spinner--sm kt-spinner--brand"></i> <b style="margin-left: 20px">Checking</b>');
        $.ajax({
            url: "<?=base_url('timesheet/checktime')?>",
            method: "post",
            data: {
                time: $("#start_time").val(),
                date_hour: $("#date").val()
            },
            success: function (data) {
                var response = JSON.parse(data);
                if (response.result == 0) {
                    errorStartTime.html("<b class='text-danger'>" + response.error + "</b>");
                    $("#btn_add_to_work").attr('di')
                    start_time.addClass('is-invalid').removeClass('is-valid');
                } else {
                    start_time.removeClass('is-invalid').addClass('is-valid');
                    errorStartTime.html("<b class='text-success'>the start time is valid</b>");
                }
            }
        });

        workTime();
    }

    function end_Time() {
        var errorStartTime = $("#end_time_error");
        var start_time = $("#end_time");
        errorStartTime.html('<i class="kt-spinner kt-spinner--sm kt-spinner--brand"></i> <b style="margin-left: 20px">Checking</b>');
        $.ajax({
            url: "<?=base_url('timesheet/checktime')?>",
            method: "post",
            data: {
                time: $("#end_time").val(),
                date_hour: $("#date").val()
            },
            success: function (data) {
                var response = JSON.parse(data);
                if (response.result == 0) {
                    errorStartTime.html("<b class='text-danger'>" + response.error + "</b>");
                    start_time.addClass('is-invalid').removeClass('is-valid');
                } else {
                    start_time.removeClass('is-invalid').addClass('is-valid');

                    errorStartTime.html("<b class='text-success'>the end time is valid</b>");
                }
            }
        });

        workTime();
    }

    function workTime() {
        var start = $("#start_time");
        var end = $("#end_time");
        var count = $("#count_hours");
        var errorV = $("#count_hours_error");
        // alert(1);
        console.log("start  = " + start.val() + " end " + end.val())
        var startDate = new Date("1/1/1900 " + start.val());
        var endDate = new Date("1/1/1900 " + end.val());
        var difftime = (endDate - startDate) / 3600000;

        if (startDate > endDate) {
            count.addClass('is-invalid').removeClass('is-valid');
            errorV.text('The start time is greater than the end time');

        } else {
            count.addClass('is-valid').removeClass('is-invalid');
            count.val(difftime);
            errorV.text('');

        }

    }

    $(document).ready(function () {

        $("#date").datepicker({
            format: 'yyyy-mm-dd'
        }).datepicker("setDate", new Date()).datepicker().on('changeDate', function () {

            // MYdate=   $("#date").datepicker.formatDate("yy-mm-dd", )
            // alert($("#date").val());
            var Mdate = $("#date").val().toString();
            $("#expense_date").val(Mdate);
            $("#date_hour").val(Mdate);
            table_expense(Mdate);
            table_workTime(Mdate);
            start_Time();
            end_Time();
        });
        $("#start_date").datepicker({
            format: 'yyyy-mm-dd'
        }).datepicker("setDate", new Date()).datepicker();
        $("#end_date").datepicker({
            format: 'yyyy-mm-dd'
        }).datepicker("setDate", new Date()).datepicker();
        $('#expense_type').select2({
            placeholder: "Select a Type Expense",
            width: '100%'
        });
        $('#type').select2({});
        $('#hours_type').select2({
            placeholder: "Select a Type Hour",
            width: '100%'
        });
        $('#expense_project').select2({
            placeholder: "Select a Project",
            width: '100%'
        });
        $('#hour_project').select2({
            placeholder: "Select a Project",
            width: '100%'
        });
        $('#expense_client').select2({
            placeholder: "Select a Client   ",
            width: '100%'
        });$('#worktime_client').select2({
            placeholder: "Select a Client   ",
            width: '100%'
        });
        $("#expense_client").on('change', function () {
            // alert();
            $.ajax({
                url: "<?=base_url('timesheet/getmyproject')?>",
                data: {client_id: $("#expense_client").val()},
                method: "post",
                success: function (response) {
                    $("#expense_project").empty().append(response);

                }
            });

        });
        $("#worktime_client").on('change', function () {
            // alert();
            $.ajax({
                url: "<?=base_url('timesheet/getmyproject')?>",
                data: {client_id: $("#worktime_client").val()},
                method: "post",
                success: function (response) {
                    $("#hour_project").empty().append(response);

                }
            });

        });

        $("#expense").ready(function () {
            if (this.value == "") {
                $("#expense").addClass("is-invalid").removeClass("is-valid");
            } else if (this.value <= 0) {
                $("#expense").addClass("is-invalid").removeClass("is-valid");
            } else {
                $("#expense").removeClass("is-invalid").addClass("is-valid");

            }
        });
        $("#expense").change(function () {
            if (this.value == "") {
                $("#expense").addClass("is-invalid").removeClass("is-valid");
            } else if (this.value <= 0) {
                $("#expense").addClass("is-invalid").removeClass("is-valid");
            } else {
                $("#expense").removeClass("is-invalid").addClass("is-valid");

            }
        });

        $('#start_time').timepicker({
            defaultTime: '10:30',
            minuteStep: 30,
            showSeconds: false,
            showMeridian: false,
        });
        $('#end_time').timepicker({
            defaultTime: '10:30',
            minuteStep: 30,
            showSeconds: false,
            showMeridian: false,
        });
        $('#next').on("click", function () {
            var date = $('#date').datepicker('getDate');
            date.setTime(date.getTime() + (1000 * 60 * 60 * 24))
            $('#date').datepicker("setDate", date);
        });
        $('#prev').on("click", function () {
            var date = $('#date').datepicker('getDate');
            date.setTime(date.getTime() - (1000 * 60 * 60 * 24))
            $('#date').datepicker("setDate", date);
        });
        $("#form_work_time").on('submit', function (event) {
            event.preventDefault();
            $("#btn_add_to_work").html("In Processing").addClass(" kt-spinner kt-spinner--v2 kt-spinner--lg kt-spinner--brand");
            $.ajax({
                url: "<?=base_url('timesheet/create_hours')?>",
                method: "post",
                data: $("#form_work_time").serialize(),
                success: function (data) {
                    var response = JSON.parse(data);
                    $("#btn_add_to_work").html('Add Work Time').removeClass(" kt-spinner kt-spinner--v2 kt-spinner--lg kt-spinner--brand");
                    ;

                    if (response.result == 1) {
                        $("#form_work_time").resetForm();
                        table_workTime($("#date").val());
                    } else if (response.result == "-1") {
                        swal.fire("Good job!", response.problem, "error");

                    } else {
                        // $("#expense_type_error").html(response.type);
                        // $("#expense_project_error").html(response.project);
                        // $("#expense_title_error").html(response.title);
                        // $("#expense_error").html(response.expense);
                    }


                }
            });
        });
        $("#form_expense").on('submit', function (event) {
            event.preventDefault();
            $("#btn_submit_expense").html("In Processing").addClass(" kt-spinner kt-spinner--v2 kt-spinner--lg kt-spinner--brand");
            $.ajax({
                url: "<?=base_url('timesheet/create_expense')?>",
                method: "post",
                data: $("#form_expense").serialize(),
                success: function (data) {
                    var response = JSON.parse(data);
                    $("#btn_submit_expense").html('Add Expense').removeClass(" kt-spinner kt-spinner--v2 kt-spinner--lg kt-spinner--brand");
                    ;

                    if (response.result == 1) {
                        $("#form_expense").resetForm();
                        table_expense($("#date").val());
                    } else {
                        $("#expense_type_error").html(response.type);
                        $("#expense_project_error").html(response.project);

                        $("#expense_error").html(response.expense);
                    }


                }
            });
        });
        table_expense($("#date").val());
        table_workTime($("#date").val());
    });

    var name_table = "employee";
</script>
