<div class="kt-portlet">
    <div class="kt-portlet__body">
        <table style="overflow-x:auto;" class="table table-bordered" id="table">
            <thead>
            <tr>
                <th>client</th>
                <th>project</th>
                <th>Type</th>
                <th>reference</th>
                <th>Debit</th>
                <th>Credit</th>
                <th>Date</th>
                <th>Date Added</th>
                <th>State</th>


            </tr>
            </thead>
            <tbody>
            <?php foreach ($reports as $report) { ?>
            <tr>
                <td><?= $report['client'] ?></td>
                <td><?= $report['project'] ?></td>
                <td><?= $report['type'] ?></td>
                <td><?= $report['reference'] ?></td>
                <td><?= $report['Debit'] ?></td>
                <td><?= $report['Credit'] ?></td>
                <td><?= $report['date'] ?></td>
                <td><?= $report['date_added'] ?></td>
                <td><?= $report['state'] ?></td>

                <?php } ?>
            </tbody>

        </table>
    </div>
</div>
<script>
    $("#table").DataTable({
        // "responsive": true,
        "processing": true,
        "order": [],
        "searching": true,
        "select": false,
        "ordering": false,
        "scrollX": true, 'paging': false,
        "dom": 'Bfrtip',
        "buttons": [
            'colvis',
            'excel',
            'print'
        ]
    });
</script>