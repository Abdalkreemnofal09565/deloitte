<div class="kt-portlet">
    <div class="kt-portlet__body">
        <table class="table table-bordered" id="table">
            <thead>
            <tr>
                <th>Employee</th>
                <th>client</th>
                <th>project</th>

                <th>Expanse Type</th>
                <th>Expanse</th>
                <th>Note</th>
                <th>Date</th>



            </tr>
            </thead>
            <tbody>
            <?php foreach ($reports as $report) { ?>
                <tr>
                    <td><?= $report->employee?></td>
                    <td><?= $report->client ?></td>
                    <td><?= $report->project ?></td>
                    <td><?= $report->payment_type?></td>
                    <td><?= $report->cp_price?></td>
                    <td><?= $report->cp_note?></td>
                    <td><?= $report->cp_date ?></td>
                </tr>
            <?php } ?>
            </tbody>

        </table>
    </div>
</div>
<script>
    $("#table").DataTable({
        "paging": false,
        "processing": true,
        "order": [],
        "searching": true,
        "select": false,
        "ordering": false,
        "scrollY": 300,
        "scrollX": true,
        "dom": 'Bfrtip',
        "buttons": [
            'colvis',
            'excel',
            'print'
        ]
    });
</script>