<div class="kt-portlet">
    <div class="kt-portlet__body">
        <table style="overflow-x:auto;" class="table table-bordered" id="table">
            <thead>
            <tr>
                <th>client</th>
                <th>project</th>
                <th>Type Hour</th>
                <th>Start Time</th>
                <th>End Time</th>
                <th>Date</th>
                <th>Date Added</th>
                <th>Level</th>
                <th>Level price <sub style="font-size: xx-small">per.hour</sub></th>
                <th>Employee</th>
                <th> Hours</th>
                <th>price Hours</th>
                <th>State</th>


            </tr>
            </thead>
            <tbody>
            <?php foreach ($reports as $report) { ?>
            <tr>
                <td><?= $report['client'] ?></td>
                <td><?= $report['project'] ?></td>
                <td><?= $report['type'] ?></td>
                <td><?= $report['start_time'] ?></td>
                <td><?= $report['end_time'] ?></td>
                <td><?= $report['date'] ?></td>
                <td><?= $report['date_added'] ?></td>
                <td><?= $report['level'] ?></td>
                <td><?= $report['level_price'] ?></td>
                <td><?= $report['employee'] ?></td>
                <td><?= $report['hours'] ?></td>
                <td><?= $report['price_hours'] ?></td>
                <td><?= $report['state'] ?></td>

                <?php } ?>
            </tbody>

        </table>
    </div>
</div>
<script>
    $("#table").DataTable({
        // "responsive": true,
        "processing": true,
        "order": [],
        "searching": true,
        "select": false,
        "ordering": false,
        "scrollX": true,'paging':false,
        "dom": 'Bfrtip',
        "buttons": [
            'colvis',
            'excel',
            'print'
        ]
    });
</script>