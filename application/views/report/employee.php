<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">


    <div class="kt-portlet">


        <form id="form_report" class="kt-form kt-form--fit kt-form--label-left">
            <div class="kt-portlet__body">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="col-form-label col-lg-12 " for="Employees">Employees</label>

                            <div class="col-md-12"><select multiple name="employees[]" id="Employees"
                                                           class="form-control">

                                    <?php foreach ($employee as $item) { ?>
                                        <option value="<?= $item->e_id ?>"> <?= $item->e_identify_number . " | " . $item->e_first_name . ' ' . $item->e_last_name ?></option>
                                    <?php } ?>

                                </select></div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="col-form-label col-md-12  " for="option">Option</label>
                            <div class="col-md-12"><select name="option" id="option" class="form-control">
                                    <option value="1">All</option>
                                    <option value="2">Expanse</option>
                                    <option value="3">Hours</option>

                                </select></div>

                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label for="from_date"><b>From</b> Date</label>
                            </div>
                            <div class="col-md-12">
                                <input type="text" id="from_date" name="from_date" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label for="to_date"><b>To</b> Date</label>
                            </div>
                            <div class="col-md-12">
                                <input type="text" name="to_date" id="to_date" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-12 text-right">
                        <button class="btn btn-primary" type="submit" id="submit">GET REPORT</button>
                    </div>
                </div>

            </div>

        </form>

    </div>
    <div id="report"></div>

</div>

<script>
    $(document).ready(function () {

        $('#Employees').select2({
            placeholder: "Select  Employees",
        });

        $('#option').select2({
            placeholder: "Select a option",
        });

        $("#from_date").datepicker({
            format: 'yyyy-mm-dd'
        }).datepicker("setDate", new Date());$("#to_date").datepicker({
            format: 'yyyy-mm-dd'
        }).datepicker("setDate", new Date());
    });


    $("#form_report").submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: "<?=base_url('report/get_report_employee')?>",
            data: $("#form_report").serialize(),
            method: "post",
            success: function (data) {
                $("#report").html(data);
            }
        });
    })
</script>