<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">


    <div class="kt-portlet">


        <form id="form_report" class="kt-form kt-form--fit kt-form--label-right">
            <div class="kt-portlet__body">
                <div class="row">
                    <?php
                    foreach ($categories as $category) {
                        $sub_category = $this->db->get_where('sub_category', array('s_id' => 1, 'c_id' => $category->c_id))->result();
                        ?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="category_<?= $category->c_id ?>"><?= $category->c_title ?></label>

                                </div>
                                <div class="col-md-12">
                                    <select class="form-control" name="sub_category[]" multiple
                                            id="category_<?= $category->c_id ?>">
                                        <?php foreach ($sub_category as $item) {
                                            ?>
                                            <option value="<?= $item->sc_id ?>"><?= $item->sc_title ?></option>
                                            <?php
                                        } ?>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <script>
                            $(document).ready(function () {
                                $('#category_<?= $category->c_id ?>').select2();

                            });
                        </script>
                        <?
                    }

                    ?>

                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label for="Type">Export Data about</label>

                            </div>
                            <div class="col-md-12">
                                <select class="form-control" name="type"
                                        id="type">
                                    <option value="all">All</option>
                                    <option value="employee_worktime">Employee (Work Time)</option>
                                    <option value="employee_expanse">Employee (Expanse)</option>
                                    <option value="client_statement_account">Statement Account</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <script>
                        $(document).ready(function () {
                            $('#type').select2();

                        });
                    </script>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group ">
                            <div class="col-md-12"><label for="client">Clients</label>
                            </div>
                            <div class="col-md-12"><select multiple name="client[]" id="client" class="form-control">

                                    <?php foreach ($clients as $client) { ?>
                                        <option value="<?= $client->c_id ?>"> <?= $client->c_name ?></option>
                                    <?php } ?>

                                </select></div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <div class="col-md-12 "><label for="projects">Projects</label></div>
                            <div class="col-md-12"><select multiple name="projects[]" id="projects"
                                                           class="form-control"></select></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label for="from_date"><b>From</b> Date</label>
                            </div>
                            <div class="col-md-12">
                                <input type="text" id="from_date" name="from_date" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label for="to_date"><b>To</b> Date</label>
                            </div>
                            <div class="col-md-12">
                                <input type="text" name="to_date" id="to_date" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-12 text-right">
                        <button class="btn btn-primary" type="submit" id="submit">GET REPORT</button>
                    </div>
                </div>

            </div>

        </form>

    </div>
    <div id="report"></div>

</div>

<script>
    $(document).ready(function () {
        $('#category').select2({
            placeholder: "Select a Category",
        });
        $('#client').select2({
            placeholder: "Select a Client",
        });
        $('#sub_category').select2({
            placeholder: "Select a Sub Category",
        });
        $('#projects').select2({
            placeholder: "Select a Project",
        });
        $("#from_date").datepicker({
            format: 'yyyy-mm-dd'
        }).datepicker("setDate", new Date());$("#to_date").datepicker({
            format: 'yyyy-mm-dd'
        }).datepicker("setDate", new Date());

    });
    $("#category").change(function () {
        $.ajax({
            url: "<?=base_url('sub_category/get_options_array/')?>",
            method: "post",
            data: {
                category: $("#category").val()
            },
            success: function (data) {
                $("#sub_category").empty().append(data);

            }

        })
        ;
    });
    $("#client").change(function () {
        $.ajax({
            url: "<?=base_url('project/get_option_array/')?>",
            method: "post",
            data: {
                clients: $("#client").val()
            },
            success: function (data) {
                $("#projects").empty().append(data);

            }

        })
        ;
    });
    $("#form_report").submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: "<?=base_url('report/get_report_client')?>",
            data: $("#form_report").serialize(),
            method: "post",
            success: function (data) {
                $("#report").html(data);
            }
        });
    })

</script>