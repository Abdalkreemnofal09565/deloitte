<div class="kt-portlet">
    <div class="kt-portlet__body">
        <table class="table table-bordered" id="table">
            <thead>
            <tr>
                <th>Employee</th>
                <th>client</th>
                <th>project</th>

                <th>hours</th>
                <th>cost hours</th>
                <?php
                $hours_types = $this->db->where('s_id', 1)->get("hours_type")->result();

                foreach ($hours_types as $type) {
                    echo '<th class="bg-secondary text-dark">' . $type->ht_title . '</th>';
                }
                ?>
                <th>expanse</th>
                <?php
                $expanse_types = $this->db->where('s_id', 1)->get("cost_type")->result();

                foreach ($expanse_types as $type) {
                    echo '<th class="bg-primary text-light ">' . $type->ct_title . '</th>';
                }
                ?>


            </tr>
            </thead>
            <tbody>
            <?php foreach ($reports as $report) { ?>
                <tr>
                    <td><?= $report['name'] ?></td>
                    <td><?= $report['client'] ?></td>
                    <td><?= $report['project'] ?></td>
                    <td><?= $report['hours'] / 3600 ?></td>
                    <td><?= $report['cost_hours'] ?></td>
                    <?php
                    foreach ($hours_types as $type) {
                        echo '<td class="text-muted">' . $this->Report_model->hours_type($report['project_id'], $type->ht_id, $report['e_id']) . "</td>";
                    }
                    ?>
                    <td><?= $report['expanse'] ?></td>
                    <?php
                    foreach ($expanse_types as $type) {
                        echo '<td class="text-primary">' . $this->Report_model->expanse_type($report['project_id'], $type->ct_id, $report['e_id']) . "</td>";
                    }
                    ?>

                </tr>
            <?php } ?>
            </tbody>

        </table>
    </div>
</div>
<script>
    $("#table").DataTable({
        "paging": false, "processing": true,
        "order": [],
        "searching": true,
        "select": false,
        "ordering": false,
        "scrollY": 300,
        "scrollX": true,
        "dom": 'Bfrtip',
        "buttons": [
            'colvis',
            'excel',
            'print'
        ]
    });
</script>