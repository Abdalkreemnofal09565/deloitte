<div class="kt-portlet">
    <div class="kt-portlet__body">
        <table class="table table-bordered" id="table">
            <thead>
            <tr>
                <th>Employee</th>
                <th>client</th>
                <th>project</th>

                <th>Hours Type</th>
                <th>Hours</th>
                <th>Start Time</th>
                <th>End Time</th>
                <th>Level Employee</th>

                <th>Date</th>



            </tr>
            </thead>
            <tbody>
            <?php foreach ($reports as $report) { ?>
                <tr>
                    <td><?= $report->employee?></td>
                    <td><?= $report->client ?></td>
                    <td><?= $report->project ?></td>

                    <td><?= $report->hoursType?></td>
                    <td><?= $report->ph_hours?></td>
                    <td><?= $report->start_time?></td>
                    <td><?= $report->end_time?></td>
                    <td><?= $report->level_title ?></td>
                    <td><?= $report->ph_date ?></td>
                </tr>
            <?php } ?>
            </tbody>

        </table>
    </div>
</div>
<script>
    $("#table").DataTable({
        "paging": false,
        "processing": true,
        "order": [],
        "searching": true,
        "select": false,
        "ordering": false,
        "scrollY": 300,
        "scrollX": true,
        "dom": 'Bfrtip',
        "buttons": [
            'colvis',
            'excel',
            'print'
        ]
    });
</script>