<div class="kt-portlet">
    <div class="kt-portlet__body">
        <table style="overflow-x:auto;" class="table table-bordered" id="table">
            <thead>
            <tr>
                <th>client</th>
                <th>project</th>
                <?php foreach ($categories as $item) {
                    ?>
                    <th><?= $item->c_title ?></th>

                    <?php
                } ?>
                <th>balance</th>
                <?php
                $this->load->model('Report_model');
                $where_payment = array();
                $where_payment ['s_id'] = 1;

                $pyament_type = $this->db->where($where_payment)->get('payment_type')->result();
                foreach ($pyament_type as $type) {
                    echo '<th class="bg-dark text-light">' . $type->pt_title . '</th>';
                }
                ?>
                <th>hours</th>
                <th>cost hours</th>
                <?php

                $hours_types = $this->db->where('s_id', 1)->get("hours_type")->result();

                foreach ($hours_types as $type) {
                    echo '<th class="bg-secondary text-dark">' . $type->ht_title . '</th>';
                }
                ?>
                <th>expanse</th>
                <?php
                $expanse_types = $this->db->where('s_id', 1)->get("cost_type")->result();

                foreach ($expanse_types as $type) {
                    echo '<th class="bg-primary text-light ">' . $type->ct_title . '</th>';
                }
                ?>
                <th>budget hours</th>
                <th>budget price</th>
                <th>count employee</th>
                <th>project manger</th>

            </tr>
            </thead>
            <tbody>
            <?php foreach ($reports as $report) { ?>
                <tr>
                    <td><?= $report['client'] ?></td>
                    <td><?= $report['project'] ?></td>
                    <?php foreach ($categories as $item) {
                        ?>
                        <td><?= $report[$item->c_title] ?></td>

                        <?php
                    } ?>


                    <td><?= $report['balance_i'] - $report['balance_d'] ?></td>

                    <?php

                    foreach ($pyament_type as $type1) {
//                        print_r($type1);
                        echo '<th>' . $this->Report_model->payment_type($report['project_id'], $type1->pt_id) . '</th>';
                    }
                    ?>
                    <td><?= $report['hours'] / 3600 ?></td>
                    <td><?= $report['cost_hours'] ?></td>
                    <?php
                    foreach ($hours_types as $type) {
                        echo '<td class="text-muted">' . $this->Report_model->hours_type($report['project_id'], $type->ht_id) . "</td>";
                    }
                    ?>
                    <td><?= $report['expanse'] ?></td>
                    <?php
                    foreach ($expanse_types as $type) {
                        echo '<td class="text-primary">' . $this->Report_model->expanse_type($report['project_id'], $type->ct_id) . "</td>";
                    }
                    ?>
                    <td><?= $report['budget_hours'] ?></td>
                    <td><?= $report['budget_price'] ?></td>
                    <td><?= @$report['count_employee'] ?></td>
                    <td><?= $report['project_manger'] ?></td>
                </tr>
            <?php } ?>
            </tbody>

        </table>
    </div>
</div>
<script>
    $("#table").DataTable({
        // "responsive": true,
        "processing": true,
        "order": [],
        "searching": true,
        "select": false,
        "ordering": false,
        "scrollX": true,
        'paging':false,
        "dom": 'Bfrtip',
        "buttons": [
            'colvis',
            'excel',
            'print'
        ]
    });
</script>