<div class="kt-portlet">
    <div class="kt-portlet__body">
        <table style="overflow-x:auto;" class="table table-bordered" id="table">
            <thead>
            <tr>
                <th>client</th>
                <th>project</th>
                <th>Type Expanse</th>
                <th>Expanse</th>
                <th>Date</th>
                <th>Date Added</th>
                <th>Employee</th>
                <th>Expanse Note</th>
                <th>State</th>


            </tr>
            </thead>
            <tbody>
            <?php foreach ($reports as $report) { ?>
                <tr>
                    <td><?= $report['client'] ?></td>
                    <td><?= $report['project'] ?></td>
                    <td><?= $report['type'] ?></td>
                    <td><?= $report['expanse'] ?></td>
                    <td><?= $report['date'] ?></td>
                    <td><?= $report['date_added'] ?></td>
                    <td><?= $report['employee'] ?></td>
                    <td><?= $report['expnase_note'] ?></td>
                    <td><?= $report['state'] ?></td>

            <?php } ?>
            </tbody>

        </table>
    </div>
</div>
<script>
    $("#table").DataTable({
        // "responsive": true,
        "processing": true,
        "order": [],
        "searching": true,
        "select": false,
        "ordering": false,
        "scrollX": true,'paging':false,
        "dom": 'Bfrtip',
        "buttons": [
            'colvis',
            'excel',
            'print'
        ]
    });
</script>