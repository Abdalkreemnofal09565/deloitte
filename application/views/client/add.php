<div class="row mt-5">
    <div class="col-md-10 offset-1">

        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        New Client
                    </h3>
                </div>
            </div>

            <!--begin::Form-->
            <form class="kt-form" id="form_category">
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="client_name">Client Name</label>
                                <input type="text" name="client_name" id="client_name" class="form-control"
                                       placeholder="Client Name">
                                <b id="error_client_name" class="text-danger"></b>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="client_name">Email </label>
                                <input type="text" name="client_email" id="client_email" class="form-control"
                                       placeholder="Client Email">
                                <b id="error_client_email" class="text-danger"></b>
                            </div>
                        </div>
                        <?php
                        foreach ($additional_data as $item) {
                            ?>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="<?= $item['category'] ?>"><?= $item['category'] ?></label>

                                    <select name="sub_category[]" id="<?= $item['category'] ?>"
                                            class="form-control select2" required>
                                        <?php foreach ($item['sub_category'] as $item2) {
                                            ?>
                                            <option <?php if ($item2->sc_id == $item['active']) echo 'selected'; ?>
                                                    value="<?= $item2->sc_id ?>"> <?= $item2->sc_title ?></option>
                                            <?php
                                        } ?>
                                        <option selected value="">empty</option>
                                    </select>

                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="note">Note</label>
                                <textarea rows="3" onresize="false" class="form-control" name="client_note"
                                          placeholder="Note"></textarea>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-primary" id="submit">Submit</button>
                        <a href="<?= base_url('client') ?>" class="btn btn-secondary">Cancel</a>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

    </div>

</div>
<script>
    $(document).ready(function () {
        $('.select2').select2({
            width: '100%'
        });
    });
    $("#error").hide();
    $("#form_category").submit(function (event) {
        event.preventDefault();
        $("#submit").attr("disabled", "disabled").addClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").html("in processing");
        $.ajax({
            url: "<?=base_url('client/create')?>",
            method: "post",
            data: $("#form_category").serialize(),
            success: function (data) {
                var response = JSON.parse(data);
                if (response.result == "1") {
                    $("#submit").html("Done").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").addClass("btn-outline-success").removeClass("btn-primary");
                    $("#error").hide();
                    setTimeout(function () {
                        window.location.href = "<?=base_url('client')?>";
                    }, 2000)
                } else {
                    $("#submit").html("Submit").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").removeAttr("disabled");
                    $("#error_client_name").html(response.client_name).show();
                    $("#error_client_email").html(response.client_email).show();


                }
            }
        })
    });
</script>
