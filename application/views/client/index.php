<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon-suitcase"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    Clients
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <?php
                        $ex1 = check_permission(93, false);
                        $ex2 = check_permission(94, false);
                        if ($ex1 || $ex2) {
                            ?>
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="la la-download"></i> Export
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <ul class="kt-nav">
                                        <li class="kt-nav__section kt-nav__section--first">
                                            <span class="kt-nav__section-text">Choose an option</span>
                                        </li>
                                        <?php if ($ex1) { ?>
                                            <li class="kt-nav__item">
                                            <a href="<?= base_url('client/export/client') ?>" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon-suitcase"></i>
                                                <span class="kt-nav__link-text">Clients</span>
                                            </a>
                                            </li><?php }
                                        if ($ex2) {
                                            ?>
                                            <li class="kt-nav__item">
                                                <a href="<?= base_url('client/export/payment') ?>" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon la la-cubes"></i>
                                                    <span class="kt-nav__link-text">with Project</span>
                                                </a>
                                            </li>
                                        <?php } ?>

                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                        &nbsp; <?php if (check_permission(34, false)) { ?>
                            <a href="<?= base_url('client/add') ?>" class="btn btn-brand btn-elevate btn-icon-sm">
                                <i class="la la-plus"></i>
                                New Record
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Note</th>
                    <th><b>cnt.</b>Project</th>
                    <?php
                    foreach ($category as $item) {
                        ?>
                        <th><?= $item->c_title ?></th>
                        <?php
                    }
                    ?>
                    <th>State</th>
                    <th>Option</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function () {

        table = $('#table').DataTable({

            "responsive": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "searching": true,
            "select": false,
            "ordering": false,
            "ajax": {
                "url": "<?=base_url('client/datatable')?>",
                "type": "POST"
            },

            "columns": [
                {
                    "data": "num"
                },

                {
                    "data": "name"
                },

                {
                    "data": "email"
                },
                {
                    "data": "note"
                }, {
                    "data": "projects"
                }
                <?php
                foreach ($category as $item){
                ?>
                ,

                {
                    "data": "<?=(string)$item->c_title?>"
                }                    <?php
                }?>

                ,
                {
                    "data": "state"
                }
                ,
                {
                    "data": "option"
                }

            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });

    });
    var name_table = "client";

</script>