<div class="row mt-5">
    <div class="col-md-8 offset-2">

        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Add Level
                    </h3>
                </div>
            </div>

            <!--begin::Form-->
            <form class="kt-form" id="form_category">
                <input type="hidden" name="id" value="<?=$level->l_id?>">
                <div class="kt-portlet__body">
                    <div class="form-group">
                        <label for="Category">Level name</label>
                        <input type="text" name="level" id="Level" class="form-control" value="<?=$level->l_title?>"

                               placeholder="Level name">
                        <b id="error_level" class="text-danger"></b>
                    </div>
                    <div class="form-group">
                        <label for="Category">Price</label>
                        <input type="number" name="price" id="Price" class="form-control"  value="<?=$level->l_price?>"
                               placeholder=" Price">
                        <b id="error_price" class="text-danger"></b>
                    </div>

                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-primary" id="submit">Submit</button>
                        <a href="<?= base_url('level') ?>" class="btn btn-secondary">Cancel</a>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

    </div>

</div>
<script>
    $("#error").hide();
    $("#form_category").submit(function (event) {
        event.preventDefault();
        $("#submit").attr("disabled", "disabled").addClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").html("in processing");
        $.ajax({
                url: "<?=base_url('level/do_edit')?>",
            method: "post",
            data: $("#form_category").serialize(),
            success: function (data) {
                var response = JSON.parse(data);
                if (response.result == "1") {
                    $("#submit").html("Done").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").addClass("btn-outline-success").removeClass("btn-primary");

                    setTimeout(function () {
                        window.location.href = "<?=base_url('level')?>";
                    }, 2000)
                } else {
                    $("#submit").html("Submit").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").removeAttr("disabled");
                    $("#error_level").html(response.level).show();
                    $("#error_price").html(response.price).show();

                }
            }
        })
    });
</script>
