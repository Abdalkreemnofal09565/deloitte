<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

    <!--Begin::App-->
    <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">
        <div class="kt-grid__item kt-grid__item--fluid kt-app__content">
            <div class="row">
                <div class="col-xl-12">
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">Personal Information</h3>
                            </div>

                        </div>
                        <form class="kt-form kt-form--label-right">
                            <div class="kt-portlet__body">
                                <div class="kt-section kt-section--first">
                                    <div class="kt-section__body">


                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">First Name</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control" type="text"
                                                       value="<?= $profile->e_first_name ?>" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Last Name</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control" type="text"
                                                       value="<?= $profile->e_last_name ?>" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Identify Number</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control" type="text"
                                                       value="<?= $profile->e_identify_number ?>" disabled>
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Email Address</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i
                                                                    class="la la-at"></i></span></div>
                                                    <input type="text" class="form-control"
                                                           value="<?= $profile->e_email ?>" disabled placeholder="Email"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">
        <div class="kt-grid__item kt-grid__item--fluid kt-app__content">
            <div class="row">
                <div class="col-xl-12">
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">Change Password</h3>
                            </div>

                        </div>
                        <form class="kt-form kt-form--label-right" id="change_password">
                            <div class="kt-portlet__body">
                                <div class="kt-section kt-section--first">
                                    <div class="kt-section__body">


                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Old password</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control" name="password" type="password" placeholder="Old password">
                                                <b class="text-danger" id="error_password"></b>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">New password</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control" name="new_password" type="password" placeholder="New password">
                                                <b class="text-danger" id="error_new_password"></b>
                                            </div>

                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Password Confirmationd</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control" name="conf_new_password" type="password" placeholder="Password Confirmation">
                                                <b class="text-danger" id="error_conf_new_password"></b>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-6 offset-3 ">
                                                <button id="submit" class="btn btn-primary w-100">Change Password</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!--End:: App Content-->
    </div>

    <!--End::App-->
</div>
<script>
    $("#change_password").submit(function (event) {
        event.preventDefault();
        $("#submit").attr("disabled", "disabled").addClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").html("in processing");
        $.ajax({
            url: "<?=base_url('change_password')?>",
            method: "post",
            data: $("#change_password").serialize(),
            success: function (data) {
                var response = JSON.parse(data);
                if (response.result == "1") {
                    $("#submit").html("Done").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").addClass("btn-outline-success").removeClass("btn-primary");
                    $("#error").hide();
                    setTimeout(function () {
                        window.location.href = "<?=base_url('profile')?>";
                    }, 2000)
                } else {
                    $("#submit").html("Submit").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").removeAttr("disabled");
                    $("#error_password").html(response.password);
                    $("#error_new_password").html(response.new_password);
                    $("#error_conf_new_password").html(response.conf_new_password);


                }
            }
        })
    });
</script>