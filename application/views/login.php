<?php
//echo md5("F3THe6OtGw_deloitte");
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <base href="<?= base_url() ?>">
    <meta charset="utf-8"/>
    <title>Deloitte | Login</title>
    <link rel="manifest" href="<?= base_url('manifest.json') ?>">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <link href="<?= base_url('template') ?>/css/demo1/pages/general/login/login-4.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/tether/dist/css/tether.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/bootstrap-daterangepicker/daterangepicker.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/bootstrap-select/dist/css/bootstrap-select.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/select2/dist/css/select2.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/nouislider/distribute/nouislider.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/owl.carousel/dist/assets/owl.theme.default.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/dropzone/dist/dropzone.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/summernote/dist/summernote.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/animate.css/animate.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/toastr/build/toastr.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/morris.js/morris.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('template') ?>/vendors/general/@fortawesome/fontawesome-free/css/all.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/css/demo1/style.bundle.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('template') ?>/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="<?= base_url('template') ?>/media/logos/favicon.ico"/>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v4 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
             style="background-image: url(<?= base_url('template') ?>/media/bg/bg-2.jpg);">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__logo">
                        <a href="#">
                            <img src="<?= base_url('icon.png') ?>">
                        </a>
                    </div>
                    <div class="kt-login__signin">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title"></h3>
                        </div>
                        <form class="kt-form" id="login_form" action="<?= base_url('login/submit') ?>"
                              autocomplete="off">

                            <div class="input-group">
                                <input class="form-control"   autocomplete="ammar" type="text" placeholder="Email" name="email" autofocus
                                       value=""
                                >
                            </div>
                            <b class="text-danger error-message" id="email_error" style="margin-left: 10px"></b>

                            <div class="input-group">
                                <input class="form-control" autocomplete="off" type="password" placeholder="Password" name="password"
                                       value="">
                            </div>
                            <b class="text-danger error-message" id="password_error" style="margin-left: 10px"></b>

                            <div class="row kt-login__extra">

                                <div class="col kt-align-right">
                                    <a href="javascript:;" id="kt_login_forgot" class="kt-login__link">Forget Password
                                        ?</a>
                                </div>
                            </div>
                            <div class="kt-login__actions">
                                <button id="kt_login_signin_submit"
                                        class="btn btn-brand btn-pill kt-login__btn-primary w-100">Sign In
                                </button>
                            </div>
                        </form>
                    </div>

                        <div class="kt-login__forgot" id="kt_login__forgot_form">
                            <div class="kt-login__head">
                                <h3 class="kt-login__title">Forgotten Password ?</h3>
                                <div class="kt-login__desc">Enter your email to reset your password:</div>
                            </div>
                            <form  class="kt-form" action="<?=base_url('login/forget')?>"  id="login_forgot_form"  autocomplete="off">
                                <div class="input-group">
                                    <input class="form-control"  type="text" placeholder="Email" name="email"
                                           id="kt_email"
                                           autocomplete="off">
                                </div>
                                <div class="kt-login__actions">
                                    <button id="kt_login_forgot_submit" type="submit"
                                            class="btn btn-brand btn-pill kt-login__btn-primary">Submit
                                    </button>&nbsp;&nbsp;
                                    <button id="kt_login_forgot_cancel"
                                            class="btn btn-secondary btn-pill kt-login__btn-secondary">Cancel
                                    </button>
                                </div>
                            </form>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };

</script>

<!-- end::Global Config -->

<!--begin:: Global Mandatory Vendors -->
<script src="<?= base_url('template') ?>/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap/dist/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/tooltip.js/dist/umd/tooltip.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>

<!--end:: Global Mandatory Vendors -->

<!--begin:: Global Optional Vendors -->
<script src="<?= base_url('template') ?>/vendors/general/jquery-form/dist/jquery.form.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/block-ui/jquery.blockUI.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/js/vendors/bootstrap-datepicker.init.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/js/vendors/bootstrap-timepicker.init.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-daterangepicker/daterangepicker.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-select/dist/js/bootstrap-select.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/js/vendors/bootstrap-switch.init.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/select2/dist/js/select2.full.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/ion-rangeslider/js/ion.rangeSlider.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/typeahead.js/dist/typeahead.bundle.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/handlebars/dist/handlebars.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/inputmask/dist/jquery.inputmask.bundle.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/nouislider/distribute/nouislider.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/owl.carousel/dist/owl.carousel.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/autosize/dist/autosize.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/clipboard/dist/clipboard.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/dropzone/dist/dropzone.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/summernote/dist/summernote.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/markdown/lib/markdown.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-markdown/js/bootstrap-markdown.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/js/vendors/bootstrap-markdown.init.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/bootstrap-notify/bootstrap-notify.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/js/vendors/bootstrap-notify.init.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/jquery-validation/dist/jquery.validate.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/jquery-validation/dist/additional-methods.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/js/vendors/jquery-validation.init.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/toastr/build/toastr.min.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/raphael/raphael.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/morris.js/morris.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/vendors/jquery-idletimer/idle-timer.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/waypoints/lib/jquery.waypoints.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/counterup/jquery.counterup.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/es6-promise-polyfill/promise.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/sweetalert2/dist/sweetalert2.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/custom/js/vendors/sweetalert2.init.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/jquery.repeater/src/lib.js" type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/jquery.repeater/src/jquery.input.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/jquery.repeater/src/repeater.js"
        type="text/javascript"></script>
<script src="<?= base_url('template') ?>/vendors/general/dompurify/dist/purify.js" type="text/javascript"></script>

<!--end:: Global Optional Vendors -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="<?= base_url('template') ?>/js/demo1/scripts.bundle.js" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<!--begin::Page Scripts(used by this page) -->
<!--<script src="-->
<? //= base_url('template') ?><!--/js/demo1/pages/login/login-general.js" type="text/javascript"></script>-->

<script>
    var showErrorMsg = function (form, type, msg) {
        var alert = $('<div class="kt-alert kt-alert--outline alert alert-' + type + ' alert-dismissible" role="alert" disabled="disabled">\
			\
			<span></span>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        KTUtil.animateClass(alert[0], 'fadeIn animated');
        alert.find('span').html(msg);
    }
    $('#login_form').submit(function (e) {
        e.preventDefault();
        var btn = $("#kt_login_signin_submit_forget");
        var form = $('#login_form');

        btn.addClass("btn btn-primary kt-spinner kt-spinner--center kt-spinner--md kt-spinner--light").html('').attr('disabled', 'disabled');

        if (!form.valid()) {
            btn.html('Sign In');
            btn.removeClass('kt-spinner kt-spinner--center kt-spinner--md kt-spinner--light').attr('disabled', false);

            return;
        }

        $.ajax({
            url: form.attr('action'),
            data: form.serialize(),
            method: "post",
            success: function (response, status, xhr, $form) {
                btn.removeClass('kt-spinner kt-spinner--center kt-spinner--md kt-spinner--light').attr('disabled', false);
                var data = JSON.parse(response);
                if (data.result == "1") {

                    btn.html("success login").attr('disabled', 'disabled');
                    showErrorMsg(form, 'success', 'correct login information.');
                    $(".error-message").hide();
                    setTimeout(function () {
                        window.location.href = "<?=base_url('timesheet')?>";
                    }, 3000)
                } else {
                    btn.html('Sign In');
                    btn.removeAttr('disabled');

                    $(".error-message").hide();
                    $("#email_error").html(data.email).show();
                    $("#password_error").html(data.password).show();
                    $("#password_error").html(data.message).show();

                }
            }

        });

    });
    $("#kt_login_forgot").click(function () {
        $("#login_form").hide();
        $("#kt_login__forgot_form").show();
        $("#login_form :input").prop("disabled", true);
        $("#kt_login__forgot_form :input").prop("disabled", false);
    });
    $("#kt_login_forgot_cancel").click(function () {
        $("#login_form").show();
        $("#kt_login__forgot_form").hide();
        $("#kt_login__forgot_form :input").prop("disabled", true);
        $("#login_form :input").prop("disabled", false);

    });
    $('#login_forgot_form').submit(function (e) {
        e.preventDefault();
        var btn = $("#kt_login_forgot_submit");
        var form = $('#login_forgot_form');

        btn.addClass("btn btn-primary kt-spinner kt-spinner--center kt-spinner--md kt-spinner--light").html('').attr('disabled', 'disabled');
        form.validate({
            rules: {
                username: {
                    required: true
                }
            }
        });

        if (!form.valid()) {
            btn.html('Sign In');
            btn.removeClass('kt-spinner kt-spinner--center kt-spinner--md kt-spinner--light').attr('disabled', false);

            return;
        }

        $.ajax({
            url: form.attr('action'),
            data: form.serialize(),
            method: "post",
            success: function (response, status, xhr, $form) {
                btn.removeClass('kt-spinner kt-spinner--center kt-spinner--md kt-spinner--light').attr('disabled', false).html("Done");
                var data = JSON.parse(response);
                showErrorMsg(form, 'success', 'Check your email');

            }

        });

    });
</script>
<!--end::Page Scripts -->
</body>

<!-- end::Body -->
</html>