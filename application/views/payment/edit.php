<div class="row mt-5">
    <div class="col-md-8 offset-2">

        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Edit Statment_account
                    </h3>
                </div>
            </div>

            <!--begin::Form-->
            <form class="kt-form" id="form_category">
                <input type="hidden" name="id" value="<?= $id ?>">
                <div class="kt-portlet__body">
                    <div class="form-group">
                        <label for="Type">Type</label>
                        <select name="Type" class="form-control" id="Type">
                            <? foreach ($types as $type) {
                                ?>
                                <option value="<?= $type->pt_id ?>" <?php if ($type->pt_id == $payment->pt_id) echo 'selected'; ?> ><?= $type->pt_title ?></option>
                                <?php
                            } ?>
                        </select>
                        <b id="error_type" class="text-danger"></b>
                    </div>
                    <div class="form-group">
                        <label for="payment">payment </label>
                        <input type="number" name="payment" id="payment" class="form-control"
                               value="<?= $payment->pay_payment ?>"

                               placeholder="payment">
                        <b id="error_payment" class="text-danger"></b>
                    </div>

                    <div class="form-group">
                        <label for="payment">Date </label>
                        <input type="text" name="date" id="date" class="form-control"

                               placeholder="date">

                    </div>
                    <div class="form-group">
                        <label for="payment">reference </label>
                        <input type="text" name="reference" id="reference" class="form-control"
                               value="<?= $payment->reference ?>"
                               placeholder="reference">
                    </div>


                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-primary" id="submit">Submit</button>
                        <a href="<?= base_url('payment/' . $id) ?>" class="btn btn-secondary">Cancel</a>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

    </div>

</div>
<script>
    $("#error").hide();
   $(document).ready(function () {
       $('#Type').select2({
           placeholder: "Select a Type",
           width: '100%'
       });
       $("#date").datepicker({
           format: 'yyyy-mm-dd'
       }).datepicker("setDate", "<?=$payment->p_date?>").datepicker()   ;
   });
    $("#form_category").submit(function (event) {
        event.preventDefault();
        $("#submit").attr("disabled", "disabled").addClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").html("in processing");
        $.ajax({
            url: "<?=base_url('Statment_account/do_edit')?>",
            method: "post",
            data: $("#form_category").serialize(),
            success: function (data) {
                var response = JSON.parse(data);
                if (response.result == "1") {
                    $("#submit").html("Done").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").addClass("btn-outline-success").removeClass("btn-primary");

                    setTimeout(function () {
                        window.location.href = "<?=base_url('Statment_account/' . $id)?>";
                    }, 2000)
                } else {
                    $("#submit").html("Submit").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").removeAttr("disabled");
                    $("#error_payment_title").html(response.title).show();
                    $("#error_payment").html(response.payment).show();
                    $("#error_type").html(response.type).show();

                }
            }
        })
    });
</script>
