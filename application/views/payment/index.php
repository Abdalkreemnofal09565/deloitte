<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon-coins"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    Statment account
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">


                        <a href="<?=base_url('Statment_account/add/'.$id)?>" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            New Record
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Reference</th>
                    <th>Type</th>
                    <th>Debit</th>
                    <th>Credit</th>
                    <th>Balance</th>
                    <th>Date Added</th>
                    <th>State</th>
                    <th>Option</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function () {

        table = $('#table').DataTable({

            "responsive": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "searching": true,
            "select": false,
            "ordering":false,
            "paging":false,
            "ajax": {
                "url": "<?=base_url('Statment_account/datatable/'.$id)?>",
                "type": "POST"
            },

            "columns": [
                {
                    "data": "num"
                },
                {
                    "data": "date"
                },   {
                    "data": "reference"
                },
                {
                    "data": "type"
                }, {
                    "data": "Debit"
                },{
                    "data": "Credit"
                },{
                    "data": "Balance"
                },
                {
                    "data": "date_register"
                },
                {
                    "data": "state"
                }
                ,
                {
                    "data": "option"
                }

            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }],
            "createdRow": function( row, data, dataIndex){
                if( data.s !=  `1`){
                    $(row).addClass('bg-warning ');
                }
            }
        });

    });

    var name_table = "Statment_account";

</script>