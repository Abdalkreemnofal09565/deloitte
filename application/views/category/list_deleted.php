<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon-layers"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    Categories Deleted
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">

                        &nbsp;
                        <?php if(check_permission(87,false)){?>
                        <a href="<?=base_url('category/export/deleted')?>" class="btn btn-outline-primary btn-elevate btn-icon-sm" target="_blank">
                            <i class="la la-download"></i>
                            Export excel
                        </a>
                        <?}?>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Date added</th>

                    <th><b>cnt.</b> Sub category</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function () {

        table = $('#table').DataTable({

            "responsive": true,
            "processing": true,
            "serverSide": true,
            "searching": true,
            "select": false,
            "ordering":false,
            "ajax": {
                "url": "<?=base_url('category/datatable/deleted')?>",
                "type": "POST"
            },

            "columns": [
                {
                    "data": "num"
                },
                {
                    "data": "title"
                },
                {
                    "data": "date_register"
                }
                ,
                {
                    "data": "count_sub_category"
                }


            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });

    });

    var name_table = "category";

</script>