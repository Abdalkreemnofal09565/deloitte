<div class="row mt-5">
    <div class="col-md-8 offset-2">

        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Edit Category
                    </h3>
                </div>
            </div>

            <!--begin::Form-->
            <form class="kt-form" id="form_category">
                <div class="kt-portlet__body">
                    <div class="form-group">
                        <label for="Category">Category name</label>
                        <input type="text" name="Category" id="Category" class="form-control"
                              value="<?=$category->c_title;?>"
                               placeholder="Category name">
                        <b id="error" class="text-danger"></b>
                    </div>
                    <div class="form-group">
                        <label>classification</label>
                        <div class="kt-checkbox-inline">
                            <label class="kt-checkbox">
                                <input type="checkbox" <?php if($category->c_client == 1) echo'checked'; ?>  name="client"> Client
                                <span></span>
                            </label>
                            <label class="kt-checkbox">
                                <input type="checkbox" <?php if($category->c_project == 1) echo'checked'; ?>  name="project"> Project
                                <span></span>
                            </label>
                            <label class="kt-checkbox">
                                <input type="checkbox" <?php if($category->c_employee == 1) echo'checked'; ?>  name="employee"> Employee
                                <span></span>
                            </label>
                        </div>

                    </div>
                    <input type="hidden" name="category_id" value="<?=$category->c_id?>">

                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-primary" id="submit">Submit</button>
                        <a href="<?= base_url('Category') ?>" class="btn btn-secondary">Cancel</a>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

    </div>

</div>
<script>
    $("#error").hide();
    $("#form_category").submit(function (event) {
        event.preventDefault();
        $("#submit").attr("disabled", "disabled").addClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").html("in processing");
        $.ajax({
            url: "<?=base_url('category/do_edit')?>",
            method: "post",
            data: $("#form_category").serialize(),
            success: function (data) {
                var response = JSON.parse(data);
                if (response.result == "1") {
                    $("#submit").html("Done").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").addClass("btn-outline-success").removeClass("btn-primary");
                    $("#error").hide();
                    setTimeout(function () {
                        window.location.href = "<?=base_url('category')?>";
                    },2000)
                } else {
                    $("#submit").html("Submit").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").removeAttr("disabled");
                    $("#error").html(response.category).show();

                }
            }
        })
    });
</script>
