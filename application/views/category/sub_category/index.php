<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon-layers"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                   <b>Sub</b> - Categories
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">

                        &nbsp;
                        <a href="<?=base_url('sub_category/add/'.$id)?>" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            New Record
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Date added</th>
                    <th>State</th>
                    <th> Category </th>

                    <th>Option</th>
                </tr>
                </thead>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function () {

        table = $('#table').DataTable({

            "responsive": true,
            "processing": true,
            "serverSide": true,
            "searching": true,
            "select": false,
            "ordering":false,
            "ajax": {
                "url": "<?=base_url('sub_category/datatable/'.$id)?>",
                "type": "POST"
            },

            "columns": [
                {
                    "data": "num"
                },
                {
                    "data": "title"
                },
                {
                    "data": "date_register"
                },
                {
                    "data": "state"
                }
                ,
                {
                    "data": "category"
                }
                ,
                {
                    "data": "option"
                }

            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });

    });
    var name_table = "Sub_category";


</script>