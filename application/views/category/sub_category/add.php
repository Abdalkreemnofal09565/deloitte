<div class="row">
    <div class="col-md-8 offset-2">

        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Add Sub Category
                    </h3>
                </div>
            </div>

            <!--begin::Form-->
            <form class="kt-form" id="form_category">
                <div class="kt-portlet__body">
                    <div class="form-group">
                        <label for="Category">Category name</label>
                        <input type="text" class="form-control"
                                disabled
                               value="<?=$title?>"
                               placeholder="Category name">
                        <b id="error_category" class="text-danger error"></b>
                    </div>
                    <input type="hidden" name="category_id" value="<?=$id?>">

                    <div class="form-group">
                        <label for="Category">Sub Category name</label>
                        <input type="text" name="SubCategory" id="SubCategory" class="form-control"
                               placeholder="Sub Category name">
                        <b id="error_sub_category" class="text-danger error"></b>
                    </div>

                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-primary" id="submit">Submit</button>
                        <a href="<?= base_url('Sub_category/'.$id) ?>" class="btn btn-secondary">Cancel</a>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

    </div>

</div>
<script>
    $(".error").hide();
    $("#form_category").submit(function (event) {
        event.preventDefault();
        $("#submit").attr("disabled", "disabled").addClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").html("in processing");
        $.ajax({
            url: "<?=base_url('sub_category/create')?>",
            method: "post",
            data: $("#form_category").serialize(),
            success: function (data) {
                var response = JSON.parse(data);
                if (response.result == "1") {
                    $("#submit").html("Done").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").addClass("btn-outline-success").removeClass("btn-primary");
                    $(".error").hide();
                    setTimeout(function () {
                        window.location.href = "<?=base_url('Sub_category/'.$id)?>";
                    },2000)
                } else {
                    $("#submit").html("Submit").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").removeAttr("disabled");
                    $("#error_sub_category").html(response.sub_category).show();
                    $("#error_category").html(response.category).show();

                }
            }
        })
    });
</script>
