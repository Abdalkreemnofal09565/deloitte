<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon-suitcase"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    My Project
                </h3>
            </div>

        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Client</th>
                    <th>Project</th>
                    <th>Date added</th>
                    <th>Date start</th>
                    <th>Date end</th>

                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function () {

        table = $('#table').DataTable({

            "responsive": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "searching": true,
            "select": false,
            "ordering":false,
            "ajax": {
                "url": "<?=base_url('myproject/datatable')?>",
                "type": "POST"
            },

            "columns": [
                {
                    "data": "num"
                },

                {
                    "data": "client"
                },

                {
                    "data": "project"
                },
                {
                    "data": "date_added"
                }, {
                    "data": "date_start"
                },  {
                    "data": "date_end"
                }

            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });

    });
    var name_table = "client";

</script>