<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

    <div class="row">

        <div class="col-md-12">


            <!--begin::Portlet-->
            <div class="kt-portlet kt-portlet--tabs">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-toolbar">
                        <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-primary nav-tabs-line-2x nav-tabs-line-right"
                            role="tablist">

                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab"
                                   href="#kt_portlet_base_demo_2_2_tab_content"
                                   role="tab">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="24px" height="24px" viewBox="0 0 24 24" version="1.1"
                                         class="kt-svg-icon">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect id="bound" x="0" y="0" width="24" height="24"/>
                                            <path d="M16.0322024,5.68722152 L5.75790403,15.945742 C5.12139076,16.5812778 5.12059836,17.6124773 5.75613416,18.2489906 C5.75642891,18.2492858 5.75672377,18.2495809 5.75701875,18.2498759 L5.75701875,18.2498759 C6.39304347,18.8859006 7.42424328,18.8859006 8.060268,18.2498759 C8.06056298,18.2495809 8.06085784,18.2492858 8.0611526,18.2489906 L18.3196731,7.9746922 C18.9505124,7.34288268 18.9501191,6.31942463 18.3187946,5.68810005 L18.3187946,5.68810005 C17.68747,5.05677547 16.6640119,5.05638225 16.0322024,5.68722152 Z"
                                                  id="Stroke-1" fill="#000000" fill-rule="nonzero"/>
                                            <path d="M9.85714286,6.92857143 C9.85714286,8.54730513 8.5469533,9.85714286 6.93006028,9.85714286 C5.31316726,9.85714286 4,8.54730513 4,6.92857143 C4,5.30983773 5.31316726,4 6.93006028,4 C8.5469533,4 9.85714286,5.30983773 9.85714286,6.92857143 Z M20,17.0714286 C20,18.6901623 18.6898104,20 17.0729174,20 C15.4560244,20 14.1428571,18.6901623 14.1428571,17.0714286 C14.1428571,15.4497247 15.4560244,14.1428571 17.0729174,14.1428571 C18.6898104,14.1428571 20,15.4497247 20,17.0714286 Z"
                                                  id="Combined-Shape" fill="#000000" opacity="0.3"/>
                                        </g>
                                    </svg>
                                    Payments
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_2_3_tab_content"
                                   role="tab">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="24px" height="24px" viewBox="0 0 24 24" version="1.1"
                                         class="kt-svg-icon">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect id="bound" x="0" y="0" width="24" height="24"/>
                                            <path d="M10.9630156,7.5 L11.0475062,7.5 C11.3043819,7.5 11.5194647,7.69464724 11.5450248,7.95024814 L12,12.5 L15.2480695,14.3560397 C15.403857,14.4450611 15.5,14.6107328 15.5,14.7901613 L15.5,15 C15.5,15.2109164 15.3290185,15.3818979 15.1181021,15.3818979 C15.0841582,15.3818979 15.0503659,15.3773725 15.0176181,15.3684413 L10.3986612,14.1087258 C10.1672824,14.0456225 10.0132986,13.8271186 10.0316926,13.5879956 L10.4644883,7.96165175 C10.4845267,7.70115317 10.7017474,7.5 10.9630156,7.5 Z"
                                                  id="Path-107" fill="#000000"/>
                                            <path d="M7.38979581,2.8349582 C8.65216735,2.29743306 10.0413491,2 11.5,2 C17.2989899,2 22,6.70101013 22,12.5 C22,18.2989899 17.2989899,23 11.5,23 C5.70101013,23 1,18.2989899 1,12.5 C1,11.5151324 1.13559454,10.5619345 1.38913364,9.65805651 L3.31481075,10.1982117 C3.10672013,10.940064 3,11.7119264 3,12.5 C3,17.1944204 6.80557963,21 11.5,21 C16.1944204,21 20,17.1944204 20,12.5 C20,7.80557963 16.1944204,4 11.5,4 C10.54876,4 9.62236069,4.15592757 8.74872191,4.45446326 L9.93948308,5.87355717 C10.0088058,5.95617272 10.0495583,6.05898805 10.05566,6.16666224 C10.0712834,6.4423623 9.86044965,6.67852665 9.5847496,6.69415008 L4.71777931,6.96995273 C4.66931162,6.97269931 4.62070229,6.96837279 4.57348157,6.95710938 C4.30487471,6.89303938 4.13906482,6.62335149 4.20313482,6.35474463 L5.33163823,1.62361064 C5.35654118,1.51920756 5.41437908,1.4255891 5.49660017,1.35659741 C5.7081375,1.17909652 6.0235153,1.2066885 6.2010162,1.41822583 L7.38979581,2.8349582 Z"
                                                  id="Combined-Shape" fill="#000000" opacity="0.3"/>
                                        </g>
                                    </svg>
                                    Work hours
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="tab-content">

                        <div class="tab-pane active" id="kt_portlet_base_demo_2_2_tab_content" role="tabpanel">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col">

                                        <div class="text-right">

                                            <button class="btn btn-primary" id="btn_add_payment">
                                                <i class="la la-plus"></i>
                                                New Payment
                                            </button>
                                        </div>

                                        <div class="row">
                                            <div class="col">
                                                <hr>
                                                <table class="table table-striped- table-bordered table-hover table-checkable"
                                                       id="table">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Title</th>
                                                        <th>Payment</th>
                                                        <th>Payment Type</th>
                                                        <th>Note</th>
                                                        <th>Date Added</th>
                                                        <th>State</th>
                                                        <th>Option</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="kt_portlet_base_demo_2_3_tab_content" role="tabpanel">
                            <div class="col-12">
                                <div class="row  ">
                                    <?php if($can_added =="0"){
                                        ?>
                                        <br>
                                        <div class="alert alert-outline-danger fade show w-100" role="alert">
                                            <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
                                            <div class="alert-text">You don't have level for <b>hours level</b> ,please return to your <b>uppers</b> . </div>
                                            <div class="alert-close">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                                </button>
                                            </div>
                                        </div>

                                        <?php
                                    }?>
                                </div>
                                <div class="row">

                                    <div class="col">

                                        <div class="text-right">

                                            <button class="btn btn-primary" <?php if($can_added =="0") echo 'disabled';?>  id="btn_work_hours">
                                                <i class="la la-plus"></i>
                                                Add Work Hours
                                            </button>

                                        </div>

                                        <div class="row">
                                            <div class="col">
                                                <hr>
                                                <table class="table table-striped- table-bordered table-hover table-checkable"
                                                       id="table_hours">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>My level</th>
                                                        <th>Hours Type</th>
                                                        <th>Hours</th>
                                                        <th>Date Added</th>
                                                        <th>State</th>
                                                        <th>Option</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<div class="modal fade" id="model_add_payment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add payment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form kt-form--fit kt-form--label-right" id="form_addpayment">
                    <input type="hidden" name="project" value="<?= $id ?>">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12" for="type_payment">Type
                                Payment</label>
                            <div class=" col-lg-9 col-md-9 col-sm-12">
                                <select class="form-control " id="type_payment"
                                        name="type_payment">
                                    <?php foreach ($payment_type as $item) {
                                        ?>
                                        <option value="<?= $item->ct_id ?>"><?= $item->ct_title ?></option>
                                        <?php
                                    } ?>
                                </select>

                                <b id="type_payment_error " class="text-danger error-message"><br></b>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12">
                                Title</label>
                            <div class=" col-lg-9 col-md-9 col-sm-12">
                                <input type="text" class="form-control" min="0"
                                       name="title_payment" id="title_payment">
                                <b id="title_payment_error" class="text-danger error-message"></b>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12">
                                Payment</label>
                            <div class=" col-lg-9 col-md-9 col-sm-12">
                                <div class="input-group">

                                    <input type="number" class="form-control" min="0"
                                           name="payment" id="payment">
                                    <div class="input-group-prepend"><span
                                                class="input-group-text"
                                                id="basic-addon1">S.P</span></div>

                                </div>
                                <b id="payment_error" class="text-danger error-message"></b>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12">
                                Note</label>
                            <div class=" col-lg-9 col-md-9 col-sm-12">
                                <textarea name="note" rows="3" class="form-control"></textarea>
                            </div>

                        </div>
                        <div class="form-group row  text-center">
                            <div class="col-form-label col-lg-3 col-sm-12"></div>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <button type="submit" id="submit_payment"
                                        class="btn btn-brand w-100">Add to list
                                    <b>payment</b>
                                </button>

                            </div>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="model_add_hours" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Hours</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form kt-form--fit kt-form--label-right" id="form_addhours">
                    <input type="hidden" name="project" value="<?= $id ?>">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12" for="type_payment">Hours Type </label>
                            <div class=" col-lg-9 col-md-9 col-sm-12">
                                <select class="form-control kt-select2" id="type_hours"
                                        name="type_hours">
                                    <?php foreach ($hours_type as $item) {
                                        ?>
                                        <option value="<?= $item->ht_id ?>"><?= $item->ht_title ?></option>
                                        <?php
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12">
                                Hours</label>
                            <div class=" col-lg-9 col-md-9 col-sm-12">
                                <div class="input-group">

                                    <input type="number" class="form-control" min="0"
                                           name="hours" id="hours">
                                    <div class="input-group-prepend"><span
                                                class="input-group-text"
                                                id="basic-addon1">Hours</span></div>

                                </div>
                                <b id="hours_error" class="text-danger error-message"></b>
                            </div>

                        </div>

                        <div class="form-group row  text-center">
                            <div class="col-form-label col-lg-3 col-sm-12"></div>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <button type="submit" id="submit_hours"
                                        class="btn btn-brand w-100">Add to list
                                    <b>Hours</b>
                                </button>

                            </div>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>

<script>
    var table;
    var table2;
    $("#payment").addClass("is-invalid").removeClass("is-valid");
    $("#title_payment").addClass("is-invalid").removeClass("is-valid");
    $("#btn_add_payment").click(function () {
        $("#form_addpayment").resetForm();
        $("#model_add_payment").modal('show');
    });
    $("#btn_work_hours").click(function () {
        $("#form_addhours").resetForm();
        $("#model_add_hours").modal('show');
    });
    $(document).ready(function () {
        $('#type_payment').select2({
            placeholder: "Select a Type Payment",
        });

        $("#payment").keyup(function () {
            if (this.value == "") {
                $("#payment").addClass("is-invalid").removeClass("is-valid");
            } else if (this.value <= 0) {
                $("#payment").addClass("is-invalid").removeClass("is-valid");
            } else {
                $("#payment").removeClass("is-invalid").addClass("is-valid");

            }
        });
        $("#hours").keyup(function () {
            if (this.value == "") {
                $("#hours").addClass("is-invalid").removeClass("is-valid");
            } else if (this.value <= 0) {
                $("#hours").addClass("is-invalid").removeClass("is-valid");
            } else {
                $("#hours").removeClass("is-invalid").addClass("is-valid");

            }
        });
        $("#title_payment").keyup(function () {
            if (this.value == "") {
                $("#title_payment").addClass("is-invalid").removeClass("is-valid");
            } else {
                $("#title_payment").removeClass("is-invalid").addClass("is-valid");

            }
        });

        table = $('#table').DataTable({

            "responsive": true,
            "processing": true,
            "serverSide": true,
            "sort": false,

            "searching": true,
            "select": true,
            "ajax": {
                "url": "<?=base_url('myproject/payments_datatable/' . $id)?>",
                "type": "POST"
            },

            "columns": [
                {
                    "data": "num"
                },
                {
                    "data": "title"
                }, {
                    "data": "price"
                }, {
                    "data": "payment_type"
                },
                {
                    "data": "note"
                },
                {
                    "data": "date_added"
                },
                {
                    "data": "state"
                }
                ,
                {
                    "data": "option"
                }

            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });
        table2 = $('#table_hours').DataTable({

            "responsive": true,
            "processing": true,
            "serverSide": true,
            "sort": false,
            "searching": true,
            "select": true,
            "ajax": {
                "url": "<?=base_url('myproject/hours_datatable/' . $id)?>",
                "type": "POST"
            },

            "columns": [
                {
                    "data": "num"
                },
                {
                    "data": "level"
                }, {
                    "data": "hoursType"
                }, {
                    "data": "hours"
                },
                {
                    "data": "date_added"
                },
                {
                    "data": "state"
                }
                ,
                {
                    "data": "option"
                }

            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });

    });
    $("#form_addpayment").submit(function (event) {
        event.preventDefault();
        $("#submit_payment").attr("disabled", "disabled").addClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").html("in processing");
        $.ajax({
            url: "<?=base_url('myproject/add_payment')?>",
            method: "post",
            data: $("#form_addpayment").serialize(),
            success: function (data) {
                var response = JSON.parse(data);
                if (response.result == "1") {
                    $("#submit_payment").removeAttr("disabled").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").html("Add to list payment")
                    $("#form_addpayment").resetForm();
                    table.ajax.reload();
                } else {
                    $("#submit_payment").removeAttr("disabled").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").html("Add to list payment")
                    $("#type_payment_error").html(response.type);
                    $("#title_payment_error").html(response.title_payment);
                    $("#payment_error").html(response.payment);
                }
            }
        })
    });
    $("#form_addhours").submit(function (event) {
        event.preventDefault();
        $("#submit_hours").attr("disabled", "disabled").addClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").html("in processing");
        $.ajax({
            url: "<?=base_url('myproject/add_hours')?>",
            method: "post",
            data: $("#form_addhours").serialize(),
            success: function (data) {
                var response = JSON.parse(data);
                if (response.result == "1") {
                    $("#submit_hours").removeAttr("disabled").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").html("Add to list payment")
                    $("#form_addhours").resetForm();
                    table2.ajax.reload();
                } else {
                    $("#submit_hours").removeAttr("disabled").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").html("Add to list payment")
                    $("#hours_error").html(response.hours);
                }
            }
        })
    });

    function remove_cp(id) {
        $("#item_remove_" + id).hide();
        $.ajax({
            url: "<?=base_url('myproject/remove_payment/')?>" + id
        });
        table.ajax.reload();
    }

    function remove_ph(id) {
        $("#item_remove_ph" + id).hide();
        $.ajax({
            url: "<?=base_url('myproject/remove_hours/')?>" + id
        });
        table2.ajax.reload();
    }
</script>
<style>
    .select2 .select2-container {
        width: 100%;
    }
</style>