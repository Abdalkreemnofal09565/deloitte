<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon-users"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    Employees
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">

                        &nbsp;
                        <button id="New_recode"
                                class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            New Record
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Client</th>
                    <th>Project</th>
                    <th>Employee</th>
                    <th><b>sum.</b> Hours</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Date Added</th>
                    <th>Note</th>
                    <th>State</th>
                    <th>Option</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>
<div class="modal fade" id="model" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="">Add Employees</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <form class="kt-form kt-form--fit kt-form--label-right" id="project_add_employee">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3 col-sm-12" for="Employee">Select Employees</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <select class="form-control kt-select2" id="Employee" name="employees[]"
                                    multiple="multiple">
                                <?php
                                foreach ($employees as $employee) {
                                    ?>
                                    <option value="<?= $employee->e_id ?>"><?= $employee->e_first_name . ' ' . $employee->e_last_name ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <input type="hidden" name="project" value="<?= $id ?>">
                            <b class="text-danger" id="employees_error"></b>
                        </div>

                    </div>
                    <div class="form-group row kt-margin-b-20">
                        <label class="col-form-label col-lg-3 col-sm-12" for="start_date">Start Date</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" id="start_date" name="start_date">

                            <b class="text-danger" id="start_date_error"></b>
                        </div>
                    </div>
                    <div class="form-group row kt-margin-b-20">
                        <label class="col-form-label col-lg-3 col-sm-12" label="end_date">End Date</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">

                            <input type="text" class="form-control" id="end_date" name="end_date">

                            <b class="text-danger" id="end_date_error"></b>
                        </div>
                    </div>
                    <div class="form-group row kt-margin-b-20">
                        <label class="col-form-label col-lg-3 col-sm-12" label="Note">Note</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">

                    <textarea class="form-control" name="Note" rows="3" placeholder='Enter your note ...'></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-brand" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-secondary" id="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    var table;

    $("#New_recode").click(function () {
        $("#model").modal('show');


        $("#Form_add_employee").resetForm();
    });
    $(document).ready(function () {
        $('#model').on('shown.bs.modal', function () {
            $('#Employee').select2({
                placeholder: "Select a Employee",
            });

        });
        table = $('#table').DataTable({

            "responsive": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "searching": true,
            "select": true,
            "ajax": {
                "url": "<?=base_url('project/employees_datatable/' . $id)?>",
                "type": "POST"
            },

            "columns": [
                {
                    "data": "num"
                },
                {
                    "data": "client"
                },
                {
                    "data": "project"
                },
                {
                    "data": "employee"
                },
                {
                    "data": "hours"
                },


                {
                    "data": "start_date"
                },
                {
                    "data": "end_date"
                },
                {
                    "data": "date_added"
                },
                {
                    "data": "note"
                },  {
                    "data": "state"
                },
                {
                    "data": "option"
                }

            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });

    });
    var name_table = "project_employee";
    $(document).ready(function () {

        var arrows;
        if (KTUtil.isRTL()) {
            arrows = {
                leftArrow: '<i class="la la-angle-right"></i>',
                rightArrow: '<i class="la la-angle-left"></i>'
            }
        } else {
            arrows = {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        }
        $('#start_date').datepicker({
            templates: arrows
        });
        $('#end_date').datepicker({
            templates: arrows
        });
        $("#start_date").datepicker("setDate", new Date());
        $("#end_date").datepicker("setDate", new Date());

    });
    $("#project_add_employee").submit(function (event) {
        event.preventDefault();
        $("#submit").attr("disabled", "disabled").addClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").html("in processing");
        $.ajax({
            url: "<?=base_url('project/set_employee')?>",
            method: "post",
            data: $("#project_add_employee").serialize(),
            success: function (data) {
                var response = JSON.parse(data);
                if (response.result == "1") {
                    $("#submit").html("Done").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").addClass("btn-outline-success").removeClass("btn-primary");
                    $("#error").hide();
                    setTimeout(function () {
                        window.location.href = "<?=base_url('project/employee/'.$id)?>";
                    }, 2000)
                } else {
                    $("#submit").html("Submit").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").removeAttr("disabled");
                    $("#end_date_error").html(response.end_date).show();
                    $("#employees_error").html(response.employee).show();



                }
            }
        });
    });    var name_table = "project_emplyee";
</script>
