<div class="row mt-5">
    <div class="col-md-8 offset-2">

        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Edit Project
                    </h3>
                </div>
            </div>

            <!--begin::Form-->
            <form class="kt-form" id="form_category">
                <input type="hidden" value="<?= $project_id ?>" name="id">
                <div class="kt-portlet__body">
                    <div class="row">
                        <!--                    client name-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Category">Client Name</label>
                                <?php
                                $name = $this->db->get_where('client', array('c_id' => $client))->row()->c_name;
                                ?>
                                <input type="hidden" name="client" value="<?= $client ?>">
                                <input type="text" id="client" class="form-control" value="<?= $name ?>"
                                       disabled
                                       placeholder="client name">
                                <b id="error_client" class="text-danger"></b>
                            </div>
                        </div>
                        <!--                    name-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Category">Project Name</label>
                                <input type="text" name="project_name" id="project_name" class="form-control"
                                       aria-describedby="emailHelp" value="<?= $project->p_title ?>"
                                       placeholder="Project name">
                                <b id="error_project_name" class="text-danger"></b>
                            </div>
                        </div>
                        <?php
                        foreach ($additional_data as $item) {
                            ?>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="<?= $item['category'] ?>"><?= $item['category'] ?></label>

                                    <select name="sub_category[]" id="<?= $item['category'] ?>"
                                            class="form-control select2" required>
                                        <?php foreach ($item['sub_category'] as $item2) {
                                            ?>
                                            <option <?php if ($item2->sc_id == $item['active']) echo 'selected'; ?>
                                                value="<?= $item2->sc_id ?>"> <?= $item2->sc_title ?></option>
                                            <?php
                                        } ?>
                                        <option selected value="">empty</option>
                                    </select>

                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label for="project_manger">Project Manger</label>
                                <select class="form-control" name="project_manger" id="project_manger">
                                    <?php foreach ($project_manger as $item) { ?>
                                        <option value="<?= $item->e_id ?>" <?php if ( $item->e_id==$project->project_manger) echo "selected"; ?>> <?= $item->e_first_name . " " . $item->e_last_name ?></option>
                                    <?php } ?>
                                </select>

                                <b id="error_project_manger" class="text-danger"></b>
                            </div>
                        </div>
                        <!--                   budget hours -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="budget_hours"> Budget hours</label>
                                <div class="input-group">
                                    <input type="number" name="budget_hours" id="budget_hours" class="form-control"
                                           value="<?=$project->p_budget_hours?>" min="0"
                                           placeholder="Budget hours">
                                    <div class="input-group-append"><span class="input-group-text" id="basic-addon2">hours</span>
                                    </div>
                                </div>

                                <b id="error_budget_hours" class="text-danger"></b>
                            </div>
                        </div>
                        <!--                    budget price -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="budget_price">Budget price</label>
                                <div class="input-group">
                                    <input type="number" name="budget_price" id="budget_price" class="form-control"
                                           min="0" value="<?=$project->p_budget_price?>"
                                           placeholder="Budget hours">
                                    <div class="input-group-append"><span class="input-group-text"
                                                                          id="basic-addon2">S.P</span></div>
                                </div>
                                <b id="error_budget_price" class="text-danger"></b>
                            </div>
                        </div>

                        <!--                        note -->
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="note">Project note</label>
                                <textarea name="note" id="note" rows="3" style="resize:none"
                                          class="form-control"><?=$project->p_note?></textarea>
                                <b id="error_note" class="text-danger"></b>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-primary" id="submit">Submit</button>
                        <a href="<?= base_url('project/'.$client) ?>" class="btn btn-secondary">Cancel</a>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

    </div>

</div>
<script>
    $("#error").hide();
    $(document).ready(function () {
        $('.select2').select2({
            width: '100%'
        });
    });
    $(document).ready(function () {
        $('#category').select2({
            placeholder: "Select a Category",
        });
        $('#sub_category').select2({
            placeholder: "Select a Sub Category",

        });
        $('#project_manger').select2({
            placeholder: "Select a Project Manger",
        });
        //$("#sub_category_loading").html('<span class="kt-spinner kt-spinner--v2 kt-spinner--sm kt-spinner--brand"></span><span style="margin-left :20px">Loading</span>');
        //$.ajax({
        //    url: "<?//=base_url('sub_category/get_options/')?>//",
        //    method: "post",
        //    data: {
        //        category: $("#category").val()
        //    },
        //    success: function (data) {
        //        $("#sub_category").empty().append(data);
        //        $("#sub_category").val(<?//=$project->sc_id?>//);
        //        $("#sub_category_loading").html("");
        //    }
        //
        //})
        //;
    });

    $("#category").change(function () {
        $("#sub_category_loading").html('<span class="kt-spinner kt-spinner--v2 kt-spinner--sm kt-spinner--brand"></span><span style="margin-left :20px">Loading</span>');
        $.ajax({
            url: "<?=base_url('sub_category/get_options/')?>",
            method: "post",
            data: {
                category: $("#category").val()
            },
            success: function (data) {
                $("#sub_category").empty().append(data);
                $("#sub_category_loading").html("");
            }

        })
        ;
    });
    $("#form_category").submit(function (event) {

        event.preventDefault();
        $("#submit").attr("disabled", "disabled").addClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").html("in processing");
        $.ajax({
            url: "<?=base_url('project/update')?>",
            method: "post",
            data: $("#form_category").serialize(),
            success: function (data) {
                var response = JSON.parse(data);
                if (response.result == "1") {
                    $("#submit").html("Done").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").addClass("btn-outline-success").removeClass("btn-primary");
                    $(".error").hide();
                    setTimeout(function () {
                        window.location.href = "<?=base_url('project/' . $client)?>";
                    }, 2000)
                } else {
                    $("#submit").html("Submit").removeClass("kt-spinner kt-spinner--v2 kt-spinner--md kt-spinner--info").removeAttr("disabled");
                    $("#error_client").html(response.client).show();
                    $("#error_project_name").html(response.project_name).show();
                    $("#error_category").html(response.category).show();
                    $("#error_sub_category").html(response.sub_category).show();
                    $("#error_budget_hours").html(response.budget_hours).show();
                    $("#error_budget_price").html(response.budget_price).show();
                    $("#error_project_manger").html(response.project_manger).show();

                }
            }
        })
    });
</script>
