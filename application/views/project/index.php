<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand  la la-cube"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    Projects
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">

                        &nbsp;
                        <a href="<?= base_url('project/add/' . $id) ?>" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            New Record
                        </a>
                        <a href="<?= base_url('project/export/' . $id) ?>"
                           class="btn btn-outline-brand btn-elevate btn-icon-sm">
                            <i class="la la-download"></i>
                            Export Excel
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>client</th>
                    <th>budget hours</th>
                    <th>budget price</th>

                    <th>Date Added</th>
                    <th>State</th>
                    <th>Project Manger</th>
                    <?php
                    foreach ($category as $item) {
                        ?>
                        <th><?= $item->c_title ?></th>
                        <?php
                    }
                    ?>
                    <th>Note</th>
                    <th>Hours</th>
                    <th>Cost Hours</th>
                    <th>Expanse</th>
                    <th>Option</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
</div>
<script>
    var table;
    $(document).ready(function () {

        table = $('#table').DataTable({

            "responsive": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "searching": true,
            "select": false,
            "ordering": false,
            "ajax": {
                "url": "<?=base_url('project/datatable/' . $id)?>",
                "type": "POST"
            },

            "columns": [
                {
                    "data": "num"
                },
                {
                    "data": "title"
                }, {
                    "data": "client"
                }, {
                    "data": "budget_hours"
                }, {
                    "data": "budget_price"
                },
                {
                    "data": "date_register"
                },
                {
                    "data": "state"
                },
                {
                    "data": "project_manger"
                }
                <?php
                foreach ($category as $item){
                ?>
                ,
                {
                    "data": "<?=(string)$item->c_title?>"
                }                    <?php
                }?>
                ,
                {
                    "data": "note"
                },
                {
                    "data": "hours"
                },
                {
                    "data": "cost_hours"
                },
                {
                    "data": "expanse"
                }
                ,
                {
                    "data": "option"
                }

            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });

    });

    var name_table = "project";

</script>