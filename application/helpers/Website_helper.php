<?php
function check_permission($permission, $with_view = null)
{
    $CI = get_instance();
    $CI->load->model('Website');
    if ($CI->Website->check_permission($permission)){
        return true;

    }
    else if (!isset($with_view) ||    $with_view ==true)
        page_403();
    return false;
}

function page_404()
{

}

function page_403()
{
    $CI = get_instance();
    $header['title'] = "403 not have permission";
    $header['my_project'] = $CI->db->select('count(*) as cnt')->group_by('p_id')->get_where('project_emplyee', array('e_id' => $CI->session->userdata('id'), 's_id' => 1, 'pe_date_start<=' => date("Y/m/d"), 'pe_date_active>=' => date("Y/m/d")));

    if ($header['my_project']->num_rows() != 0)
        $header['my_project'] = $header['my_project']->row()->cnt;
    else {
        $header['my_project'] = 0 ;
    }
    $CI->load->view('share/header', $header);
    $CI->load->view("share/page_403");
    $CI->load->view('share/footer');
}